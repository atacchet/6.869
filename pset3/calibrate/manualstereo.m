list = dir('../pics/z*');
fnames = {list(:).name};

for i = 1:numel(fnames)
		I = imread(fullfile('../pics',fnames{i}));
		IMT = imagecalibrate(I);
		clear I
		h = figure;
		imshow(IMT);
		set(gcf, 'units','normalized','outerposition',[0 0 1 1]);

		X = ginput(2);
		close(h);
		d(i) = abs(X(1,1) - X(2,1));
end

%d = [269.298609628722 230.577415599535 224.63174772346 213.173160978665 195.766175106365];
d = [271.545454545455 254.267753201398 225.909090909091 207.832167832168 193.01282051282];
z = [18 25 32 40 50];
