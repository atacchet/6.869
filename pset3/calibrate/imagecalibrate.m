function imt = imagecalibrate(im)
		U =  [1361.71637426901          202.025146198831;
	          3047.19941520468          245.328070175439;
	          3040.53742690058          2410.47426900585;
	          1341.73040935673          2420.46725146199];
		h = figure;
		imshow(im);
		set(gcf, 'units','normalized','outerposition',[0 0 1 1]);
		X = ginput(4);
		close(h);
		t = maketform('projective', X, U);
		imt = imtransform(im, t);


