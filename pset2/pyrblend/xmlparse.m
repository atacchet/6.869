function [p] = xmlparse(filename)
	s = xml2struct(filename);
	for i = 1:numel(s.annotation.object.polygon.pt)
		p(i,1) = str2num(s.annotation.object.polygon.pt(i).x);
		p(i,2) = str2num(s.annotation.object.polygon.pt(i).y);
	end
		
