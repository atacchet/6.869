% xml2struct.m
% Charlie Frogner
%
% Load XML into a struct.
% ================================
% In:
% filename
% -> [string]
%
% Out:
% result
% -> [struct]

function result = xml2struct(filename)
    if (nargin<1||~ischar(filename))
        error('expected argument ''filename''');
    end

    dom = xmlread(filename);

    queue = {dom};
    descent_base = {''};
    descent_num = [1];
    descent_combo = {''};
    result = struct;

    while (~isempty(queue))
        parent = queue{1};
        children = parent.getChildNodes;
        n_children = children.getLength;

        if (n_children==0&&any(strcmpi(methods(parent),'getData')))
            eval(['result' regexprep(descent_combo{1},'\([0-9]+\)$','') ' = parent.getData;']);
        elseif (n_children==1&&strcmpi(children.item(0).getNodeName,'#text'))
            eval(['result' regexprep(descent_combo{1},'\([0-9]+\)$','') ' = char(children.item(0).getData);']);
        else
            eval(['result' descent_combo{1} ' = struct;']);
            for which_child = 0:(n_children-1)
                child = children.item(which_child);
                child_name = regexprep(char(child.getNodeName),'^_|\W','');
                queue{end+1} = child;
                descent_base{end+1} = [descent_combo{1} '.' child_name];
                descent_num(end+1) = nnz(strcmpi(descent_base{end},descent_base));
                descent_combo{end+1} = [descent_base{end} '(' num2str(descent_num(end)) ')'];
                %{
                descent{end} = ...
                    [descent{end} '(' ...
                     num2str(nnz(strcmpi(descent{end},regexprep(descent,'\([0-9]+\)$','')))) ...
                     ')'];
                %}
            end % (which_child loop)
        end

        queue = queue(2:end);
        descent_base = descent_base(2:end);
        descent_num = descent_num(2:end);
        descent_combo = descent_combo(2:end);
    end % (queue traversal)
end % (xml2struct)