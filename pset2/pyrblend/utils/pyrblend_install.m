function path = pyrblend_install()

basePath = pyrblend_root();
addpath(basePath);
addpath(fullfile(basePath,'utils'));
addpath(fullfile(basePath,'../lappyr'));
addpath(fullfile(basePath,'../../../../pset2/matlabPyrTools'));
