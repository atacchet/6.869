function [Ires] = plotPyr(I)

[pyr, inds] = buildLpyr(I);
showLpyr(pyr, inds);
Ires = reconLpyr(pyr,inds);
