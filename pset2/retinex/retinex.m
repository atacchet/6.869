% RETINEX - pset 3
%
% Implement a simple version of the retinex algorithm
img = mean(imread('kid.jpg'),3);
img = log(img);
theta = .05;


% Derivation filters
clear fn
fn(:,:,1) = [0 0 0; -1 1 0; 0 0 0];
fn(:,:,2) = [0 0 0; -1 1 0; 0 0 0]';

% Derivate
out = convFn(img, fn);


% INSERT YOUR CODE HERE
out( abs(out) < theta ) = 0;

% Pseudo-inverse (using the trick from Weiss, ICCV 2001; equations 5-7)
im=deconvFn(out,fn);
im = exp(im);
img = exp(img);

%im = 255*(im - min(im(:)))/(max(im(:)) - min(im(:)));

% Visualization
figure
subplot(221)
imshow(uint8(img))
title('input (I)')
subplot(222)
imagesc(im)
title('output')
%axis('square'); axis('off')
axis equal tight off
colormap(gray(256))
subplot(223)
imagesc(out(:,:,1))
title('dI/dx')
%axis('square'); axis('off')
axis equal tight off
colormap(gray(256))
subplot(224)
imagesc(out(:,:,2))
title('dI/dy')
%axis('square'); axis('off')
axis equal tight off
colormap(gray(256))
