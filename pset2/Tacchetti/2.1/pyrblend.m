function [ires] = pyrblend(a,b,mask);

[pa ia] = buildLpyr(a);
[pb ib] = buildLpyr(b);
[m im] = buildGpyr(mask);

for i = 1:size(ia, 1)
	ialow = 0;
	for j = 1:i-1
		ialow = ia(j,1)*ia(j,2) + ialow;
	end
	iai = pa(ialow+1 : ialow + ia(i,1) * ia(i,2));
	ibi = pb(ialow+1 : ialow + ia(i,1) * ia(i,2));
	mi  = m (ialow+1 : ialow + ia(i,1) * ia(i,2));
	iai = reshape(iai, ia(i,:));
	ibi = reshape(ibi, ib(i,:));
	mi =  reshape(mi,  im(i,:))/(2^(i-1));
	mineg = 1-mi;

	tmp = iai.*mi + ibi.*mineg;

	pres(ialow+1 : ialow + ia(i,1) * ia(i,2)) = tmp(:);
end	
ires = reconLpyr(pres', ia);
