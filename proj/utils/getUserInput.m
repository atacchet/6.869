function [decision] = getUserInput(vexobj_location)
		choice = input(['Do you want to over write the existing file '...
						vexobj_location '? (y/[n]) : '],'s');
		if isempty(choice) choice = 'n'; end
		while ~strcmp(choice,'y') && ~strcmp(choice,'n')
				choice = input('\Please type ''y'' or ''n'' : ','s'); 
		end		
		switch(choice)
		case {'y'}
			delete(vexobj_location);
			decision = false;
			return;
		case{'n'}
			decision = true;
			return;
		end	
end

