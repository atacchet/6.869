function path = tempdir

path = fullfile(basedir, 'temp', getenv('USER'));

return;
