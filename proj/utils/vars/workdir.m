function path = workdir

% The path to the neo2 work directory, where classifiers, etc. are stored.

path = fullfile(basedir, 'work');

return;
