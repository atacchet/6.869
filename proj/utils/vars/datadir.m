function path = datadir

% The path to the neo2 data files (movies).

path = fullfile(basedir, 'data');

return;
