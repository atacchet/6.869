% -------------------------------------------------------------------------------
% Do super resolution given class priors
% The image structs must have been computed beforehand
% -------------------------------------------------------------------------------
% Input :
% Output :
% -------------------------------------------------------------------------------

addpath(genpath([pwd 'src']))
run('GURLS/biggurls/utils/biggurls_install.m');
params = sr_initialize;
test_images = sr_get_test_images(params);
% ids_to_process = 1:length(test_images);
ids_to_process = [1:5 801:806 864:869 886:890 1291:1296 1700:1705 1769:1776 1827:1828];

opt.NumImages = length(ids_to_process);
opt.DictPath = fullfile(params.path.data,params.path.dicoFile);

% prepare and save infopath struct
imInfo.NumImages = opt.NumImages;

imInfoPath = fullfile(params.path.jobs,'imInfo.mat');
opt.imInfoPath = imInfoPath;

blockID = 0;
imgID   = 0;
fprintf('\n')
for im_ind = ids_to_process
    imgID = imgID + 1;

    % Pre-process image
    fprintf('   - pre-processing image %04d/%04d...\n',imgID,length(ids_to_process));
    im_struct = sr_preprocess_img(test_images{im_ind},params);
    im_struct.imgID = imgID;
    
    % Extract patches from input
    im_struct = sr_extract_patches(im_struct,params);

    % Set parameters
    im_struct.doPCA                  = params.doPCA;
    im_struct.doContrastNormalize    = params.doContrastNormalize;
    im_struct.NN                     = params.NN;
    imInfo.image(imgID).patches_h    = im_struct.patch_h;
    reportDir                        = fullfile(params.path.output,im_struct.im_name);
    imInfo.reportWorkFileName{imgID} = fullfile(reportDir,[im_struct.im_name '_candidates.mat']);
    opt.imRows(imgID)                = im_struct.patch_h;
    if ~exist(reportDir,'dir')
        mkdir(reportDir)
    end

    % Save struct
    structPath = fullfile(params.path.imStruct,[im_struct.im_name '_struct.mat']);
    save(structPath,'-struct', 'im_struct','-v7.3');

    for rowID = 1:im_struct.patch_h
        blockID                     = blockID + 1;
        opt.imStructPath{blockID}   = structPath;
        opt.rowID(blockID)          = rowID;
    end
end

save(imInfoPath,'-struct','imInfo','-v7.3')

optFileName =sprintf('job_opt.mat');
save(fullfile(params.path.output,optFileName),'-struct','opt','-v7.3')

jobFileName = 'output/jobs/job_file.mat';
admMRFRetrievePrepare(opt,jobFileName);
