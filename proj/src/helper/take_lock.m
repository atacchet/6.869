% Make a lock file, return true if the lock has been made and taken,
% false if it already exists
function bool = take_lock(dirName)
[smesg, smess, smessid] = mkdir(dirName);
bool = ~strcmp(smessid,'MATLAB:MKDIR:DirectoryExists');

