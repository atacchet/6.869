%----------------------------------------------------------------------
% Get the dictionary of patches and load/save
%----------------------------------------------------------------------
% Input: 
%   - params : global parameters
% Output:
%   - dictionary : dictionary of patches from the database
%----------------------------------------------------------------------
function dictionary = sr_get_dictionary(params)
train = vex_load(fullfile(params.path.data,params.path.train_set),'d');
train_images = train.imPath;
dicoFile = fullfile(params.path.data,params.path.dicoFile);

if ~exist(dicoFile,'file')
    dictionary = sr_gentrainingvex(train,params);

    % PCA if needed
    lowres = dictionary.lowres;
    nSamples = size(lowres,1);
    if params.doPCA
        fprintf('-- Doing PCA')
        tic;
        dictionary.lowres_mean = mean(lowres);
        lowres = lowres - repmat(dictionary.lowres_mean,[nSamples,1]);
        [V,D] = eig(lowres'*lowres);
        D = diag(D);
        D = D(end:-1:1);
        V = V(:,end:-1:1);
        dictionary.reduced_dim = params.PCAdim;
        dictionary.V = V(:,1:dictionary.reduced_dim);
        lowres = lowres*dictionary.V;
        dictionary.lowresPCA = lowres;
        t = toc;
        fprintf(' - done in %fs\n',t)
    end

    % Build kd_tree
    fprintf('-- Building kd-tree')
    tic
    kd_tree = vl_kdtreebuild(lowres');
    time = toc;
    dictionary.kd_tree = kd_tree;
    dictionary.prob = sr_get_class_prob(params);
    fprintf(' done in %.2fs\n',time);
    
    % Add category names to the struct
    dictionary.category_names = train.catNames;
    % Save dico structure
    tic;
    fprintf('-- Saving dictionary %s', params.path.dicoFile)
    save(dicoFile,'-struct','dictionary','-v7.3');
    t = toc;
    fprintf(' - done and saved in %fs\n',t)
else 
    fprintf('-- Loading dictionary %s', params.path.dicoFile)
    tic;
    dictionary = load(dicoFile);
    t = toc;
    fprintf(' - done in %fs\n',t)
end
% EOF
