%----------------------------------------------------------------------
% Perform belief propagation to find explaining patches of high res
%----------------------------------------------------------------------
% Input: 
%   - im_struct : the image data to append
%   - params : global parameters
% Output:
%   - im_struct : the image data to append
%----------------------------------------------------------------------
function im_struct = sr_run_bp(im_struct,dictionary,params)
%----------------------------------------------------------------------
% run belief propagation
%----------------------------------------------------------------------
tic;
fprintf('     running BP')
filename = fullfile(params.path.output,im_struct.im_name,im_struct.im_name);
[height,width] = size(im_struct.im);
patchDim = params.patchSize.H*2+1;
alpha = 0.5;
nIterations = [1,5,10,30,50,100];
for ii = 1:length(nIterations)
    [IDX,En] = immaxproduct(im_struct.CO,im_struct.CM_h*alpha,im_struct.CM_v*alpha,nIterations(ii),0.5);

    % Keep track of number of patches from each class
    classes_count = zeros(1,size(dictionary.prob,2));
    patchesBP = zeros(patchDim^2,im_struct.patch_h,im_struct.patch_w);
    for i=1:im_struct.patch_h
        for j=1:im_struct.patch_w
            patchesBP(:,i,j) = im_struct.candidates(i,j).patches(IDX(i,j),:)';
            id = im_struct.candidates(i,j).idx(IDX(i,j));
            patch_class = dictionary.classhigh(id);
            classes_count(patch_class) = classes_count(patch_class)+1;
        end
    end
    im_struct.patch_classes(ii,:) = classes_count;

    % Save Energy Optimization Profile
    im_struct.energy = En;

    % the result of just applying the iid best matches
    im_high = mergePatches(patchesBP,params.overlapSize,width,height);
    im_struct.im_high{ii} = im_high;

    % figure;imshow(composeImage(im_high+im_struct.im_low,im_struct.im_color));drawnow;

    pS = params.probScale;
    highresFile = sprintf('%s_prob_%03d_bp_%03d_highres.jpg',filename,pS,nIterations(ii));
    grayFile = sprintf('%s_prob_%03d_bp_%03d_highres.jpg',filename,pS,nIterations(ii));
    colorFile = sprintf('%s_prob_%03d_bp_%03d_highres.jpg',filename,pS,nIterations(ii));
    imwrite(im_high+0.5,highresFile,'quality',100);
    imwrite(im_high+im_struct.im_low,grayFile,'quality',100);
    imwrite(composeImage(im_high+im_struct.im_low,im_struct.im_color),colorFile,'quality',100);
end
imwrite(im_high+0.5,[filename '_highfrequency.jpg'],'quality',100);
t = toc;
fprintf(' - done in %fs\n',t)
