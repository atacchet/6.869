%----------------------------------------------------------------------
% Extract patches from the query image
%----------------------------------------------------------------------
% Input: 
%   - im_struct : the image data to append
%   - params : global parameters
% Output:
%   - im_struct : the image data to append
%----------------------------------------------------------------------
function im_struct = sr_extract_patches(im_struct,params)

    fprintf('     extract patches')
    tic;
[patches,mask] = extractPatches(im_struct.im_bandpass,...
    params.patchSize.L,params.overlapSize,params.patchSize.H);

[dim,h,w]=size(patches);

% if contrast normalization is turned on, then normalize the patches
if params.doContrastNormalize
    stdev = squeeze(std(patches));
    scale = stdev + 0.0001;
    patches = patches./repmat(reshape(scale,[1 h w]),[dim 1 1]);
end

im_struct.patches   = patches;
im_struct.mask      = mask;
im_struct.patch_dim = dim;
im_struct.patch_h   = h;
im_struct.patch_w   = w;
im_struct.scale     = scale;
t = toc;
fprintf(' - done in %fs\n',t)

%EOF
