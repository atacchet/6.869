%------------------------------------------------------------
% Get default parameters for super resolution
%------------------------------------------------------------
% Output : 
%   - params : global parameters
%------------------------------------------------------------
function params = sr_initialize
    tic;
    fprintf('-- Initialiazing parameters')

% Set the environment for external librairies
try
    % run('GURLS/biggurls/utils/biggurls_install.m');
    % run('jmlibs/jmlibs_install')
    addpath(genpath('jmlibs/vex_14'))
    addpath(genpath('SRmatlab')) %TODO : remove that
    addpath('jmlibs/gl_4')
catch
    fprintf('ERROR : Missing jmlibs toolbox\n')
    return
end
try
   run('../vlfeat/toolbox/vl_setup')
catch
    fprintf('ERROR : Missing vlfeat toolbox\n')
    return
end

% Set path dependencies
path.data           = 'data';if ~exist(path.data,'dir') mkdir(path.data); end;
path.test_set       = 'cal10best_test';
path.train_set      = 'cal10best_train';
path.classes        = 'cal10best.mat';
path.dicoFile       = 'dictionary_10.mat';
path.output         = 'output'; if ~exist(path.output,'dir') mkdir(path.output); end;
path.imStruct       = fullfile(path.output,'im_structs');if ~exist(path.imStruct,'dir') mkdir(path.imStruct); end;
path.jobs           = fullfile(path.output,'jobs');if ~exist(path.jobs,'dir') mkdir(path.jobs); end;
params.path = path;


% Set global patch structure/dimensions
patchSize.L         = 7; % for lowres
patchSize.H         = 4; % for highres
params.patchSize    = patchSize;
params.intervalSize = 3;
params.overlapSize  = 2;

% Number of neighbors to query in the kd-tree,
% also the number of state for each latent node in the MRF
params.NN           = 30;

% Reduce dimensions for speedup
params.doPCA        = true;
params.PCAdim       = 20;

% Contrast normalize across patches
params.doContrastNormalize = true;

% weight coef for the probability term : 1 full weight, 0 non existent
params.probScale    = 10;
t = toc;
fprintf(' - done in %fs\n',t)
%EOF
