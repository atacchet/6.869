% -------------------------------------------------------------------------------
% Gets the class pred from a GURLS ouput file and spits probabilities 
% -------------------------------------------------------------------------------
% Input :
%   - params : global params with the path to GURLS output
% Output :
%   - N*T matrix of probabilities with T classes, N images
% -------------------------------------------------------------------------------
function prob = sr_get_class_prob(params)
    pathToData = fullfile(params.path.data,params.path.classes);
    opt = load(pathToData);
    opt = opt.opt;

    prob = opt.pred;
    
    % Convert to probs :
    prob = exp(prob);
    prob = bsxfun(@rdivide,prob,sum(prob,2));

% EOF
