%----------------------------------------------------------------------
% Retrive top matching patches for the image using kd-tree
%----------------------------------------------------------------------
% Input: 
%   - im_struct : the image data to append
%   - dictionary : dictionary of patches from the database
%   - params : global parameters
% Output:
%   - im_struct : the image data to append
%----------------------------------------------------------------------
function im_struct = sr_retrieve_nn(im_struct,dictionary,params)

nameTrunk = ...
fullfile(params.path.output,im_struct.im_name,[im_struct.im_name '_candidates']);
candidatesLock = [nameTrunk '.lock'];
candidatesFile = [nameTrunk '.mat'];
chunk_files = {};

if ~exist(candidatesFile,'file')
fprintf('     retrieving candidate NN\n')
tic;
order = randperm(im_struct.patch_h);
for i=order
    chunkTime = tic;
    chunkName = [nameTrunk sprintf('_chunk_%02d',i)];
    chunkLock = [chunkName '.lock'];
    chunkFile = [chunkName '.mat'];
    chunk_files{i} = chunkFile;

    fprintf('     - chunk %2d/%2d',i,length(order))
    % check if chunk lock is present
    if exist(chunkFile,'file') || ~take_lock(chunkLock)
        fprintf(' : skip   \n')
        % is processed or being processed elsewhere
        continue;
    else
        for j=1:im_struct.patch_w
        fprintf('\r')
        fprintf('     - chunk %2d/%2d',i,length(order))
        fprintf(' : %06.2f%%',100*j/im_struct.patch_w)
            % use the patch and mask as the key to search the training data
            if params.doPCA
                patchPCA = (im_struct.patches(:,i,j)'-dictionary.lowres_mean)*dictionary.V;
                [idx,distance] = ...
                vl_kdtreequery(dictionary.kd_tree,dictionary.lowresPCA',patchPCA','NumNeighbors',params.NN*4);
                [~, index] = sort(distance,'ascend');
                idx = idx(index(1:params.NN));
            else
                [idx,distance] = ...
                vl_kdtreequery(dictionary.kd_tree,dictionary.lowres',im_struct.patches(:,i,j),'NumNeighbors',params.NN);
            end
            idx = idx(end:-1:1); % sort wrt distance
            chunk(j).idx               = idx;
            chunk(j).patches           = dictionary.highres(idx,:);
            if params.doContrastNormalize
                chunk(j).patches           = chunk(j).patches*im_struct.scale(i,j);
            end
        end
        tt = toc(chunkTime);
        fprintf(' - %fs\n',tt)

        % Save file & release lock
        save(chunkFile,'chunk','-v7.3');
        try
            rmdir(chunkLock);
        catch
            fprintf('     <!> cannot remove %s\n',chunkLock)
        end
    end % chunk lock

end
    t = toc;
    fprintf('     retrieving candidate NN')
    fprintf(' - done in %fs\n', t)
end % chunks files building

if exist(candidatesFile,'file')|| ~take_lock(candidatesLock)
    %file is here or someone has the lock
    wait_until_all_present({candidatesFile},5);
    
    %load
    fprintf('     loading candidates file')
    candidates = load(candidatesFile);
    candidates = candidates.candidates;
    fprintf(' - done\n')
    im_struct.candidates = candidates;
    return;
end

fprintf('     gathering all candidates\n')

% we own the lock : gather chunks
wait_until_all_present(chunk_files,5);
for i = 1:numel(chunk_files)
    chunk = load(chunk_files{i});
    candidates(i,:) = chunk.chunk;
end

% save the candidates
save(candidatesFile,'candidates','-v7.3')
try
    rmdir(candidatesLock);
catch
    fprintf('     <!> cannot remove %s\n',candidatesLock)
end
fprintf(' - done \n')

%cleanup temp files
if fileexists(candidatesFile)
    for i = 1:numel(chunk_files)
        try
            delete(chunk_files{i});
        catch
            fprintf('     <!> cannot remove %s\n',chunk_files{i})
        end
    end
end
im_struct.candidates = candidates;

%EOF
