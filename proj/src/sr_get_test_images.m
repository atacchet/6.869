%----------------------------------------------------------------------
% Load all test images paths
%----------------------------------------------------------------------
% Input: 
%   - params : global parameters
% Output:
%   - test_images : paths to images
%----------------------------------------------------------------------
function test_images = sr_get_test_images(params)
    fprintf('-- Loading test images')
    tic;
    test = vex_load(fullfile(params.path.data,params.path.test_set));
    test_images = test.imPath;
    test_images = cellfun(@(x)strcat(params.path.data,regexp(x,'/cal101.*','match')),test_images);
    t = toc;
    fprintf(' - done in %fs',t)

