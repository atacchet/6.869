%----------------------------------------------------------------------
% Pre-process current test image, spectral bands decomposition etc
%----------------------------------------------------------------------
% Input: 
%   - imPath : path to image to load/process
%   - params : global parameters
% Output:
%   - im_struct : the resulting image data structure used in the pipeline
%----------------------------------------------------------------------
function im_struct = sr_preprocess_img(imPath,params)
    [path, filename, ~] = fileparts(imPath);
    [~,classname,~] = fileparts(path);

    tic;
    fprintf('     pre-processing')
    filename = [classname '_' filename];
    im_struct.im_name = filename;
    % make new folder
    imFolder = fullfile(params.path.output,filename);
    if ~exist(imFolder,'dir')
        mkdir(imFolder)
    end
    
    filename = fullfile(imFolder,filename);
    
    % lowres version
    im = imread(imPath);
    if ndims(im) ~=3 % assume grayscale
        im = repmat(im,[1 1 3]);
        %force rgb representation
    end
    imwrite(im,[filename '_groundtruth_HR.jpg'],'quality',100);
    Im = imresize(imfilter(im,fspecial('gaussian',7,1),'same','replicate'),0.25,'bicubic');
    imwrite(Im,[filename '_input.jpg'],'quality',100);
    
    im = im2double(im);
    
    %luminace chrominace decomposition
    [im,im_color] = decomposeImage(im);
    [height,width]=size(im);

    im_color = imresize(imfilter(im_color,fspecial('gaussian',7,1),'same','replicate'),0.25,'bicubic');
    im_color = imresize(im_color,[height,width],'bicubic');
    
    %-------------------------------------------------------------------
    % separate the image into several bands
    % we will infer the high-res band from im_bandpass
    %-------------------------------------------------------------------
    [im_laplacian,im_bandpass,im_lowlow]=imband(im);
    im_low = im_bandpass + im_lowlow;
    imwrite(composeImage(im_low,im_color),[filename '_lowres.jpg'],'quality',100);
    % imwrite(im_low,[filename '_lowres_gray.jpg'],'quality',100);
    % imwrite(im_laplacian+0.5,[filename '_laplacian.jpg'],'quality',100);
    % imwrite(im,[filename '_highres.jpg'],'quality',100);
    % imwrite(im_bandpass+0.5,[filename '_bandpass.jpg'],'quality',100);
    im_cn = imcontrastnormalize(im_bandpass,params.patchSize.L);
    % imwrite(im_cn/(max(abs(im_cn(:)))+0.5)+0.5,[filename '_bandpass_cn.jpg'],'quality',100);


    im_struct.im = im;
    im_struct.im_color = im_color;
    im_struct.im_low = im_low;
    im_struct.im_cn = im_cn;
    im_struct.im_lowlow = im_lowlow;
    im_struct.im_bandpass = im_bandpass;
    imstruct.im_laplacian = im_laplacian;

    t = toc;
    fprintf(' - done in %fs\n',t)
%EOF
