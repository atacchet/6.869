%----------------------------------------------------------------------
% Compute the compatibility function to prepare for belief propagation
%----------------------------------------------------------------------
% Input: 
%   - im_struct : the image data to append
%   - dictionary : dictionary of patches from the database
%   - params : global parameters
% Output:
%   - im_struct : the image data to append
%----------------------------------------------------------------------
function im_struct = sr_compute_compatibilities(im_struct,dictionary,params)

% Compute compatibility with the data (Phi in the paper)
fprintf('     building DATA compatibility');
tic;
nDim = params.NN;
CO = zeros(nDim,im_struct.patch_h,im_struct.patch_w);

for i=1:im_struct.patch_h
    for j=1:im_struct.patch_w
        lowres = dictionary.lowres(im_struct.candidates(i,j).idx,:);
        classes = dictionary.classlow(im_struct.candidates(i,j).idx,:);
        % NOTE : modify standard method by adding class prob prior, compute
        % in log( - arg)
        CO(:,i,j) = sum((repmat(im_struct.patches(:,i,j)',[nDim,1])-lowres).^2,2); 

        % probability prior
        CO(:,i,j) = CO(:,i,j) - params.probScale*log(dictionary.prob(im_struct.imgID,classes)');

        if params.doContrastNormalize
            CO(:,i,j) = CO(:,i,j)*im_struct.scale(i,j)^2;
        end
    end
end
t = toc;
fprintf(' - done in %fs\n',t);


% get the patches from the low-res image
PatchesLowRes = extractPatches(im_struct.im_low,params.patchSize.H,params.overlapSize);

% add the low res onto the patches
for i=1:im_struct.patch_h
    for j=1:im_struct.patch_w
        im_struct.candidates(i,j).patchesFull = double(im_struct.candidates(i,j).patches) + ...
                                      repmat(PatchesLowRes(:,i,j)',[params.NN,1]);
    end
end

% Compute compatibility between the latent vars (Psi in the paper)
% Horizontal pass first
patchDim = params.patchSize.H*2+1;
fprintf('     building SPATIAL compatibility (horizontal)');
tic;
CM_h = zeros([params.NN, params.NN, im_struct.patch_h, im_struct.patch_w-1]);
for i=1:im_struct.patch_h
    for j=1:im_struct.patch_w-1
        patch1 = reshape(im_struct.candidates(i,j).patchesFull',[patchDim,patchDim,params.NN]);
        patch2 = reshape(im_struct.candidates(i,j+1).patchesFull',[patchDim,patchDim,params.NN]);
        patch1 = reshape(patch1(:,end-params.overlapSize+1:end,:),[patchDim*params.overlapSize,params.NN]);
        patch2 = reshape(patch2(:,1:params.overlapSize,:),[patchDim*params.overlapSize,params.NN]);
        patch1 = repmat(patch1(:),[1,params.NN]);
        patch2 = repmat(patch2,[params.NN,1]);
        CM_h(:,:,i,j) = reshape(sum(reshape((patch1-patch2).^2,[patchDim*params.overlapSize,params.NN^2]),1),[params.NN,params.NN]);
    end
end
t = toc;
fprintf(' - done in %fs\n',t);

% Then vertical
fprintf('     building SPATIAL compatibility (vertical)');
tic ;
CM_v = zeros([params.NN, params.NN, im_struct.patch_h-1, im_struct.patch_w]);
for i=1:im_struct.patch_h-1
    for j=1:im_struct.patch_w
        patch1 = reshape(im_struct.candidates(i,j).patchesFull',[patchDim,patchDim,params.NN]);
        patch2 = reshape(im_struct.candidates(i+1,j).patchesFull',[patchDim,patchDim,params.NN]);
        patch1 = reshape(patch1(end-params.overlapSize+1:end,:,:),[patchDim*params.overlapSize,params.NN]);
        patch2 = reshape(patch2(1:params.overlapSize,:,:),[patchDim*params.overlapSize,params.NN]);
        patch1 = repmat(patch1(:),[1,params.NN]);
        patch2 = repmat(patch2,[params.NN,1]);
        CM_v(:,:,i,j) = reshape(sum(reshape((patch1-patch2).^2,[patchDim*params.overlapSize,params.NN^2]),1),[params.NN,params.NN]);
    end
end
t = toc;
fprintf(' - done in %fs\n',t);

im_struct.CO    = CO;
im_struct.CM_h  = CM_h;
im_struct.CM_v  = CM_v;
