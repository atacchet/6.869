%----------------------------------------------------------------------
% Extract image spectral bands adapted to the Vex structure
%----------------------------------------------------------------------
% Input: 
%   - im_struct : the image data to append
%   - dictionary : dictionary of patches from the database
%   - params : global parameters
% Output:
%   - im_struct : the image data to append
%----------------------------------------------------------------------
function io = vex_imband(im,layer)

[im_laplacian,im_bandpass,im_lowlow] = imband(im);
if layer == 1
    io = im_lowlow;
    return
elseif layer == 2
    io = im_bandpass;
    return
else
    io = im_laplacian;
    return
end
% function imo = vex_imband(im, layer);
% 	[height,width,nchannels]=size(im);
% 	if isfloat(im)
% 	    im = im2double(im);
% 	end
% 
% 	if layer == 1 || layer == 2
% 		im_lowlow = imfilter(im,fspecial('gaussian',25,5),'same','replicate');
% 		lowstd = std(im_lowlow(:));
% 		if layer == 1
% 			imo = im_lowlow/(lowstd+eps);
% 			return;
% 		end
% 	end
% 	
% 	% downsample 
% 	im_Low = imresize(imfilter(im,fspecial('gaussian',7,1),'same','replicate'),0.25,'bicubic');
% 	
% 	% upsample
% 	im_low = imresize(im_Low,[height,width],'bicubic');
% 	
% 	% obtain laplacian
% 	if layer == 3
% 		imo = im - im_low;
% 		return;
% 	end
% 	% get the band-pass 
% 	imo = im_low - im_lowlow;
% 	imo = imo/(lowstd+eps);
