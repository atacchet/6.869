function dictionary = sr_gentrainingvex(d, params)
% function to generate training for super resolution

dictionary = [];
dictionary.lowres = [];
dictionary.highres = [];
dictionary.classlow = [];
dictionary.classhigh = [];

patchSizeLow = params.patchSize.L;
patchSizeHigh = params.patchSize.H;
intervalSize = params.intervalSize;

for k=1:d.NumItems
    fprintf('-- Building dictionary %s', params.path.dicoFile)
	fprintf(' - image %d: %.2f%%\r',k, k*100/d.NumItems);    
    % obtain the dictionary
    patches = im2patches(d.imband{k}, patchSizeLow, intervalSize);
    dictionary.lowres = [dictionary.lowres; patches];
	c = d.catID(k) * ones(size(patches,1),1);
	dictionary.classlow = [dictionary.classlow; c];
    patches = im2patches(d.imhigh{k}, patchSizeHigh, intervalSize,[],patchSizeLow);
    dictionary.highres = [dictionary.highres; patches];
	c = d.catID(k) * ones(size(patches,1),1);
	dictionary.classhigh = [dictionary.classhigh; c];
end

if params.doContrastNormalize
    fprintf('\n   - contrast normalization');
    % compute the standard deviation of each low-res patch
    lowres_std = std(dictionary.lowres,[],2);
    dictionary.scale = lowres_std+0.0001;
    % normalize both low- and high-res patches based on the scale
    dictionary.lowres = dictionary.lowres./repmat(dictionary.scale,[1 size(dictionary.lowres,2)]);
    dictionary.highres = dictionary.highres./repmat(dictionary.scale,[1 size(dictionary.highres,2)]);
    fprintf('- done\n');
end


