%----------------------------------------------------------------------
% Compute the compatibility function to prepare for belief propagation
%----------------------------------------------------------------------
% Input: 
%   - im_struct : the image data to append
%   - dictionary : dictionary of patches from the database
%   - params : global parameters
% Output:
%   - im_struct : the image data to append
%----------------------------------------------------------------------
function sr_naive_reconstruct
%-------------------------------------------------------------------
% put all the patches together on a grid
%-------------------------------------------------------------------
patchDim = patchSizeH*2+1;
PatchesIID = zeros(patchDim^2,h,w);
for i=1:h
    for j=1:w
        PatchesIID(:,i,j) = candidates(i,j).patches(1,:)';
    end
end

% the result of just applying the iid best matches
Im_high = mergePatches(PatchesIID,setting.Para.OverlapSize,width,height);
figure;imshow(composeImage(Im_high+im_low,im_color));
imwrite(composeImage(Im_high+im_low,im_color),[filename '_iid.jpg'],'quality',100);
imwrite(Im_high+0.5,[filename '_idd_highfrequency.jpg'],'quality',100);
