function s = hmaxdm_score(jname)

if dm_exist(fullfile(jname, 'error'))
    s = 0;
    return;
end

p = dm_read(fullfile(jname, 'pred'));

catIDs = unique(p.d.catID);
s = zeros(1, numel(catIDs));
for i = 1 : numel(catIDs)
    catID = catIDs(i);
    s(i) = mean(p.predIDs(p.d.catID == catID) == catID);
end

s = mean(s);

return;