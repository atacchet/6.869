function v = hmaxdm_v(m, gs)

if (nargin < 2) || isempty(gs), gs = 1 : numel(m.groups); end

v = single([]);

for g = gs

    ind = m.groups{g}.rIndexes(:);
    if isempty(ind), continue; end

    rzs = cns('get', -g, 'val');
    for i = 1 : numel(rzs)
        rzs{i} = rzs{i}(:);
    end
    rg = cat(1, rzs{:});

    if ~isequal(ind, inf), rg = rg(ind); end

    v = [v; rg];

end

return;