function [nc] = hmaxdm_numcell(p, lib, g)
%
% Returns the number of cells given a 'g'
% Conctact: {jmutch, pavan_m}@mit.edu
% Warning: doesn't work with color
%

m  = hmax.Model (p, lib);
nf = m.layers {m.groups{g}.zs(1)}.size{1};
nl = hmaxdm_numloc (m, g);
nc = nf * nl;
