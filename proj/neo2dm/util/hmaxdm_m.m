function m = hmaxdm_m(p, lib, varargin)

if ~isfield(p, 'package'), p.package = 'hmax'; end  % For older jobs.

if nargin < 2, lib = struct; end

m = cns_call(p, 0, 'Model', lib, varargin{:});

for g = 1 : numel(m.groups)
    try
        ind = lib.groups{g}.rIndexes;
        if isequal(ind, 0), ind = []; end  % For older jobs.
    catch
        ind = [];
    end
    m.groups{g}.rIndexes = ind;
end

end