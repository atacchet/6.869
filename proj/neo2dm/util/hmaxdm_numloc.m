function numLoc = hmaxdm_numloc(m, gs)

	numLoc = 0;

	for g = gs
	    for z = m.groups{g}.zs
	        numLoc = numLoc + prod([m.layers{z}.size{2 : 3}]);
	    end
	end

return;
