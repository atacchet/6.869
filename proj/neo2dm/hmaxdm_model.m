classdef hmaxdm_model

%-----------------------------------------------------------------------------------------------------------------------

properties
    p;
    g;
    lib   = struct;
    class = [];
    catNames;
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function T = Copy(T, M)

T.p     = M.p;
T.g     = M.g;
T.lib   = M.lib;
T.class = M.class;
T.catNames = M.catNames;

end
end

%-----------------------------------------------------------------------------------------------------------------------

end
