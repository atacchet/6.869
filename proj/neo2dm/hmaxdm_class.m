classdef hmaxdm_class < hmaxdm_model & dm_task_batch

%-----------------------------------------------------------------------------------------------------------------------

properties (Constant, GetAccess = private)
    dm_args = {'hmaxjob_model', 'hmaxjob_vect'};
end

%-----------------------------------------------------------------------------------------------------------------------

properties (Transient, Hidden)
    args;
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function T = DMStart(T, M, V)

T = T.Copy(M);

T.args.printf = @dm_printf;

[T.class, T.dm_numItems, T.dm_batchSize] = linclass.Train_Start(T.args, V.d.catID, V.v);
    
end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function r = DMItem(T, itemNo, varargin)

r = T.class.Train_Item(itemNo);

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function T = DMCombine(T, r, varargin)

T.class = T.class.Train_Combine(r);

end
end

%-----------------------------------------------------------------------------------------------------------------------

end