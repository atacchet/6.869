function [P, F, C] = neo2_hmax_readmodel(model)

jobdir = fullfile(workdir, model);
readClass = (nargout >= 3);

[P, F, C] = Version4(jobdir, readClass);


if ~isfield(P.p, 'package'), P.p.package = 'hmax'; end

for i = 1 : numel(F.lib.groups)
    if isempty(F.lib.groups{i}.rIndexes), F.lib.groups{i}.rIndexes = 0; end
end

return;

%***********************************************************************************************************************

function [P, F, C] = Version4(jobdir, readClass)

% Jim's HMAXDM + Pavan's neo2dm new script

t = load(fullfile(jobdir, 'choose.mat'));

P.p   = t.p;
P.g   = t.g;
F.lib = t.lib;

t = load(fullfile(jobdir, 'start.mat'));

P.catNames = t.catNames;

C.class = [];
return;


