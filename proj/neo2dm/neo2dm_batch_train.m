classdef neo2dm_batch_train < dm_task_single

%-------------------------------------------------------
properties 
    x_bigarray_train_filename
    y_bigarray_train_filename
    opt
end

%-------------------------------------------------------
methods 
function T = DMBuild(T, varargin)
    
    trainVectors = bigarray.Obj(T.x_bigarray_train_filename);
    trainCats = bigarray.Obj(T.y_bigarray_train_filename);

    trainVectors.Transpose(true);
    
    trainCats.Transpose(true);

    % train classifier
    gurls (trainVectors, trainCats, T.opt, 1);
    
end
end
end
