classdef neo2dm_bazscore_test < dm_task_single

%-------------------------------------------------------
properties 
    x_bigarray_test_filename
    zscore_filename
end

%-------------------------------------------------------
methods 
function T = DMBuild(T, varargin)
    
    zscore = load(T.zscore_filename);
    
    testVectors = bigarray.Obj(T.x_bigarray_test_filename);
    
    % Overides testing set
    ba_zscore(testVectors, zscore.fMeans, zscore.fStds);
    testVectors.Flush();
    
end
end
end
