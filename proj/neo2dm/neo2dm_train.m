classdef neo2dm_train < hmaxdm_model & dm_task_single
    properties(Constant, GetAccess = private)
        dm_args = {'neo2dm_cfrparams'};
    end
    properties
        groups
    end
    
    methods
        function [T] = DMBuild(T, P, varargin)
            batch_opt = P.batch_opt;
            workpath  = P.workpath;

	    existingjobs = dir(fullfile(workpath,'*.dm.mat'));
            
            c = struct;
            c.dm_name = 'zscore';
            c.dm_class = 'neo2dm_bazscore';	
            c.x_bigarray_train_filename = batch_opt.x_bigarray_train_filename;
            T.groups{end+1} = c;
                  
            c = struct;
            c.dm_name = 'drls_split';
            c.dm_class = 'batch_splitho';
            c.dm_depends = {'zscore'};
            c.x_bigarray_filename = batch_opt.x_bigarray_train_filename;
            c.y_bigarray_filename = batch_opt.y_bigarray_train_filename;
            c.xva_bigarray_filename = batch_opt.xva_bigarray_filename;
            c.yva_bigarray_filename = batch_opt.yva_bigarray_filename;
            c.proportion = 0.2;
            T.groups{end+1} = c;
            
            c = struct; 
            c.dm_name = 'drls_XtX';
            c.dm_class = 'bigarray_product';
            c.dm_depends = {'drls_split'}; 
            c.U_filename = batch_opt.x_bigarray_train_filename;
            c.V_filename = batch_opt.x_bigarray_train_filename;   
            T.groups{end+1} = c;
	
            c = struct;
            c.dm_name = 'drls_Xty';
            c.dm_class = 'bigarray_product';
            c.dm_depends = {'drls_split'};
            c.U_filename = batch_opt.x_bigarray_train_filename;
            c.V_filename = batch_opt.y_bigarray_train_filename;
            T.groups{end+1} = c;	
            

            c = struct;
            c.dm_name = 'drls_XvatXva';
            c.dm_class = 'bigarray_product';
            c.dm_depends = {'drls_split'};
            c.U_filename = batch_opt.xva_bigarray_filename;
            c.V_filename = batch_opt.xva_bigarray_filename;
            T.groups{end+1} = c;
                        
            c = struct;
            
            c.dm_name = 'drls_Xvatyva';
            c.dm_class = 'bigarray_product';
            c.dm_depends = { 'drls_split'};
            c.U_filename = batch_opt.xva_bigarray_filename;
            c.V_filename = batch_opt.yva_bigarray_filename;
            T.groups{end+1} = c;
            
            c = struct;
            
            c.dm_name = 'drls_train';
            c.dm_class = 'neo2dm_batch_train';
            c.dm_depends = {'drls_XtX','drls_XvatXva','drls_Xty','drls_Xvatyva'};
            c.x_bigarray_train_filename = batch_opt.x_bigarray_train_filename;
            c.y_bigarray_train_filename = batch_opt.y_bigarray_train_filename;
            c.opt = batch_opt.train_opt;
            T.groups{end+1} = c;

            dm_create(workpath,T.groups);

		%% TEMPROARY FIX - pavan
		% wait for 2 seconds and update
	   pause(2);
	   for i = 1:numel(existingtasks)
	   end

        end
    end
    
end
