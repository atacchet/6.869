classdef neo2dm_model_nodep < dm_task_single  & hmaxdm_model

	properties
		jobname;
		date
		predstat
		predstatorder;
		choosefilename;
	end

	methods
		function T = DMBuild(T, varargin)
			T.date = now;
			C = load(T.choosefilename);

			T.p = C.p;
			T.g = C.g;
			T.lib = C.lib;

			T.catNames = C.catNames;

			[tp,tf,tc] =  neo2_hmax_readmodel_nodep(T.jobname);
			T.class      = tc.class;

			%stats = trainpredstat(T.jobname,{@mean, @median, @std});

			%T.predstat(1,:) = stats.mean.pos;
			%T.predstat(2,:) = stats.mean.neg;
			%T.predstat(3,:) = stats.median.pos;
			%T.predstat(4,:) = stats.median.neg;
			%T.predstat(5,:) = stats.std.pos;
			%T.predstat(6,:) = stats.std.neg;

			%T.predstatorder = {'posmean','negmean','posmedian','negmedian','posstd','negstd'};
		end
	end
end
