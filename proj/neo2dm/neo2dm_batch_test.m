classdef neo2dm_batch_test < dm_task_single

%-------------------------------------------------------
properties 
    x_bigarray_test_filename
    y_bigarray_test_filename
    gurlsfile
    opt
end

%-------------------------------------------------------
methods 
function T = DMBuild(T, varargin)
    
    load(T.gurlsfile);
    T.opt = opt;
    
    testVectors = bigarray.Obj(T.x_bigarray_test_filename);
    testCats = bigarray.Obj(T.y_bigarray_test_filename);

    testVectors.Transpose(true);
    
    testCats.Transpose(true);

    % test classification
    gurls (testVectors, testCats, T.opt, 2);
    
end
end
end
