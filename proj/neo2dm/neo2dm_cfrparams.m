classdef neo2dm_cfrparams < dm_task_const
    properties
        workpath
        gurlsfile
        batch_opt
    end
    
    
    methods
        function [T] = DMBuild(T)
            T.batch_opt = drls_settings(T.workpath, T.gurlsfile);
            
            workpath = T.workpath;
            name = fullfile(workpath,T.gurlsfile);
            
            % Classification parameters
            
            train_opt = defopt(name);
            train_opt.nb_pred = 1;
            train_opt.hoperf = @perf_flatcost;
            train_opt.singlelambda = @median;
            
            % XtX and Xty information
            
            train_opt.kernel.type = 'primal_K_already_computed';
            train_opt.kernel.XtX_filename = fullfile(workpath, 'drls_XtX');
            train_opt.kernel.Xty_filename = fullfile(workpath, 'drls_Xty');
            train_opt.kernel.XvatXva_filename = fullfile(workpath, 'drls_XvatXva');
            train_opt.kernel.Xvatyva_filename = fullfile(workpath, 'drls_Xvatyva');
            train_opt.split.Xva_filename = T.batch_opt.xva_bigarray_filename;
            train_opt.split.yva_filename = T.batch_opt.yva_bigarray_filename;
            train_opt.transposedY_bigarray = fullfile(workpath, 'big/transposedY');
            train_opt.hoproportion = 0.2;
            train_opt.nholdouts = 1;
            % Gurls sequence
            train_opt.seq = {'paramsel:dhoprimal','rls:dprimal','pred:dprimal','perf:flatcost'};  
            train_opt.process{1} = [2,2,0,0];
            train_opt.process{2} = [3,3,2,2];
            T.batch_opt.train_opt = train_opt;
        end
    end
end
