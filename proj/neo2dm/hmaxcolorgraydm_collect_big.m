classdef hmaxcolorgraydm_collect_big < hmaxcolorgraydm_vect & dm_task_batch

%-----------------------------------------------------------------------------------------------------------------------

methods
function T = DMStart(T, varargin)

T.dm_array = 0;

T = T.DMStart@hmaxcolorgraydm_vect(varargin{:});

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function T = DMCombine(T, b, varargin)

T.v = b;

end
end

%-----------------------------------------------------------------------------------------------------------------------

end
