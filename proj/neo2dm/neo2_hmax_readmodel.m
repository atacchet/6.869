function [P, F, C] = neo2_hmax_readmodel(model)

jobdir = fullfile(workdir, model);
readClass = (nargout >= 3);

if dm_exist(fullfile(jobdir, 'choose'))

    [P, F, C] = Version4(jobdir, readClass);

else

    p = dm_read(fullfile(jobdir, 'params'));

    if dm_exist(fullfile(jobdir, 'zscore'))
        [P, F, C] = Version3(jobdir, p, readClass);
    elseif isfield(p, 'p')
        [P, F, C] = Version2(jobdir, p, readClass);
    else
        [P, F, C] = Version1(jobdir, p, readClass);
    end

end

if ~isfield(P.p, 'package'), P.p.package = 'hmax'; end

for i = 1 : numel(F.lib.groups)
    if isempty(F.lib.groups{i}.rIndexes), F.lib.groups{i}.rIndexes = 0; end
end

return;

%***********************************************************************************************************************

function [P, F, C] = Version4(jobdir, readClass)

% Jim's HMAXDM + Pavan's neo2dm new script

t = load(fullfile(jobdir, 'choose.mat'));

P.p   = t.p;
P.g   = t.g;
F.lib = t.lib;

t = load(fullfile(jobdir, 'start.mat'));

P.catNames = t.catNames;

if readClass

    rls_file = 'gurls'; 
    t = dm_read(fullfile(jobdir, rls_file));
    c.W = t.opt.rls.W';

    t = dm_read(fullfile(jobdir, 'zscore'));
    c.fMeans = feval(class(c.W), t.fMeans);
    c.fStds  = feval(class(c.W), t.fStds );

    c.numCats = size(c.W, 1);
    c.oaa     = true;
    c.prob    = false;
    c.b       = zeros(size(c.W, 1), 1, class(c.W));

    C.class = c;

else

    C.class = [];

end

return;

%***********************************************************************************************************************

function [P, F, C] = Version3(jobdir, p, readClass)

% Pavan's and Andrea's new script

P = p;

% Ugly fix to save today's test by USC. -- Pavan
if not (isfield(p, 'catNames'))
	v = vex_load(p.vexloc);
	P.catNames = v.catNames;
end

F = dm_read(fullfile(jobdir, 'feat'));

if readClass

    rls_file = 'gurls'; % TODO: need to infer this
    t = dm_read(fullfile(jobdir, rls_file));
    c.W = t.opt.rls.W';

    t = dm_read(fullfile(jobdir, 'zscore'));
    c.fMeans = feval(class(c.W), t.fMeans);
    c.fStds  = feval(class(c.W), t.fStds );

    c.numCats = size(c.W, 1);
    c.oaa     = true;
    c.prob    = false;
    c.b       = zeros(size(c.W, 1), 1, class(c.W));

    C.class = c;

else

    C.class = [];

end

return;

%***********************************************************************************************************************

function [P, F, C] = Version2(jobdir, p, readClass)

% hmaxjob, using new DM (post 2011/03/19).

P = p;

F = dm_read(fullfile(jobdir, 'feat'));

if readClass

    C = dm_read(fullfile(jobdir, 'class'));

else

    C.class = [];

end

return;

%***********************************************************************************************************************

function [P, F, C] = Version1(jobdir, p, readClass)

% hmaxjob, using old DM (pre 2011/03/19).

f = dm_read(fullfile(jobdir, 'feat'));

if readClass
    c = dm_read(fullfile(jobdir, 'class'));
else
    c = [];
end

if strcmp(p.groups{1}.type, 'ri')

    % Old HMAX (pre 2011/03/05).

    for g = 1 : numel(p.groups)
        n = p.groups{g}.name;
        t = p.groups{g}.type;
        [n, t] = ConvType(n, t);
        p.groups{g}.name = n;
        p.groups{g}.type = t;
    end

    dg = 0;
    for g = 1 : numel(f.groups)
        d = f.groups{g};
        if isempty(d) || ~isstruct(d) || ~isfield(d, 'fSizes'), continue; end
        if dg > 0, error('more than one dictionary, cannot convert'); end
        dg = g;
    end
    if dg > 0
        [f.groups{dg}, c] = ConvDict(f.groups{dg}, c);
        p.groups{dg}.fCount = numel(f.groups{dg}.fSizes);
    end

end

P.p.groups = p.groups;
P.p.quiet  = true;
P.g        = cns_groupno(p);
P.catNames = p.s.catNames;
F.lib      = f;
C.class    = c;

return;

%-----------------------------------------------------------------------------------------------------------------------

function [n, t] = ConvType(n, t)

switch t
case 'ri', t = 'input'; n = 'input';
case 'si', t = 'scale'; n = 'scale';
case 'ndp', t = 'sdNDP';
case 'max', t = 'cMax';
case 'sndp', t = 'sdNDP';
end

return;

%-----------------------------------------------------------------------------------------------------------------------

function [d, c] = ConvDict(d, c)

% Number of features must have lots of factors of 2.
n0 = numel(d.fSizes);
n1 = n0;
while true
    fact = factor(n1);
    if prod(fact(fact == 2)) >= sqrt(n1), break; end
    n1 = n1 + 1;
end
nd = n1 - n0;
if nd == 0, return; end

inds = [ones(1, nd), 1 : n0];
dups = 1 : nd;

d = hmax_s.SelectFeatures([], [], d, inds);

if isempty(c)
    fprintf('warning: added %u copies of first feature\n', nd);
    return;
end

r = numel(c.fMeans) / n0;
if mod(r, 1) ~= 0, error('cannot convert classifier'); end
inds2 = [];
dups2 = [];
for i = 1 : r
    inds2 = [inds2, inds + (i - 1) * n0];
    dups2 = [dups2, dups + (i - 1) * n1];
end

c.W      = c.W     (:, inds2); c.W     (:, dups2) = 0;
c.fMeans = c.fMeans(inds2, 1); c.fMeans(dups2, 1) = 0;
c.fStds  = c.fStds (inds2, 1); c.fStds (dups2, 1) = 1;

return;
