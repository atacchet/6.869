classdef hmaxcolorgraydm_vect

%-----------------------------------------------------------------------------------------------------------------------

properties (Constant, GetAccess = private)
    dm_args = {'hmaxdm_model'};
end

%-----------------------------------------------------------------------------------------------------------------------

properties
    d;
    v;
end

properties (Transient, Hidden)
    gs;
    m;
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function T = DMStart(T, varargin)

T.dm_numItems = T.d.NumItems;

if isempty(T.dm_batchSize), T.dm_batchSize = 200; end

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function T = DMOpen(T, M)

T.m = hmaxdm_m(M.p, M.lib);

cns('init', T.m, 'gpu');

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function r = DMItem(T, itemNo, varargin)

cns_call(T.m, 0, 'LoadImageColor', T.d.Item(itemNo));
cns_call(T.m, 0, 'LoadImageGray', T.d.Item(itemNo));

cns('run');
r = hmaxdm_v(T.m, T.gs);
end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function T = DMClose(T)

cns('done');

T.m = [];

end
end

%-----------------------------------------------------------------------------------------------------------------------

end
