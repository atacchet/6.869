function [dSample, dTrain, dTest] = neo2_splits(s)
% neo2_splits outputs three vex objects dSample, dTrain and dTest.
%
% INPUT:
%    train  : file containing USC training polygons
%    test   : file containing USC testing  polygons OR a number between (0.0 - 1.0)
%
% OUTPUT:
%    dSample is a vex object that is used for sampling the dictionary
%    dTrain is used for training
%    dTest  is used for testing
%

if isfield(s,'fraction')
	acceptrate = s.fraction;
else
	acceptrate = 1;
end

[dSample, catNames, USCLabelCode] = construct_vex_test(s.vexloc{1}, s.image_base, s.anno_file{1}, s.winlowlim, s.imReturnType, acceptrate);
dTrain = dSample;
[dTest, ~, ~] = construct_vex_test(s.vexloc{2}, s.image_base, s.anno_file{2}, s.winlowlim, s.imReturnType, acceptrate, catNames, USCLabelCode);




%[train, rest] = vex_catselect([]  , d.catID, nTrain);
%test          = vex_catselect(rest, d.catID, nTest );

%sample = vex_catsort(train, d.catID);

%dSample = d.Copy(sample);
%dTrain  = d.Copy(train );
%dTest   = d.Copy(test  );

return;
