classdef hmaxgraydm_sample < hmaxdm_model & dm_task_batch

%-----------------------------------------------------------------------------------------------------------------------

properties (Constant, GetAccess = private)
    dm_args      = {'hmaxdm_model'};
    dm_dim_fVals = 3;
end

%-----------------------------------------------------------------------------------------------------------------------

properties (Transient, Hidden)

    d;
    sg;
    useMask = false;
    numSamples;
    k = [];

    catIDs;
    catCounts;
    m;

end

%-----------------------------------------------------------------------------------------------------------------------

methods
function T = DMStart(T, M)

if ~isempty(T.class) || ~isempty(M.class), error('classifier already exists'); end

T.catIDs = unique(T.d.catID);

nc = numel(T.catIDs);

perCat = floor(T.numSamples / nc);
extra = randperm(nc);
extra = extra(1 : T.numSamples - perCat * nc);
T.catCounts = repmat(perCat, 1, nc);
T.catCounts(extra) = T.catCounts(extra) + 1;

T.dm_numItems  = nc;
T.dm_batchSize = max(floor(nc / 50), 1); % Want around 50 batches.  No more than 100.

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function T = DMOpen(T, M)

T.m = hmaxdm_m(M.p, M.lib);

cns('init', T.m, 'gpu');

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function r = DMItem(T, itemNo, M)

catID  = T.catIDs   (itemNo);
needed = T.catCounts(itemNo);

inds = find(T.d.catID == catID);
if isempty(inds), error('no images for category %u', catID); end
inds = inds(randperm(numel(inds)));
total = numel(inds);

r = cns_call(T.m, -T.sg, 'EmptyDict');

while needed > 0

    im = T.d.Item(inds(1));
    if T.useMask
        mask = T.d.Item(inds(1), 'mask');
    else
        mask = [];
    end
    if ~isempty(mask)
        % Assumes scaled image group name is 'scale'.
        mask = (cns_resizeimage(double(mask), M.p.groups{M.g.scale}.baseSize) >= 0.1);
    end
    extra.mask = mask;
    extra.rad  = 0.05; % TODO: make a parameter

    %TODO: the imresize is done here to compensate that scale problem 
    %im = imresize(im, [300 300]);
    cns_call(T.m, 0, 'LoadImageGray', im);
    cns('run');

    try
        r = cns_call(T.m, -T.sg, 'SampleFeatures', r, 1, extra);
        needed = needed - 1;
        inds = [inds(2 : end), inds(1)];
    catch
        dm_printf('unable to sample from "%s"\n', T.d.Desc(inds(1)));
        inds = inds(2 : end);
        if numel(inds) <= 0.75 * total
            error('unable to sample features for category %u', catID);
        end
    end

end

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function T = DMClose(T)

cns('done');

T.m = [];

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function T = DMCombine(T, r, M)

if ~isempty(T.k)

    if any(diff(r.fSizes)), error('kmeans requires a single feature size'); end

    siz = size(r.fVals);
    siz(end + 1 : 4) = 1;
    v = double(reshape(r.fVals, [prod(siz(1 : 3)), siz(4)])');
    [ans, v] = kmeans(v, T.k, 'Display', 'iter');
    r.fVals = reshape(single(v)', [siz(1 : 3), T.k]);

    r.fSizes = r.fSizes(1 : T.k);
    r.fSPos  = repmat(1  , 1, T.k);
    r.fYPos  = repmat(0.5, 1, T.k);
    r.fXPos  = repmat(0.5, 1, T.k);

end

r = cns_call(M.p, -T.sg, 'SortFeatures', r);

if cns_istype(M.p, -T.sg, 'ss')
    r = cns_call(M.p, -T.sg, 'SparsifyDict', r);
end

T = T.Copy(M);

T.lib.groups{T.sg} = r;

end
end

%-----------------------------------------------------------------------------------------------------------------------

end
