function [cfrtasks] = neo2dm_cfrtasks(workpath, gurlsfile, n, d)

		if n > 5e4 && d > 5e4
			error('Online classifier not yet implemented');
		end	


		if n > d


            T.batch_opt = drls_settings(workpath, gurlsfile);
            
            name = fullfile(workpath,gurlsfile);
            
            % Classification parameters
            
            train_opt = defopt(name);
            train_opt.nb_pred = 1;
            train_opt.hoperf = @perf_flatcost;
            train_opt.singlelambda = @median;
            
            % XtX and Xty information
            
            train_opt.kernel.type = 'primal_K_already_computed';
            train_opt.kernel.XtX_filename = fullfile(workpath, 'drls_XtX');
            train_opt.kernel.Xty_filename = fullfile(workpath, 'drls_Xty');
            train_opt.kernel.XvatXva_filename = fullfile(workpath, 'drls_XvatXva');
            train_opt.kernel.Xvatyva_filename = fullfile(workpath, 'drls_Xvatyva');
            train_opt.split.Xva_filename = T.batch_opt.xva_bigarray_filename;
            train_opt.split.yva_filename = T.batch_opt.yva_bigarray_filename;
            train_opt.transposedY_bigarray = fullfile(workpath, 'big/transposedY');
            train_opt.hoproportion = 0.2;
            train_opt.nholdouts = 1;
			train_opt.nb_pred = 1;
            % Gurls sequence
            train_opt.seq = {'paramsel:dhoprimal','rls:dprimal','predstat:dprimal','pred:dprimal','perf:flatperclass'};  
            train_opt.process{1} = [2,2,2,0,0];
            train_opt.process{2} = [3,3,3,2,2];
            T.batch_opt.train_opt = train_opt;

	    	[cfrtasks] = neo2dm_cfrtasks_internal_primal(T.batch_opt, workpath);

		else	
			batch_opt = drls_settings(workpath, gurlsfile);	

	
			%% Classification parameters
			
			name = fullfile(workpath,gurlsfile);
			train_opt = defopt(name);
			train_opt.hoperf = @perf_dprecrec;
			train_opt.singlelambda = @median;
			
			% XtX and Xty information
			
			train_opt.transposedY_bigarray = fullfile(workpath, 'big/transposedY');
			train_opt.hoproportion = 0.2;
			train_opt.nholdouts = 1;
			% Gurls sequence
		
			train_opt.seq = {'kernel:biglin','split:dho','paramsel:hodual','rls:ddual','pred:dprimal','perf:dprecrec'};
			train_opt.process{1} = [2,2,2,2,0,0];
			train_opt.process{2} = [3,3,3,3,2,2];
		
			%%
			
			batch_opt.train_opt = train_opt;
			[cfrtasks] = neo2dm_cfrtasks_internal_dual(batch_opt);
		end	
end


function [groups] = neo2dm_cfrtasks_internal_primal(batch_opt, workpath)
	    T = struct;
	    T.groups = {};

            c = struct;
            c.dm_name = 'zscore';
            c.dm_class = 'neo2dm_bazscore';	
            c.dm_depends = {'trainX'};
            c.x_bigarray_train_filename = batch_opt.x_bigarray_train_filename;
            T.groups{end+1} = c;
                  
            c = struct;
            c.dm_name = 'drls_split';
            c.dm_class = 'batch_splitho';
            c.dm_depends = {'zscore'};
            c.x_bigarray_filename = batch_opt.x_bigarray_train_filename;
            c.y_bigarray_filename = batch_opt.y_bigarray_train_filename;
            c.xva_bigarray_filename = batch_opt.xva_bigarray_filename;
            c.yva_bigarray_filename = batch_opt.yva_bigarray_filename;
            c.proportion = 0.2;
            T.groups{end+1} = c;
            
            c = struct; 
            c.dm_name = 'drls_XtX';
            c.dm_class = 'bigarray_product';
            c.dm_depends = {'drls_split'}; 
            c.U_filename = batch_opt.x_bigarray_train_filename;
            c.V_filename = batch_opt.x_bigarray_train_filename;   
			c.dm_holdTime = 219600;
            T.groups{end+1} = c;
	
            c = struct;
            c.dm_name = 'drls_Xty';
            c.dm_class = 'bigarray_product';
            c.dm_depends = {'drls_split', 'trainY'};
            c.U_filename = batch_opt.x_bigarray_train_filename;
            c.V_filename = batch_opt.y_bigarray_train_filename;
			c.dm_holdTime = 219600;
            T.groups{end+1} = c;	
            

            c = struct;
            c.dm_name = 'drls_XvatXva';
            c.dm_class = 'bigarray_product';
            c.dm_depends = {'drls_split'};
            c.U_filename = batch_opt.xva_bigarray_filename;
            c.V_filename = batch_opt.xva_bigarray_filename;
			c.dm_holdTime = 219600;
            T.groups{end+1} = c;
                        
            c = struct;
            
            c.dm_name = 'drls_Xvatyva';
            c.dm_class = 'bigarray_product';
            c.dm_depends = { 'drls_split'};
            c.U_filename = batch_opt.xva_bigarray_filename;
            c.V_filename = batch_opt.yva_bigarray_filename;
			c.dm_holdTime = 219600;
            T.groups{end+1} = c;
            
            c = struct;
            
            c.dm_name = 'drls_train';
            c.dm_class = 'neo2dm_batch_train';
            c.dm_depends = {'drls_XtX','drls_XvatXva','drls_Xty','drls_Xvatyva'};
            c.x_bigarray_train_filename = batch_opt.x_bigarray_train_filename;
            c.y_bigarray_train_filename = batch_opt.y_bigarray_train_filename;
            c.opt = batch_opt.train_opt;
            T.groups{end+1} = c;

	   groups = T.groups;

end


function [groups] = neo2dm_cfrtasks_internal_dual(batch_opt)
	
	groups = {};
	 
	%-------------------------------------------------------
	c = struct;
    c.dm_name = 'zscore';
    c.dm_class = 'neo2dm_bazscore';	
    c.dm_depends = {'trainX'};
    c.x_bigarray_train_filename = batch_opt.x_bigarray_train_filename;
    groups{end+1} = c;

	%-------------------------------------------------------
    c = struct;
    
    c.dm_name = 'drls_train';
    c.dm_class = 'neo2dm_batch_train';
    c.dm_depends = {'zscore'};
    c.x_bigarray_train_filename = batch_opt.x_bigarray_train_filename;
    c.y_bigarray_train_filename = batch_opt.y_bigarray_train_filename;
    c.opt = batch_opt.train_opt;
	groups{end+1} = c;



	c = struct;
end	
