classdef neo2dm_bazscore < dm_task_single

%-------------------------------------------------------
properties 
    x_bigarray_train_filename
    fMeans
    fStds
end

%-------------------------------------------------------
methods 
function T = DMBuild(T, varargin)
    
    trainVectors = bigarray.Obj(T.x_bigarray_train_filename);
    
    % Overides the training set
	[T.fMeans,T.fStds] = ba_zscore(trainVectors);
	trainVectors.Flush();
    
end
end
end
