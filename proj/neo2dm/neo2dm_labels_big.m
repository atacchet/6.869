classdef neo2dm_labels_big < dm_task_single

properties (Constant, GetAccess = private)
    dm_args = {'hmaxdm_collect_big'};
end

properties
    filename
    B;
end

methods

function T = DMBuild(T, P, varargin)
        y = P.d.dsLabels;
		y = y{1};
		T.B = bigarray_mat(T.filename);
		T.B.Init(P.v.BlockSize);
		T.B.Append(y);
		T.B.Flush();
end		

end

end
