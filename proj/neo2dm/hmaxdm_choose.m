classdef hmaxdm_choose < hmaxdm_model & dm_task_single

%-----------------------------------------------------------------------------------------------------------------------

properties (Constant, GetAccess = private)
    dm_args = {'hmaxdm_model'};
end

%-----------------------------------------------------------------------------------------------------------------------

properties (Transient, Hidden)
    gs;
    ns;
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function T = DMBuild(T, M)

if ~isempty(T.class) || ~isempty(M.class), error('classifier already exists'); end

T = T.Copy(M);

m = cns_call(M.p, 0, 'Model', M.lib);

for g = 1 : numel(M.p.groups)

    i = find(T.gs == g, 1);
    if isempty(i)
        rCount = 0;
    else
        rCount = T.ns(i);
    end

    if rCount == 0
        ind = [];
    elseif isinf(rCount)
        ind = inf;
    else
        numUnits = 0;
        for z = m.groups{g}.zs
            numUnits = numUnits + prod([m.layers{z}.size{:}]);
        end
        ind = randperm(numUnits);
        ind = sort(ind(1 : rCount));
    end

    T.lib.groups{g}.rIndexes = ind;

end

end
end

%-----------------------------------------------------------------------------------------------------------------------

end
