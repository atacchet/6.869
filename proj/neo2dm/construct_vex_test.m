function [d, catNames, USCLabelCode] = construct_vex(vexobj_location, image_base, anno_file, winlowlim, imReturnType, acceptrate, catNames, USCLabelCode)
	
	if nargin < 6
		acceptrate = 1;
	end	

	minWidth = winlowlim(1);
	minHeight = winlowlim(2);

	if exist(vexobj_location) == 2
			decision = getUserInput(vexobj_location);
			if decision 
				fprintf('Loading existing file %s .\n',vexobj_location);
				d = vex_load(vexobj_location);
				catNames = d.catNames;
				USCLabelCode = d.uscLabelCode;
				return;
			end	
	end		
	
	d = vex_dataset;
	t = load(anno_file);	
	if nargin < 8
		[t.d, USCLabelCode] = cleanLabels(t.d);
	else	
		t.d.catNames = catNames;
		inverseMap(USCLabelCode) = 1:numel(USCLabelCode);
		t.d.catNo = inverseMap(t.d.catNo);
	end	
	catNames = t.d.catNames;
	noCat = numel(t.d.catNames);
	code = 2*eye(noCat) - 1;
	ycode = code(t.d.catNo,:);



	j = 1;
	k = 1;
	inverseIndexing = zeros(size(ycode,1),1);
	for s = 1:size(ycode,2)
		numElemClass = sum(t.d.catNo == s);
		dsample = rand(numElemClass,1);
		for i = 1:numElemClass
			if dsample(i) <= acceptrate || i == 1 %always accept the first sample of each class.
				y{k} = ycode(j,:);
				catNo(k) = t.d.catNo(j);
				imName{k} = t.d.path{j};
				win{k} = t.d.win(:,j);
				status_update('construct_vex',['Copying fields  ' num2str(k) '/' num2str(round(acceptrate*size(ycode,1))) '\r']);
				inverseIndexing(j) = k;
				k = k+1;
			end	
			j = j+1;
		end
	end	
	fprintf('\n');
			 

	%for i = 1:size(ycode,1)
	%	if downsample(i) <= acceptrate
	%		y{i} = ycode(i,:);
	%		catNo(i) = t.d.catNo(i);
	%		imName{i} = t.d.path{i};
	%		win{i} = t.d.win(:,i);
	%		status_update('construct_vex',['Copying fields  ' num2str(i) '/' num2str(size(ycode,1)) '\r']);
	%	end	
	%end		

	status_update('construct_vex','Adding properties');
	d.AddProp('imBase',		image_base,		'path');
	d.AddProp('dataType', 					'RGB');
	d.AddProp('returnType',					imReturnType);
	d.AddProp('numCats',		 			max(t.d.catNo));
	d.AddProp('cCodes', 					code);
	d.AddProp('uscLabelCode',				USCLabelCode);
	d.AddProp('catNames',					t.d.catNames);

	d.AddField('catNo',						catNo);
    d.AddField('catID',						catNo);
	d.AddField('imFileName', 				imName);
	d.AddField('win',						win);
	
	d.AddForm('imFullPath',		@(x,i) ...
		{fullfile(x.imBase,x.imFileName{i})});
	d.AddForm('imLabel'	,		@(x,i) {x.cCodes(:,x.catNo(i))});	
	d.AddForm('dsLabels',		@(x) {x.cCodes(:,x.catNo)});
	d.AddForm('object'	,		@(x,i) {getWinPix(x,i)});
	d.AddForm('item'	,		@(x) x.object);

	% Clean up Laurent Mess
	jdx = cleanWindows(t,d,minHeight,minWidth);
	jdx = inverseIndexing(jdx);
	jdx = jdx(jdx > 0);
	d.DeleteItems(jdx);
	%

	origIndex = 1:d.NumItems;

	d.AddField('origIndex',				origIndex);

% 	switch (splittype)
% 		case {'auto'}
% 			split = getSplits(d);
% 		case {'tr'}
% 			split{1}.tr = {1:d.NumItems};
% 			split{1}.te = {};
% 		case {'te'}
% 			split{1}.tr = {};
% 			split{1}.te = {1:d.NumItems};
% 		otherwise
% 			error('Unknown split type\n');
% 	end
% 
% 	d.AddProp('split',					split{1});
	status_update('construct_vex',['saving' vexobj_location]);
	vex_save(vexobj_location,d);
end

