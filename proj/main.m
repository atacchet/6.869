% -------------------------------------------------------------------------------
% Do super resolution given class priors
% The image structs must have been computed beforehand
% -------------------------------------------------------------------------------
% Input :
% Output :
% -------------------------------------------------------------------------------

% Initialize parameters, folders, librairies
addpath('src')
params = sr_initialize;

% Load training data and class probabilities
% and build dictionary and kdtree 
dictionary = sr_get_dictionary(params);

% Load test images
test_images = sr_get_test_images(params);
ids_to_process = [1:5 801:806 864:869 886:890 1291:1296 1700:1705 1769:1776 1827:1828];

fprintf('\n\n-- Computing up-res\n')
% Loop through test images and do super-res
for im_ind = ids_to_process;
    [path,filename, ~] = fileparts(test_images{im_ind});
    [~,classname,~] = fileparts(path);

    fprintf('-- Loading image struct %s\n',[classname '_' filename])
    structPath = fullfile(params.path.imStruct,[classname '_' filename '_struct.mat']);
    im_struct = load(structPath);
    fprintf('-- Adding candidate patches\n')
    candidates = load(fullfile(params.path.output,im_struct.im_name,[im_struct.im_name '_candidates.mat']));
    im_struct.candidates = candidates.candidates;

    % Compute and save dumb baseline NN reconstruction

    % Compute compatibility function of the MRF
    im_struct = sr_compute_compatibilities(im_struct,dictionary,params);

    % Run BP
    im_struct = sr_run_bp(im_struct,dictionary,params);
    fprintf('\n')

    name = im_struct.im_name;
    fprintf('-- Repeating without probability prior')
    %Repeat with no proba
    params.probScale = 0;
    % Compute compatibility function of the MRF
    im_struct = sr_compute_compatibilities(im_struct,dictionary,params);
    % Run BP
    im_struct = sr_run_bp(im_struct,dictionary,params);
    fprintf('\n')
    
    fprintf('-- Repeating with low prior')
    %Repeat with no proba
    params.probScale = 1;
    % Compute compatibility function of the MRF
    im_struct = sr_compute_compatibilities(im_struct,dictionary,params);
    % Run BP
    im_struct = sr_run_bp(im_struct,dictionary,params);
    fprintf('\n')

    fprintf('-- Repeating with high prior')
    %Repeat with no proba
    params.probScale = 100;
    % Compute compatibility function of the MRF
    im_struct = sr_compute_compatibilities(im_struct,dictionary,params);
    % Run BP
    im_struct = sr_run_bp(im_struct,dictionary,params);
    fprintf('\n')
end % test img loop
