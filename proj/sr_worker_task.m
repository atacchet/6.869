fprintf('-- Adding path dependencies')
tic;
jobFileName = 'output/jobs/job_file.mat';
addpath('src')
try
   run('../vlfeat/toolbox/vl_setup')
catch
    fprintf('ERROR : Missing vlfeat toolbox\n')
    return
end
try
    run('GURLS/biggurls/utils/biggurls_install.m');
catch
    fprintf('ERROR : Missing BIGGURLS toolbox\n')
    return
end
t=toc;
fprintf(' - done in %fs\n',t)

admMRFRetrieveRun(jobFileName);
