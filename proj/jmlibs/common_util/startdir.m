function path = startdir

path = fileparts(which('startup'));

if isempty(path)
    error('cannot find startup.m');
end

return;