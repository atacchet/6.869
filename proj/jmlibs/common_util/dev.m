function varargout = dev(command, varargin)

path = cd(devdir);

if nargin > 0
    try
        [varargout{1 : nargout}] = evalin('caller', [command sprintf(' %s', varargin{:})]);
    catch
        cd(path);
        rethrow(lasterror);
    end
    cd(path);
end

return;
