function varargout = data(command, varargin)

path = cd(datadir);

if nargin > 0
    try
        [varargout{1 : nargout}] = evalin('caller', [command sprintf(' %s', varargin{:})]);
    catch
        cd(path);
        rethrow(lasterror);
    end
    cd(path);
end

return;
