function [ C b ] = CholeskyCB( R, W, Y)
if isscalar(W)
    W = ones(size(Y, 1), 1)*W;
end
r = R\(R'\W);
b = sum(Y.*r)/sum(W.*r);
C = W.*(R\(R'\(Y - b.*W)));
end

