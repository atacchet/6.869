function varargout = linclass(mode, varargin)

switch mode
case 'build'  , [varargout{1 : nargout}] = Build  (varargin{:});
case 'start'  , [varargout{1 : nargout}] = Start  (varargin{:});
case 'item'   , [varargout{1 : nargout}] = Item   (varargin{:});
case 'combine', [varargout{1 : nargout}] = Combine(varargin{:});
case 'value'  , [varargout{1 : nargout}] = Value  (varargin{:});
case 'prob'   , [varargout{1 : nargout}] = Prob   (varargin{:});
case 'votes'  , [varargout{1 : nargout}] = Votes  (varargin{:});
case 'score'  , [varargout{1 : nargout}] = Score  (varargin{:});
case 'best'   , [varargout{1 : nargout}] = Best   (varargin{:});
end

return;

%***********************************************************************************************************************

function s = Build(c)

c.printf = @fprintf;

c = Start(c);

for i = 1 : c.count

    r = Item(c, i);

    c.printf('%u/%u: %s\n', i, c.count, r.desc);
    r = rmfield(r, 'desc');

    if i == 1
        items = r;
    else
        fields = fieldnames(r);
        for j = 1 : numel(fields)
            items.(fields{j}) = [items.(fields{j}), r.(fields{j})];
        end
    end

end

s = Combine(c, items);

return;

%***********************************************************************************************************************

function c = Start(c)

if c.numCats < 2, error('invalid numCats'); end

if ~isfield(c, 'oaa'   ), c.oaa    = true    ; end
if ~isfield(c, 'prob'  ), c.prob   = false   ; end
if ~isfield(c, 'opt'   ), c.opt    = false   ; end
if ~isfield(c, 'lib'   ), c.lib    = 'libsvm'; end
if ~isfield(c, 'printf'), c.printf = @fprintf; end

if c.oaa

    c.count = c.numCats;

    if ~isfield(c, 'batchSize'), c.batchSize = 1; end

else

    c.count = c.numCats * (c.numCats - 1) / 2;

    c.y1 = zeros(1, c.count);
    c.y2 = zeros(1, c.count);
    i = 0;
    for y1 = 1 : c.numCats - 1
        for y2 = y1 + 1 : c.numCats
            i = i + 1;
            c.y1(i) = y1;
            c.y2(i) = y2;
        end
    end

    if ~isfield(c, 'batchSize'), c.batchSize = 50; end

end

if isfield(c, 'cost')
    if isempty(c.cost), error('invalid cost'); end
    if ~c.opt && ~isscalar(c.cost), error('invalid cost'); end
elseif c.opt
    c.cost = logspace(-2, 1, 4);
else
    c.cost = 1;
end

if isfield(c, 'weight')
    if isempty(c.weight), error('invalid weight'); end
    if ~c.opt && ~isscalar(c.weight), error('invalid weight'); end
elseif c.opt
    c.weight = logspace(0, 1, 6);
else
    c.weight = 1;
end

c = feval(['init_' c.lib], c);

return;

%***********************************************************************************************************************

function r = Item(c, itemNo)

% TODO: remove soon
if nargin < 2, itemNo = c.itemNo; end

if c.oaa

    pinds = find(c.y == itemNo);
    ninds = find(c.y ~= itemNo);

    desc = sprintf('rule %u', itemNo);

else

    y1 = c.y1(itemNo);
    y2 = c.y2(itemNo);

    pinds = find(c.y == y1);
    ninds = find(c.y == y2);

    desc = sprintf('rule %u-%u', y1, y2);

end

r = Solve(c, pinds, ninds);

r.desc = desc;

return;

%***********************************************************************************************************************

function s = Combine(c, items)

% TODO: remove soon
if nargin < 2, items = c.items; end

s.numCats = c.numCats;
s.oaa     = c.oaa;
s.prob    = c.prob;
s.lib     = c.lib;

if ~c.oaa
    s.y1 = c.y1;
    s.y2 = c.y2;
end

s.fMeans = c.fMeans;
s.fStds  = c.fStds;

fields = fieldnames(items);
for i = 1 : numel(fields)
    s.(fields{i}) = items.(fields{i})';
end

return;

%***********************************************************************************************************************

function v = Value(s, x)

if isempty(s.fMeans)
    nx = x;
else
    nx = linclass_norm(x, s.fMeans, s.fStds);
end

v = double(bsxfun(@plus, s.W * nx, s.b));

return;

%***********************************************************************************************************************

function p = Prob(s, x)

if ~s.prob, error('this classifier was not trained to output probabilities'); end

v = Value(s, x);

p = 1 ./ (1 + exp(bsxfun(@plus, bsxfun(@times, s.A, v), s.B)));

return;

%***********************************************************************************************************************

function n = Votes(s, x)

if s.oaa, error('not valid for one-against-all classifiers'); end

if s.prob
    v = Prob(s, x);
    t = 0.5;
else
    v = Value(s, x);
    t = 0;
end

n = zeros(s.numCats, size(x, 2));
for y = 1 : s.numCats
    n(y, :) = n(y, :) + sum(v(s.y1 == y, :) >= t, 1);
    n(y, :) = n(y, :) + sum(v(s.y2 == y, :) <= t, 1);
end

return;

%***********************************************************************************************************************

function v = Score(s, x)

if s.oaa
    if s.prob
        v = Prob(s, x);
    else
        v = Value(s, x);
    end
else
    v = Votes(s, x);
end

return;

%***********************************************************************************************************************

function c = Best(s, x, top)

if nargin < 3, top = 1; end

v = Score(s, x);

if top == 1
    [ans, c] = max(v, [], 1);
else
    [ans, c] = sort(v, 1, 'descend');
    c = c(1 : top, :);
end

return;

%***********************************************************************************************************************

function r = Solve(c, pinds, ninds)

if (numel(c.cost) > 1) || (numel(c.weight) > 1)

    nfolds = 3;

    if (numel(pinds) < nfolds) || (numel(ninds) < nfolds)
        error('not enough examples for %u-fold cross-validation', nfolds);
    end
    pgroups = mod(0 : numel(pinds) - 1, nfolds) + 1;
    ngroups = mod(0 : numel(ninds) - 1, nfolds) + 1;
    pgroups = pgroups(randperm(numel(pinds)));
    ngroups = ngroups(randperm(numel(ninds)));

    err = zeros(numel(c.cost), numel(c.weight));

    for i = 1 : numel(c.cost)
        for j = 1 : numel(c.weight)

            for k = 1 : nfolds

                r = Binary(c, pinds(pgroups ~= k), ninds(ngroups ~= k), c.cost(i), c.weight(j), false, false);

                fn = mean(r.W' * c.x(:, pinds(pgroups == k)) + r.b <  0);
                fp = mean(r.W' * c.x(:, ninds(ngroups == k)) + r.b >= 0);

                err(i, j) = err(i, j) + max(fn, fp) / nfolds;

            end

            c.printf('cost=%f weight=%f err=%f\n', c.cost(i), c.weight(j), err(i, j));

        end
    end

    [i, j] = find(err == min(err(:)), 1);
    cost   = c.cost  (i);
    weight = c.weight(j);

    c.printf('cost=%f weight=%f\n', cost, weight);

else

    cost   = c.cost;
    weight = c.weight;

end

r = Binary(c, pinds, ninds, cost, weight, c.prob, true);

return;

%***********************************************************************************************************************

function r = Binary(c, pinds, ninds, cost, weight, prob, final)

if isempty(pinds) || isempty(ninds)

    if isempty(pinds) && isempty(ninds)
        b = 0;
    elseif isempty(pinds)
        b = -inf;
    elseif isempty(ninds)
        b = inf;
    end

    r.W = zeros(c.dim, 1, 'single');
    r.b = single(b);

    if prob
        r.A = -1;
        r.B = 0;
    end

else

    r = feval(['call_' c.lib], c, pinds, ninds, cost, weight, prob, final);

end

r.p = [cost; weight];

return;

%***********************************************************************************************************************

function c = init_libsvm(c)

if (min(c.y) < 1) || (max(c.y) > c.numCats), error('invalid y'); end

[c.x, c.fMeans, c.fStds] = linclass_norm(c.x);
c.dim = size(c.x, 1);

return;

%-----------------------------------------------------------------------------------------------------------------------

function r = call_libsvm(c, pinds, ninds, cost, weight, prob, final)

y = [ones(numel(pinds), 1); zeros(numel(ninds), 1)];
x = double(c.x(:, [pinds ninds])');

weight2 = weight * numel(ninds) / numel(pinds);

if final
    eps = 0.001;
else
    eps = 0.01;
end

opts = sprintf('-s 0 -t 0 -m 4096 -q -c %f -w1 %f -b %u -e %f', cost, weight2, prob, eps);

model = svmtrain(y, x, opts);

r.W = single(model.SVs' * model.sv_coef);
r.b = single(-model.rho);

if prob
    r.A = model.ProbA;
    r.B = model.ProbB;
end

return;

%***********************************************************************************************************************

function c = init_liblinear(c)

if c.prob, error('liblinear does not output probabilities'); end

if (min(c.y) < 1) || (max(c.y) > c.numCats), error('invalid y'); end

[c.x, c.fMeans, c.fStds] = linclass_norm(c.x);
c.dim = size(c.x, 1);

return;

%-----------------------------------------------------------------------------------------------------------------------

function r = call_liblinear(c, pinds, ninds, cost, weight, prob, final)

y = [ones(numel(pinds), 1); zeros(numel(ninds), 1)];
x = sparse(double(c.x(:, [pinds ninds])'));

weight2 = weight * numel(ninds) / numel(pinds);

eps = 0.1; % TODO: what to use if not final?

opts = sprintf('-s 1 -B 1 -q -c %f -w1 %f -e %f', cost, weight2, eps);

model = liblinear_train(y, x, opts);

r.W = single(model.w(1, 1 : end - 1))';
r.b = single(model.w(end));

return;

%***********************************************************************************************************************

function c = init_rls(c)

if c.prob, error('rls does not output probabilities'); end

if (min(c.y) < 1) || (max(c.y) > c.numCats), error('invalid y'); end

[c.x, c.fMeans, c.fStds] = linclass_norm(c.x);
c.dim = size(c.x, 1);

c.k = double(c.x' * c.x);

return;

%-----------------------------------------------------------------------------------------------------------------------

function r = call_rls(c, pinds, ninds, cost, weight, prob, final)

np = numel(pinds);
nn = numel(ninds);

y = [ones(np, 1); -ones(nn, 1)];
x = double(c.x(:, [pinds ninds]));
k = c.k([pinds ninds], [pinds ninds]);

% TODO: how to incorporate weight and final?

W = sqrt([repmat(0.5 / np, np, 1); repmat(0.5 / nn, nn, 1)]);
M = zeros(numel(y));
for i = 1 : numel(y)
    M(:, i) = k(:, i) .* W * W(i);
end
Z = y .* W;
R = chol(M + eye(numel(y)) * cost);
[C, b] = CholeskyCB(R, W, Z);

r.W = single(x * C);
r.b = single(b);

return;

%***********************************************************************************************************************

function c = init_lsrls(c)

if ~c.oaa, error('lsrls does not support one-against-one classification'); end
if c.prob, error('lsrls does not output probabilities'); end
if c.opt, error('lsrls does not support parameter optimization'); end

if any(isfield(c, {'x', 'y'})) == isfield(c, 'read')
    error('must give either x and y or read');
end

if isfield(c, 'x')
    if (min(c.y) < 1) || (max(c.y) > c.numCats), error('invalid y'); end
    [c.x, c.fMeans, c.fStds] = linclass_norm(c.x);
    c.read = @(a, i) a.x(:, a.y == i);
else
    c.fMeans = [];
    c.fStds  = [];
end

for i = 1 : c.numCats
    x = c.read(c, i);
    if i == 1
        c.dim = size(x, 1);
        c.obj = ClassifierState(c.dim);
        ys = [];
    end
    c.obj.AddPoints(x');
    ys(end + 1 : end + size(x, 2)) = i;
    c.printf('added category %u/%u to none-vs-all matrix\n', i, c.numCats);
end

if ~isfield(c, 'x')
    c.y = ys;
end

return;

%-----------------------------------------------------------------------------------------------------------------------

function r = call_lsrls(c, pinds, ninds, cost, weight, prob, final)

x = c.read(c, c.y(pinds(1)));

% TODO: how to incorporate weight?

wp = 0.5 / numel(pinds);
wn = 0.5 / numel(ninds);
[W, b] = c.obj.GetClassifier(x', true, cost, wp, wn);

r.W = single(W);
r.b = single(b);

return;
