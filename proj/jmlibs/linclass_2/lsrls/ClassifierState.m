classdef ClassifierState < handle
    
    properties
        K
        N
        Y
    end
    
    methods
        function obj = ClassifierState(d)
            obj.K = zeros(d);
            obj.Y = zeros(d, 1);
            obj.N = 0;
        end
        
        function AddPoints(obj, X)
            obj.K = obj.K + X'*X;
            obj.Y = obj.Y + sum(X)';
            obj.N = obj.N + size(X, 1);
        end
        
        function [W b] = GetClassifier(obj, X, useOffset, lambda, wplus, wneg)
            if nargin <5
                wplus = 1;
                wneg = 1;
            end
            
            [nplus d] = size(X);
            nminus = obj.N - nplus;
            temp = sum(X)';
            R = wneg*obj.K + (wplus - wneg)*(X'*X) + eye(d)*lambda;
            Z = (wplus + wneg)*temp - wneg*obj.Y;
            R = chol(R);
            
            if useOffset
                temp = (wplus - wneg)*temp + wneg*obj.Y;
                W = R\(R'\temp);
                num = wplus*nplus - wneg*nminus - sum(Z.*W);
                den = wplus*nplus + wneg*nminus - sum(temp.*W);
                b = num/den;
                W = R\(R'\(Z - b*temp));
            else
                W = R\(R'\Z);
                b = 0;
            end
        end
        
        function Classification = Classify(obj, X, W, b)
            n = size(X, 1);
            vect = X*W + repmat(b, n, 1);
            [v Classification] = max(vect, [], 2);
        end
    end
    
end

