function [x, fMeans, fStds] = linclass_norm(x, fMeans, fStds)

if (nargin ~= 1) && (nargin ~= 3), error('incorrect number of arguments'); end

if nargin == 1
    [fMeans, fStds] = GetStats(x);
end

x = Normalize(x, fMeans, fStds);
    
return;

%***********************************************************************************************************************

function [fMeans, fStds] = GetStats(x)

fMeans = zeros(size(x, 1), 1, class(x));
fStds  = zeros(size(x, 1), 1, class(x));

for f = 1 : size(x, 1)

    values = x(f, x(f, :) > -1e9);

    if isempty(values)

        fMeans(f) = 0;
        fStds (f) = Inf;

    else

        fMeans(f) = mean(values);
        fStds (f) = std (values);

        if fStds(f) == 0, fStds(f) = Inf; end

    end

end

return;

%***********************************************************************************************************************

function x = Normalize(x, fMeans, fStds)

for f = 1 : size(x, 1)
    x(f, x(f, :) <= -1e9) = fMeans(f);
    x(f, :) = (x(f, :) - fMeans(f)) / fStds(f);
end

return;
