function varargout = libsvm_old(mode, varargin)

switch mode
case 'build'  , [varargout{1 : nargout}] = Build  (varargin{:});
case 'start'  , [varargout{1 : nargout}] = Start  (varargin{:});
case 'item'   , [varargout{1 : nargout}] = Item   (varargin{:});
case 'combine', [varargout{1 : nargout}] = Combine(varargin{:});
case 'value'  , [varargout{1 : nargout}] = Value  (varargin{:});
case 'prob'   , [varargout{1 : nargout}] = Prob   (varargin{:});
case 'votes'  , [varargout{1 : nargout}] = Votes  (varargin{:});
case 'best'   , [varargout{1 : nargout}] = Best   (varargin{:});
end

return;

%***********************************************************************************************************************

function r = Build(c)

c.batchSize = 1;

c = Start(c);

c.items.alpha = single([]);
c.items.bias  = single([]);
c.items.A     = [];
c.items.B     = [];

for i = 1 : c.count

    c.itemNo = i;

    b = Item(c);

    c.items.alpha = [c.items.alpha, b.alpha];
    c.items.bias  = [c.items.bias , b.bias ];
    c.items.A     = [c.items.A    , b.A    ];
    c.items.B     = [c.items.B    , b.B    ];

end

r = Combine(c);

return;

%***********************************************************************************************************************

function c = Start(c)

if c.numCats < 2, error('invalid numCats'); end
if (min(c.y) < 1) || (max(c.y) > c.numCats), error('invalid y'); end

[c.x, c.fMeans, c.fStds] = libsvm_sphere(c.x);

if ~isfield(c, 'oaa'), c.oaa = false; end

if c.oaa

    c.count = c.numCats;

    if ~isfield(c, 'batchSize'), c.batchSize = 1; end

else

    c.count = c.numCats * (c.numCats - 1) / 2;

    c.y1 = zeros(1, c.count);
    c.y2 = zeros(1, c.count);
    i = 0;
    for y1 = 1 : c.numCats - 1
        for y2 = y1 + 1 : c.numCats
            i = i + 1;
            c.y1(i) = y1;
            c.y2(i) = y2;
        end
    end

    if ~isfield(c, 'batchSize'), c.batchSize = 50; end

end

return;

%***********************************************************************************************************************

function r = Item(c)

if c.oaa

    r.desc = sprintf('rule %u', c.itemNo);

    numPos = sum(c.y == c.itemNo);
    numNeg = sum(c.y ~= c.itemNo);
    inds   = [find(c.y == c.itemNo), find(c.y ~= c.itemNo)];

else

    y1 = c.y1(c.itemNo);
    y2 = c.y2(c.itemNo);

    r.desc = sprintf('rule %u-%u', y1, y2);

    numPos = sum(c.y == y1);
    numNeg = sum(c.y == y2);
    inds   = [find(c.y == y1), find(c.y == y2)];

end

if (numPos == 0) || (numNeg == 0)

    if (numPos == 0) && (numNeg == 0)
        bias = 0;
    elseif numPos == 0
        bias = -inf;
    elseif numNeg == 0
        bias = inf;
    end

    r.alpha = zeros(numel(c.y), 1, 'single');
    r.bias  = single(bias);
    r.A     = -1;
    r.B     = 0;

    return;

end

y = [ones(1, numPos), zeros(1, numNeg)];
x = double(c.x(:, inds));

options = '';
options = [options '-s 0 ']; % C-SVC
options = [options '-t 0 ']; % linear kernel
options = [options '-c 1 ']; % cost
options = [options '-b 1 ']; % probability estimate
options = [options '-q '  ]; % quiet
options = [options '-w1 ' num2str(numNeg / numPos)]; % penalty for positive class

model = svmtrain(y', x', options);

svinds = inds(FindVectors(x, model.SVs'));

alpha = zeros(numel(c.y), 1);
alpha(svinds, 1) = model.sv_coef;

r.alpha = single(alpha);
r.bias  = single(-model.rho);
r.A     = model.ProbA;
r.B     = model.ProbB;

return;

%-----------------------------------------------------------------------------------------------------------------------

function inds = FindVectors(as, vs)

inds = zeros(1, size(vs, 2));

asum = sum(as, 1);
vsum = sum(vs, 1);

for i = 1 : size(vs, 2)
    js = find(asum == vsum(i));
    found = false;
    for j = js
        if all(as(:, j) == vs(:, i))
            found = true;
            break;
        end
    end
    if ~found, error('support vector not found'); end
    inds(i) = j;
end

return;

%***********************************************************************************************************************

function r = Combine(c)

r.numCats = c.numCats;
r.oaa     = c.oaa;

if ~c.oaa
    r.y1 = c.y1;
    r.y2 = c.y2;
end

r.fMeans = c.fMeans;
r.fStds  = c.fStds;

svinds = find(sum(abs(c.items.alpha), 2) ~= 0);

r.W = (c.x(:, svinds) * c.items.alpha(svinds, :))';
r.b = c.items.bias';
r.A = c.items.A';
r.B = c.items.B';

return;

%***********************************************************************************************************************

function v = Value(s, x)

nx = libsvm_sphere(x, s.fMeans, s.fStds);

v = double(bsxfun(@plus, s.W * nx, s.b));

return;

%***********************************************************************************************************************

function p = Prob(s, x)

v = Value(s, x);

p = 1 ./ (1 + exp(bsxfun(@plus, bsxfun(@times, s.A, v), s.B)));

return;

%***********************************************************************************************************************

function n = Votes(s, x)

if s.oaa, error('not valid for one-against-all svms'); end

p = Prob(s, x);

n = zeros(s.numCats, size(x, 2));
for y = 1 : s.numCats
    n(y, :) = n(y, :) + sum(p(s.y1 == y, :) >= 0.5, 1);
    n(y, :) = n(y, :) + sum(p(s.y2 == y, :) <= 0.5, 1);
end

return;

%***********************************************************************************************************************

function c = Best(s, x)

if s.oaa
    n = Prob(s, x);
else
    n = Votes(s, x);
end

[ans, c] = max(n, [], 1);

return;
