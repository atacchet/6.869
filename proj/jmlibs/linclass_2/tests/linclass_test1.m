% Generate fake 2D data.
x = single(rand(2, 100));
y = double(x(1, :) - x(2, :) > 0) * 2 + double(x(1, :) + x(2, :) < 1) + 1;

% Build classifier.
c.x       = x;
c.y       = y;
c.numCats = 4;
c.lib     = 'libsvm';
c.prob    = true;
s = linclass('build', c);
clear c;

% Classify data.
cat = linclass('best' , s, x); % Most likely category.
v   = linclass('value', s, x); % Raw classifier outputs.
p   = linclass('prob' , s, x); % Probabilities.

figure(1); clf;
plot(x(1, (y == 1) & (cat == 1)), x(2, (y == 1) & (cat == 1)), 'b.'); hold on;
plot(x(1, (y == 1) & (cat ~= 1)), x(2, (y == 1) & (cat ~= 1)), 'bo'); hold on;
plot(x(1, (y == 2) & (cat == 2)), x(2, (y == 2) & (cat == 2)), 'r.'); hold on;
plot(x(1, (y == 2) & (cat ~= 2)), x(2, (y == 2) & (cat ~= 2)), 'ro'); hold on;
plot(x(1, (y == 3) & (cat == 3)), x(2, (y == 3) & (cat == 3)), 'g.'); hold on;
plot(x(1, (y == 3) & (cat ~= 3)), x(2, (y == 3) & (cat ~= 3)), 'go'); hold on;
plot(x(1, (y == 4) & (cat == 4)), x(2, (y == 4) & (cat == 4)), 'k.'); hold on;
plot(x(1, (y == 4) & (cat ~= 4)), x(2, (y == 4) & (cat ~= 4)), 'ko'); hold off;
title('circles are misclassified points');

figure(2); clf;
vv = v(1, :); % for 1st class only
pp = p(1, :); % for 1st class only
plot(vv, pp, 'b.'); hold on;
plot([min(vv) max(vv)], [0.5 0.5], 'r'); hold off;
title(sprintf('%f percent of points probably (p > 0.5) in class 1', 100 * mean(pp > 0.5)));
xlabel('raw classifier output for class 1');
ylabel('probability of belonging to class 1');
