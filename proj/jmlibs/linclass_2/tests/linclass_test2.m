load ~/scratch/hristo/can_you_get_70.mat;
xtr = single(xtr);
xts = single(xts);

c.x       = xtr;
c.y       = ytr;
c.numCats = max(ytr);
c.lib     = 'rls';
tic;
s = linclass('build', c);
cpu = toc;
clear c;

ypr = linclass('best', s, xts);

score = zeros(1, max(ytr));
for i = 1 : max(ytr)
    score(i) = mean(ypr(yts == i) == i);
end
score = mean(score);
clear i;
