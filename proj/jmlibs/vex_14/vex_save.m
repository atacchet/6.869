function vex_save(path, d)

% Saves a vex_dataset object (or a structure that may contain vex_dataset
% objects) to a .mat file.
%
% vex_save(PATH, D) saves dataset D to path PATH.  D will be saved under the
% variable name "d".
%
% vex_save(PATH, S) saves structure S to path PATH.  Each field of S will be
% saved as a separate variable.
%
% See also: vex_load.

%***********************************************************************************************************************

% TODO: doc
path = gl_abspath(path);
if exist(path, 'dir'), path = fullfile(path, 'vex.mat'); end

if isa(d, 'vex_dataset')
    s.d = d;
elseif isstruct(d)
    s = d;
else
    error('invalid argument');
end

gl_save(path, s);

return;