classdef vex_dataset < handle

%-----------------------------------------------------------------------------------------------------------------------

methods
function names = PropNames(T)

% NAMES = D.PropNames: returns a cell array containing the names of all
% properties defined for dataset D.  Note that a property applies to the entire
% dataset, not just one particular item.
%
% See also: AddProp, PropDef, PropType.

names = fieldnames(T.props);

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function def = PropDef(T, name)

% DEF = D.PropDef(NAME) returns the definition of property NAME for dataset D.
% Note that for properties that were defined in terms of other properties (i.e.
% using a function), the function handle will be returned, not its result.  The
% result can be retrieved via the syntax D.NAME.
%
% See also: PropNames, AddProp.

def = T.props.(name);

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function type = PropType(T, name)

% TYPE = D.PropType(NAME) returns the special data type of property NAME for
% dataset D.  Currently the only special data type is 'path'.  (See the AddProp
% method.)
%
% See also: PropNames, AddProp.

type = T.types.(name);

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function AddProp(T, name, def, type, pos)

% D.AddProp(NAME, DEF) adds new property NAME to dataset D.  DEF is either the
% value of the property (any MATLAB variable) or a function handle that takes a
% single argument.  In the latter case, the property's value will be computed by
% calling DEF(S), where S is a structure containing the values of all
% previously-defined properties for the dataset.
%
% D.AddProp(..., TYPE) identifies the special data type of the property, if any.
% Currently only '' and 'path' are valid.  Identifying a property as containing
% a path activates checking for path validity.
%
% D.AddProp(..., TYPE, POS) causes the new property to be inserted at position
% POS in the property order.  If POS is omitted, or Inf, the new property will
% appear last in the order.
%
% See also: UpdateProp, DeleteProp, MoveProp, PropNames.

if T.opend, error('dataset is open'); end

T.CheckName(name);

def = StripFunc(def);

if nargin < 4, type = ''; end

fields = fieldnames(T.props);
if (nargin < 5) || isinf(pos)
    pos = numel(fields) + 1;
elseif (pos < 1) || (pos > numel(fields) + 1)
    error('invalid position');
end

s = rmfield(T.cache, setdiff(fieldnames(T.cache), fields(1 : pos - 1)));
[use, store] = ComputeProp(def, type, s);

T.props.(name) = store;
T.types.(name) = type;
T.cache.(name) = use;

fields = [fields(1 : pos - 1); {name}; fields(pos : end)];
T.props = orderfields(T.props, fields);
T.types = orderfields(T.types, fields);

T.RecomputeProps(false);

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function UpdateProp(T, name, def)

% D.UpdateProp(NAME, DEF) changes the definition of existing property NAME for
% dataset D.  DEF is the same as in the AddProp method.  Any other properties
% defined using function handles will be recomputed.
%
% See also: AddProp, PropNames.

if T.opend, error('dataset is open'); end

fields = fieldnames(T.props);
[ans, pos] = ismember(name, fields);
if pos == 0, error('property "%s" does not exist', name); end

def = StripFunc(def);

s = rmfield(T.cache, setdiff(fieldnames(T.cache), fields(1 : pos - 1)));
[use, store] = ComputeProp(def, T.types.(name), s);

T.props.(name) = store;
T.cache.(name) = use;

T.RecomputeProps(false);

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function DeleteProp(T, name)

% D.DeleteProp(NAME) deletes property NAME from dataset D.  Be sure to also
% update or delete any properties or formulas that depend on the deleted
% property.
%
% See also: AddProp, PropNames.

if T.opend, error('dataset is open'); end

if ~isfield(T.props, name), error('property "%s" does not exist', name); end

T.props = rmfield(T.props, name);
T.types = rmfield(T.types, name);

T.RecomputeProps(false);

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function MoveProp(T, name, newName, pos)

% D.MoveProp(NAME, NEWNAME) renames property NAME to NEWNAME in dataset D.  Be
% sure to update any properties or formulas that depend on the renamed property.
%
% D.MoveProp(..., POS) also moves the property to position POS in the property
% order.  Use Inf to move to the end of the order.  Pass NEWNAME = '' to just
% change the position (keeping the old name).  Note that any properties that
% depend on the repositioned property must still come after it in the property
% order.
%
% See also: AddProp, PropNames.

if T.opend, error('dataset is open'); end

if ~isfield(T.props, name), error('property "%s" does not exist', name); end

if nargin < 4, pos = []; end

[fields, newName] = T.CheckMove(fieldnames(T.props), name, newName, pos);

if ~isempty(newName)
    T.props.(newName) = T.props.(name);
    T.types.(newName) = T.types.(name);
    T.props = rmfield(T.props, name);
    T.types = rmfield(T.types, name);
end

T.props = orderfields(T.props, fields);
T.types = orderfields(T.types, fields);

T.RecomputeProps(false);

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function n = NumItems(T)

% N = D.NumItems: returns the number of items in dataset D.
%
% See also: AddItems, AddField.

n = T.count;

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function names = FieldNames(T)

% NAMES = D.FieldNames: returns a cell array containing the names of all data
% fields defined for dataset D.  Note that each data field holds a value for
% each item in the dataset.
%
% See also: AddField, FieldSize.

names = fieldnames(T.items);

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function siz = FieldSize(T, name)

% SIZE = D.FieldSize(NAME) returns, for dataset D, the size of field NAME for a
% single item.  For example, if field NAME contains a scalar value for each
% item, SIZE will be [1 1].  If field NAME has a 4-element column vector for
% each item, SIZE will be [4 1].  Etc.
%
% See also: FieldNames, AddField.

siz = T.sizes.(name);

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function AddField(T, name, vals, dim, pos)

% D.AddField(NAME, VALS) adds new data field NAME to dataset D.
%
%    If the dataset has no data fields yet, and VALS is an AxN matrix, then the
%    resulting dataset will be considered to contain N items, with field NAME of
%    each item being an [Ax1] vector.  If VALS is AxBxN, this corresponds to N
%    items, each having an [AxB] NAME field, etc.
%
%    If the dataset already contains N items, then the last dimension of VALS
%    must have size N or size 1.  In the latter case, each item will get a copy
%    of VALS.
%
% D.AddField(..., DIM) allows you to explictly pass the dimensionality of the
% new field.  This may be necessary in some cases when N=1 due to MATLAB's
% removal of trailing singleton dimensions.  For example, there is no way to
% distinguish [Ax1xN] from [AxN] when N=1.
%
% D.AddField(..., DIM, POS) causes the new field to be inserted at position POS
% in the field order.  Field order does not matter for any computations so this
% is just for looks.  When POS is omitted, or Inf, the new property will appear
% last in the order.  (Pass DIM=[] for default handling of dimensionality.)
%
% See also: UpdateField, ExpandField, DeleteField, MoveField, AddItems,
%           NumItems, FieldNames.

T.CheckName(name);

if (nargin < 4) || isempty(dim)
    dim = ndims(vals);
else
    dim = dim + 1;
    if ndims(vals) > dim, error('vals has more than %u dimensions', dim); end
end

fields = fieldnames(T.items);
if (nargin < 5) || isinf(pos)
    pos = numel(fields) + 1;
elseif (pos < 1) || (pos > numel(fields) + 1)
    error('invalid position');
end

siz = size(vals);
siz(end + 1 : dim) = 1;

if isempty(fields)
    T.count = siz(end);
elseif siz(end) == T.count
elseif siz(end) == 1
    vals = repmat(vals, [ones(1, dim - 1), T.count]);
else
    error('need %u items', T.count);
end

T.items.(name) = vals;
T.sizes.(name) = siz(1 : end - 1);

fields = [fields(1 : pos - 1); {name}; fields(pos : end)];
T.items = orderfields(T.items, fields);
T.sizes = orderfields(T.sizes, fields);

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function UpdateField(T, name, vals, inds)

% D.UpdateField(NAME, VALS) replaces the value of field NAME in dataset D for
% all items in the dataset.  If field NAME has size A for each item, then VALS
% must either have size [AxN] or [Ax1], where N is the number of items in the
% dataset.  In the [Ax1] case, each item will get a copy of VALS.
%
% D.UpdateField(..., INDS) updates field NAME only for the items identified by
% INDS.  INDS is a vector of item numbers or a logical vector.  When using this
% syntax, VALS must have size [AxM] or [Ax1], where M is the number of items
% identified by INDS.
%
% See also: AddField, UpdateItems, NumItems, FieldNames.

s.(name) = vals;

if nargin < 4, inds = ':'; end

T.UpdateItems(s, inds);

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function ExpandField(T, name, vals, inds)

% D.ExpandField(NAME, VALS) takes each item in dataset D and generates multiple
% copies, with each copy getting a different value for field NAME.  This is a
% key step in generating virtual examples.  VALS contains the different values
% of field NAME that will be given to each copy of an item.  For example, if the
% dataset has N items, field NAME is [Ax1] for each item, and VALS is [AxM], the
% resulting dataset D will contain NM items.
%
% D.ExpandField(..., INDS) does the same thing, but only for the items
% identified by INDS.  Other items will not be multiplied and will retain their
% existing values of field NAME.
%
% See also: AddField, ExpandItems, NumItems, FieldNames.

s.(name) = vals;

if nargin < 4, inds = ':'; end

T.ExpandItems(s, inds);

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function DeleteField(T, name)

% D.DeleteField(NAME) deletes field NAME from dataset D.  Be sure to also update
% or delete any formulas that depend on the deleted field.
%
% See also: AddField, DeleteItems, FieldNames.

if ~isfield(T.items, name), error('field "%s" does not exist', name); end

T.items = rmfield(T.items, name);
T.sizes = rmfield(T.sizes, name);

if isempty(fieldnames(T.items))
    T.count = 0;
end

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function MoveField(T, name, newName, pos)

% D.MoveField(NAME, NEWNAME) renames field NAME to NEWNAME in dataset D.  Be
% sure to update any formulas that depend on the renamed field.
%
% D.MoveField(..., POS) also moves the field to position POS in the field order.
% Field order does not matter for any computations so this is just for looks.
% Use Inf to move to the end of the order.  Pass NEWNAME='' to just change the
% position (keeping the old name).
%
% See also: AddField, FieldNames.

if ~isfield(T.items, name), error('field "%s" does not exist', name); end

if nargin < 4, pos = []; end

[fields, newName] = T.CheckMove(fieldnames(T.items), name, newName, pos);

if ~isempty(newName)
    T.items.(newName) = T.items.(name);
    T.sizes.(newName) = T.sizes.(name);
    T.items = rmfield(T.items, name);
    T.sizes = rmfield(T.sizes, name);
end

T.items = orderfields(T.items, fields);
T.sizes = orderfields(T.sizes, fields);

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function AddItems(T, s, ignore)

% D.AddItems(S) appends new items to the end of dataset D.  S must either be
% another dataset having the same fields as D, or a structure having the same
% fields as D.  The sizes of fields in D and S must be compatible.
%
% D.AddItems(..., IGNORE) can be used when S is another dataset.  IGNORE=TRUE
% means that S may have extra fields, i.e. fields that D doesn't have.  These
% extra fields will be ignored.
%
% See also: UpdateItems, ExpandItems, SelectItems, DeleteItems, AddField,
%           NumItems, FieldNames.

if nargin < 3, ignore = false; end

if isstruct(s)
    if ignore, error('ignore is invalid for structure input'); end
    cnt = T.CheckItems(s, true);
else
    [cnt, s] = T.CheckDataset(s, ignore);
end

if cnt == 0, return; end

T.count = T.count + cnt;

fields = fieldnames(T.items);
for i = 1 : numel(fields)
    name = fields{i};
    colons = T.Colons(name);
    T.items.(name)(colons{:}, end + 1 : end + cnt) = s.(name);
end

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function UpdateItems(T, s, inds)

% D.UpdateItems(S) replaces the values of one or more fields for all items in
% dataset D.  S is a structure having some or all of the fields of D.  For each
% field NAME of S, this method essentially performs D.UpdateField(NAME, S.NAME).
%
% D.UpdateItems(..., INDS) replaces the values of one or more fields for the
% items identified by INDS.  For each field NAME of S, this method essentially
% performs D.UpdateField(NAME, S.NAME, INDS).
%
% See also: AddItems, UpdateField, NumItems, FieldNames.

if nargin < 3
    inds = 1 : T.count;
else
    inds = T.CheckInds(inds, 'u');
end

cnt = T.CheckItems(s, false);
if (cnt ~= numel(inds)) && (cnt ~= 1), error('need %u items', numel(inds)); end

if isempty(inds), return; end

fields = fieldnames(s);
for i = 1 : numel(fields)
    name = fields{i};
    colons = T.Colons(name);
    if cnt == numel(inds)
        T.items.(name)(colons{:}, inds) = s.(name);
    else
        T.items.(name)(colons{:}, inds) = repmat(s.(name), [ones(1, numel(colons)), numel(inds)]);
    end
end

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function ExpandItems(T, s, inds)

% D.ExpandItems(S) does the same thing as the ExpandField method, except that
% you can provide values for multiple fields.  S must be a structure containing
% one or more of the fields of D.  If each field of S contains values for M
% item copies, and D contains N items, then just like ExpandField, the resulting
% dataset will have NM items.  NOTE that this is NOT the same as calling
% ExpandField multiple times; here, only one expansion is performed, with the
% Ith copy of each item getting the Ith value of S.NAME for each field of S.
%
% D.ExpandItems(..., INDS) expands only the items identified by INDS.  Other
% items will not be multiplied and will retain their existing values for all
% fields.
%
% See also: AddItems, ExpandField, NumItems, FieldNames.

cnt = T.CheckItems(s, false);
if cnt == 0, error('no values given'); end

if nargin < 3
    inds = 1 : T.count;
else
    inds = sort(T.CheckInds(inds, 'u'));
end
if isempty(inds), return; end

% TODO: might waste too much memory when few items are being expanded
sel = zeros(cnt, T.count);
sel(1, :   ) = -(1 : T.count);
sel(:, inds) = repmat(inds, cnt, 1);
sel = sel(sel ~= 0);

T.SelectItems(abs(sel));

% TODO: make sure all values are zero before overwriting? how?
fields = fieldnames(s);
for i = 1 : numel(fields)
    name = fields{i};
    colons = T.Colons(name);
    T.items.(name)(colons{:}, sel > 0) = repmat(s.(name), [ones(1, numel(colons)), numel(inds)]);
end

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function SelectItems(T, inds)

% D.SelectItems(INDS) retains, in dataset D, only the items identified in INDS.
% INDS is a vector of item numbers or a logical vector.  Note that this method
% differs from the Copy method.  SelectItems modifies D, keeping only the
% identified items, while Copy copies the identified items into a new dataset.
% SelectItems can also be used to reorder the items in a dataset.
%
% See also: AddItems, NumItems, Copy.

inds = T.CheckInds(inds);

T.count = numel(inds);

fields = fieldnames(T.items);
for i = 1 : numel(fields)
    name = fields{i};
    colons = T.Colons(name);
    T.items.(name) = T.items.(name)(colons{:}, inds);
end

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function DeleteItems(T, inds)

% D.DeleteItems(INDS) deletes the items identified in INDS from dataset D.
% INDS is a vector of item numbers or a logical vector.
%
% See also: AddItems, NumItems.

inds = sort(T.CheckInds(inds, 'u'));
if isempty(inds), return; end

T.count = T.count - numel(inds);

fields = fieldnames(T.items);
for i = 1 : numel(fields)
    name = fields{i};
    colons = T.Colons(name);
    T.items.(name)(colons{:}, inds) = [];
end

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function names = FormNames(T)

% NAMES = D.FormNames: returns a cell array containing the names of all formulas
% defined for dataset D.  Note that a formula returns a value for each item, but
% that value is not stored -- it is computed when needed, using as input any of
% the item's fields or other formulas, or properties of the dataset as a whole.
%
% See also: AddForm, FormDef.

names = fieldnames(T.forms);

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function def = FormDef(T, name)

% DEF = D.FormDef(NAME) returns the definition of formula NAME for dataset D.
% The definition is the function handle used to compute the formula, not its
% result.  (To retrieve the result of a formula for item #I, use the syntax
% D.NAME; for example, D.NAME(:,I), or D.NAME{I} for formulas that return cell
% arrays.)
%
% See also: FormNames, AddForm.

def = T.forms.(name);

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function AddForm(T, name, def, pos)

% D.AddForm(NAME, DEF) adds formula NAME to dataset D.  DEF is a function handle
% (usually to an anonymous function) which will compute the formula upon
% request.  DEF can have one of two forms:
%
%    @(X): vectorized form.  When the user asks for the result of the formula
%    for some set of items, the function will be called just once, computing the
%    result for all the requested items together.
%
%    @(X,I): per-item form.  The function will be called for each item
%    requested, and the results will be concatenated.
%
% In both cases, X will be a structure containing all possible inputs that could
% be used to compute the function's result, including:
%
%    (1) The values of all dataset properties.
%    (2) The values of all data fields for the requested item(s).
%    (3) The results of other formulas, on which this formula might depend, for
%        the requested item(s).
%
% In the per-item case, the formula uses I as the index into (2) and (3) above.
% Note that I is *not* an item index, but a result-set index.
%
% D.AddForm(..., POS) causes the new formula to be inserted at position POS in
% the formula order.  Formula order does not matter for any computations so this
% is just for looks.  When POS is omitted, or Inf, the new formula will appear
% last in the order.
%
% See also: UpdateForm, DeleteForm, MoveForm, FormNames.

T.CheckName(name);

def = StripFunc(def);
if ~isa(def, 'function_handle'), error('must provide a function handle'); end
if ~ismember(nargin(def), [1 2]), error('function must take 1 or 2 inputs'); end

fields = fieldnames(T.forms);
if (nargin < 4) || isinf(pos)
    pos = numel(fields) + 1;
elseif (pos < 1) || (pos > numel(fields) + 1)
    error('invalid position');
end

T.forms.(name) = def;
T.multi.(name) = (nargin(def) == 1);

fields = [fields(1 : pos - 1); {name}; fields(pos : end)];
T.forms = orderfields(T.forms, fields);
T.multi = orderfields(T.multi, fields);

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function UpdateForm(T, name, def)

% D.UpdateForm(NAME, DEF) changes the definition of existing formula NAME for
% dataset D.  DEF is the same as in the AddForm method.
%
% See also: AddForm, FormNames.

if ~isfield(T.forms, name), error('formula "%s" does not exist', name); end

def = StripFunc(def);
if ~isa(def, 'function_handle'), error('must provide a function handle'); end
if ~ismember(nargin(def), [1 2]), error('function must take 1 or 2 inputs'); end

T.forms.(name) = def;
T.multi.(name) = (nargin(def) == 1);

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function DeleteForm(T, name)

% D.DeleteForm(NAME) deletes formula NAME from dataset D.  Be sure to also
% update or delete any other formulas that depend on the deleted formula.
%
% See also: AddForm, FormNames.

if ~isfield(T.forms, name), error('formula "%s" does not exist', name); end

T.forms = rmfield(T.forms, name);
T.multi = rmfield(T.multi, name);

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function MoveForm(T, name, newName, pos)

% D.MoveForm(NAME, NEWNAME) renames formula NAME to NEWNAME in dataset D.  Be
% sure to update any other formulas that depend on the renamed formula.
%
% D.MoveForm(..., POS) also moves the formula to position POS in the formula
% order.  Formula order does not matter for any computations so this is just for
% looks.  Use Inf to move to the end of the order.  Pass NEWNAME='' to just
% change the position (keeping the old name).
%
% See also: AddForm, FormNames.

if ~isfield(T.forms, name), error('formula "%s" does not exist', name); end

if nargin < 4, pos = []; end

[fields, newName] = T.CheckMove(fieldnames(T.forms), name, newName, pos);

if ~isempty(newName)
    T.forms.(newName) = T.forms.(name);
    T.multi.(newName) = T.multi.(name);
    T.forms = rmfield(T.forms, name);
    T.multi = rmfield(T.multi, name);
end

T.forms = orderfields(T.forms, fields);
T.multi = orderfields(T.multi, fields);

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function res = Item(T, ind, name)

% RES = D.Item(IND, NAME) retrieves the value of field or formula NAME for the
% single item of dataset D identified by IND.  The only difference between this
% syntax and the syntax D.NAME(..., IND) or D.NAME{..., IND} is that you don't
% need to know whether NAME refers to a cell array or not: if the result is a
% cell array of one cell, then the contents of that cell are returned.  You also
% don't need to know the dimensionality of field/formula NAME.  Note that if
% NAME is omitted, it defaults to 'item'.
%
% See also: AddField, AddForm.

if nargin < 3, name = 'item'; end

ind = T.CheckInds(ind, 's');

res = T.Retrieve(ind, name);

if iscell(res) && isscalar(res), res = res{1}; end

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function desc = Desc(T, ind)

% DESC = D.Desc(IND) retrieves the description of the single item of dataset D
% identified by IND.  This will always be a character string.  If the dataset
% contains a field or formula called 'desc', whose value for each item is a
% character string, then that value will be returned.  Otherwise, the string
% 'item #', with '#' replaced by IND, will be returned.
%
% See also: Item.

ind = T.CheckInds(ind, 's');

if isfield(T.items, 'desc') || isfield(T.forms, 'desc')
    desc = T.Retrieve(ind, 'desc');
    if iscell(desc) && isscalar(desc), desc = desc{1}; end
else
    desc = '';
end

if ~ischar(desc) || isempty(desc)
    desc = sprintf('item %u', ind);
end

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function n = NumOpenHandlers(T)

% N = D.NumOpenHandlers: returns the number of open handlers for dataset D.
%
% See also: AddOpenHandler, OpenHandlerDef.

n = numel(T.fopen);

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function def = OpenHandlerDef(T, pos)

% DEF = D.OpenHandlerDef(POS): returns the POSth open handler for dataset D.
%
% See also: AddOpenHandler, NumOpenHandlers.

if (pos < 1) || (pos > numel(T.fopen)), error('invalid position'); end

def = T.fopen{pos};

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function AddOpenHandler(T, def, pos)

% D.AddOpenHandler(DEF) adds an "open handler" to dataset D.  This is a function
% that will be called when the dataset's consumer calls D.Open.  DEF must be a
% function handle which can be called as follows:
%
%    X = DEF(X)
%
% where X is a struct containing all the dataset's properties.  The handler may
% add additional "transient" properties to X which will persist until the
% dataset is closed.  (Existing properties may not be altered.)  Dataset
% formulas may refer to these transient properties just like normal properties;
% formulas that do so will only work when the dataset is open.
%
% D.AddOpenHandler(..., POS) inserts the new handler into position POS in the
% list of open handlers.  When POS is omitted, the handler is added to the end
% of the list.
%
% See also: DeleteOpenHandler, NumOpenHandlers, Open, AddCloseHandler.

if T.opend, error('dataset is open'); end

def = StripFunc(def);
if ~isa(def, 'function_handle'), error('must provide a function handle'); end

if (nargin < 3) || isinf(pos)
    pos = numel(T.fopen) + 1;
elseif (pos < 1) || (pos > numel(T.fopen) + 1)
    error('invalid position');
end

T.fopen = [T.fopen(1 : pos - 1), {def}, T.fopen(pos : end)];

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function DeleteOpenHandler(T, pos)

% D.DeleteOpenHandler(POS) deletes the POSth open handler from dataset D.
%
% See also: AddOpenHandler, NumOpenHandlers.

if T.opend, error('dataset is open'); end

if islogical(pos), pos = find(pos); end
if any(pos < 1) || any(pos > numel(T.fopen)), error('invalid position'); end

T.fopen(pos) = [];

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function n = NumCloseHandlers(T)

% N = D.NumCloseHandlers: returns the number of close handlers for dataset D.
%
% See also: AddCloseHandler, CloseHandlerDef.

n = numel(T.fclos);

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function def = CloseHandlerDef(T, pos)

% DEF = D.CloseHandlerDef(POS): returns the POSth close handler for dataset D.
%
% See also: AddCloseHandler, NumCloseHandlers.

if (pos < 1) || (pos > numel(T.fclos)), error('invalid position'); end

def = T.fclos{pos};

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function AddCloseHandler(T, def, pos)

% D.AddCloseHandler(DEF) adds a "close handler" to dataset D.  This is a
% function that will be called when the dataset's consumer calls D.Close.  DEF
% must be a function handle which can be called as follows:
%
%    DEF(X)
%
% where X is a struct containing all the dataset's properties, including any
% "transient" properties set when the dataset was opened.  Note that the
% handler need not remove transient properties from X; this will be done
% automatically after all close handlers have been called.  If removing the
% transient properties is all you need done, then you do not need a close
% handler.
%
% D.AddCloseHandler(..., POS) inserts the new handler into position POS in the
% list of close handlers.  When POS is omitted, the handler is added to the end
% of the list.  Note that close handlers are called in reverse order, i.e. from
% the end of the list to the beginning.
%
% See also: DeleteCloseHandler, NumCloseHandlers, Close, AddOpenHandler.

if T.opend, error('dataset is open'); end

def = StripFunc(def);
if ~isa(def, 'function_handle'), error('must provide a function handle'); end

if (nargin < 3) || isinf(pos)
    pos = numel(T.fclos) + 1;
elseif (pos < 1) || (pos > numel(T.fclos) + 1)
    error('invalid position');
end

T.fclos = [T.fclos(1 : pos - 1), {def}, T.fclos(pos : end)];

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function DeleteCloseHandler(T, pos)

% D.DeleteCloseHandler(POS) deletes the POSth close handler from dataset D.
%
% See also: AddCloseHandler, NumCloseHandlers.

if T.opend, error('dataset is open'); end

if islogical(pos), pos = find(pos); end
if any(pos < 1) || any(pos > numel(T.fclos)), error('invalid position'); end

T.fclos(pos) = [];

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function U = Copy(T, inds)

% E = D.Copy: makes a copy of dataset D.  (Note that because vex_dataset is a
% MATLAB "handle class", the syntax E = D does not copy the dataset, but just
% creates a new reference to it.)
%
% E = D.Copy(INDS) copies only the items identified by INDS into the new
% dataset.  INDS is a vector of item numbers or a logical vector.
%
% See also: SelectItems.

U = vex_dataset;

U.props = T.props;
U.types = T.types;

if nargin < 2

    U.count = T.count;
    U.items = T.items;

else

    inds = T.CheckInds(inds);

    U.count = numel(inds);
    U.items = struct;

    fields = fieldnames(T.items);
    for i = 1 : numel(fields)
        name = fields{i};
        colons = T.Colons(name);
        U.items.(name) = T.items.(name)(colons{:}, inds);
    end

end

U.sizes = T.sizes;
U.forms = T.forms;
U.multi = T.multi;
U.fopen = T.fopen;
U.fclos = T.fclos;

U.cache = rmfield(T.cache, setdiff(fieldnames(T.cache), fieldnames(T.props)));
U.quiet = T.quiet;

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function open = IsOpen(T)

% TF = D.IsOpen: returns true if dataset D is currently open, i.e., if Open has
% been called.
%
% See also: Open, Close.

open = T.opend;

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function Open(T)

% D.Open: performs any initialization necessary for dataset D to begin
% performing transformations.  If there is no initialization necessary, this
% method will just return without doing anything.  If initialization is
% necessary but Open is not called, some formulas may fail to work.
%
% To define initialization for a dataset, see the AddOpenHandler method.
%
% See also: Close, AddOpenHandler.

if T.opend, error('dataset is open'); end

for i = 1 : numel(T.fopen)

    s = T.fopen{i}(T.cache);

    fields = fieldnames(s);
    fields = fields(~ismember(fields, fieldnames(T.cache)));

    for j = 1 : numel(fields)
        name = fields{j};
        T.CheckName(name);
        T.cache.(name) = s.(name);
    end

end

T.opend = true;

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function Close(T)

% D.Close: releases any resources allocated by the Open method.  If you call
% Open, then you should call Close when you are done.
%
% To define what Close does for a dataset, see the AddCloseHandler method.
%
% See also: Open, AddCloseHandler.

if ~T.opend, return; end

for i = numel(T.fclos) : -1 : 1
    T.fclos{i}(T.cache);
end

T.cache = rmfield(T.cache, setdiff(fieldnames(T.cache), fieldnames(T.props)));

T.opend = false;

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function AssignIDs(T, idName, groupBy, sortBy, inds)

% D.AssignIDs(IDNAME, GROUPBY, SORTBY) assigns unique ID numbers to items.
% Unique IDs are particularly useful for generating unique path names prior to a
% Write operation.  IDNAME is the name of an existing data field which will hold
% the generated IDs, and it must contain all zeros.  The next two parameters
% determine how the IDs are to be assigned.
%
%    GROUPBY is the name of a field or formula, or a cell array of such names.
%    Items will be divided into groups that share the same values for all these
%    fields/formulas, and IDs starting at 1 will be assigned to the items within
%    each group.  If GROUPBY is empty, the entire dataset is treated as one
%    group.  NOTE: a function handle of the form accepted by AddForm can also be
%    given in place of an existing formula name.
%
%    SORTBY is the name of a field or formula, or a cell array of such names.
%    Items within each group will by sorted by their values for these
%    fields/formulas, and IDs within a group will be assigned in that order.
%    If SORTBY is empty, items within a group will be sorted in the order they
%    currently appear in the dataset.  NOTE: a function handle of the form
%    accepted by AddForm can also be given in place of an existing formula name.
%
% D.AssignIDs(..., INDS) performs the above operation only on the subset of
% items identified by INDS, which is a numeric or logical vector.  The IDNAME
% field will remain unchanged for items not identified by INDS.
%
% See also: AddForm, Write.

if ~isfield(T.items, idName), error('field "%s" does not exist', idName); end
if ~isequal(T.sizes.(idName), 1), error('field "%s" is not 1xN', idName); end

if nargin < 4, sortBy = {}; end
if ischar(groupBy) || isa(groupBy, 'function_handle'), groupBy = {groupBy}; end
if ischar(sortBy ) || isa(sortBy , 'function_handle'), sortBy  = {sortBy }; end
if isempty(groupBy) && isempty(sortBy), error('no group/sort criteria'); end
for i = 1 : numel(groupBy)
    T.CheckForm(groupBy{i}, sprintf('groupBy{%u}', i));
end
for i = 1 : numel(sortBy)
    T.CheckForm(sortBy{i}, sprintf('sortBy{%u}', i));
end
cols = [groupBy, sortBy];
gcol = numel(groupBy);

if nargin < 5, inds = ':'; end
inds = sort(T.CheckInds(inds, 'u'));
if isempty(inds), return; end

if any(T.items.(idName)(inds) ~= 0), error('field "%s" contains nonzero values', idName); end

keys = cell(1, numel(cols));
[keys{:}] = T.Retrieve(inds, cols{:});
for i = 1 : numel(cols)
    if ~isequal(size(keys{i}), [1 numel(inds)]), error('sort column %u: must have one value per item', i); end
    if ~iscell(keys{i}), keys{i} = num2cell(keys{i}); end
    keys{i} = keys{i}';
end
keys{end + 1} = num2cell(inds(:));
keys = [keys{:}];

[keys, temp] = sortrows(keys);
inds = inds(temp);

ids = zeros(1, numel(inds));
for i = 1 : numel(inds)
    if (i == 1) || ~isequal(keys(i - 1, 1 : gcol), keys(i, 1 : gcol))
        id = 1;
    else
        id = id + 1;
    end
    ids(i) = id;
end

T.UpdateField(idName, ids, inds);

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function Write(T, writer, objDef, pathDef, check, inds)

% D.Write(WRITER, OBJDEF, PATHDEF) writes item objects (such as images) to disk.
% It might make sense to do this when objects are being generated via time-
% consuming transformations.  The transformations can then be replaced by a
% simple read from disk.
%
%    WRITER is a handle to a function that writes an object to a path.  It must
%    take two arguments (an object and a path).  For example, @imwrite is such a
%    function.
%
%    OBJDEF is the name of the formula that outputs the object.  It may also be
%    a function handle of the form accepted by AddForm.
%
%    PATHDEF is the name of a field containing the path, or of a formula that
%    outputs the path, to which the object will be written.  It may also be a
%    function handle of the form accepted by AddForm.  Each item's path must be
%    unique.  The AssignIDs method can be useful for generating components of a
%    unique path.
%
% D.Write(..., CHECK) lets you control how checking for already-existing files
% is carried out:
%
%    CHECK=FALSE means no checking is done.
%
%    CHECK=TRUE means all paths are checked, and if any of them already exist,
%    the entire operation is cancelled before any items are written.
%
%    CHECK can also be a string, or a cell array of strings, representing file
%    extension(s) to be appended to each path before the existence check is
%    done.
%
% D.Write(..., CHECK, INDS) performs the above operation only for the items
% identified by INDS, which is a numeric or logical vector.
%
% See also: AddForm, AssignIDs.

if ~isa(writer, 'function_handle'), error('writer must be a function handle'); end

T.CheckForm(objDef , 'objDef' );
T.CheckForm(pathDef, 'pathDef');

if nargin < 5
    check = {''};
elseif islogical(check) && check
    check = {''};
elseif islogical(check) && ~check
    check = {};
elseif ischar(check)
    check = {check};
elseif iscellstr(check)
    check = unique(check);
else
    error('invalid extension');
end

if nargin < 6, inds = ':'; end
inds = sort(T.CheckInds(inds, 'u'));
if isempty(inds), return; end

T.pstep = 100;
T.ptotl = numel(inds);

paths = T.Retrieve(inds, pathDef);
if ~iscellstr(paths), error('paths must be a cell array of strings'); end
for i = 1 : numel(paths)
    if ~gl_isabspath(paths{i}), error('item %u: "%s" is not an absolute path', inds(i), paths{i}); end
end

[paths, temp] = sort(paths);
inds = inds(temp);
if any(strcmp(paths(1 : end - 1), paths(2 : end))), error('paths are not unique'); end

if ~isempty(check)
    T.Printf(1, 'CHECKING FOR EXISTING FILES');
    for i = 1 : numel(inds)
        for j = 1 : numel(check)
            path = paths{i};
            if ~isempty(check{j}), path = [path '.' check{j}]; end
            if exist(path, 'file'), error('"%s" already exists', path); end
        end
        T.Printf(2, i);
    end
    T.Printf(3);
end

T.Printf(1, 'WRITING OBJECTS');
current = '';
for i = 1 : numel(inds)
    path = paths{i};
    base = fileparts(path);
    if ~strcmp(base, current)
        [found, attr] = fileattrib(base);
        if found
            if ~attr.directory, error('"%s" is not a directory', base); end
        else
            if ~mkdir(base), error('unable to create directory "%s"', base); end
        end
        current = base;
    end
    obj = T.Retrieve(inds(i), objDef);
    if iscell(obj) && isscalar(obj), obj = obj{1}; end
    writer(obj, path);
    T.Printf(2, i);
end
T.Printf(3);

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function prev = Verbose(T, verb)

% D.Verbose(TF) sets dataset D's "verbose" flag to true or false.  Verbose mode
% turns on some status messages for processes that may take significant time,
% such as the Write method.
%
% TF = D.Verbose: retrives the current value of the verbose flag.
%
% PREV = D.Verbose(TF) sets the verbose flag and returns the previously-set
% value.

prev = ~T.quiet;

if nargin < 2, return; end

T.quiet = ~verb;

end
end

%-----------------------------------------------------------------------------------------------------------------------

properties (Access = private)
    versn = 5;
    props = struct;
    types = struct;
    count = 0;
    items = struct;
    sizes = struct;
    forms = struct;
    multi = struct;
    fopen = {};
    fclos = {};
end

properties (Transient, Access = private)
    cache = struct;
    quiet = true;
    opend = false;
    pstep = [];
    ptotl = [];
end

%-----------------------------------------------------------------------------------------------------------------------

methods (Hidden)
function s = saveobj(T)

fields = fieldnames(T.props);
props = struct;
for i = 1 : numel(fields)
    name = fields{i};
    props.(name) = SaveProp(T.cache.(name), T.props.(name), T.types.(name));
end

s.versn = T.versn;
s.props = props;
s.types = T.types;
s.count = T.count;
s.items = T.items;
s.sizes = T.sizes;
s.forms = T.forms;
s.multi = T.multi;
s.fopen = T.fopen;
s.fclos = T.fclos;

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods (Static, Hidden)
function T = loadobj(s)

T = vex_dataset;

if s.versn < T.versn
    s = ConvertOld(s);
end

T.props = s.props;
T.types = s.types;
T.count = s.count;
T.items = s.items;
T.sizes = s.sizes;
T.forms = s.forms;
T.multi = s.multi;
T.fopen = s.fopen;
T.fclos = s.fclos;

T.RecomputeProps(true);

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods (Hidden)
function display(T)

fprintf('\n');

fprintf('PROPERTIES:\n');
fields = fieldnames(T.props);
ilen = numel(num2str(numel(fields)));
nlen = max(cellfun(@numel, fields));
for i = 1 : numel(fields)
    name = fields{i};
    type = T.types.(name);
    desc = ArrayDesc(T.props.(name), type);
    if ~isempty(type), desc = [desc ' (' type ')']; end
    fprintf('  %*u. %*s: %s\n', ilen, i, nlen, name, desc);
end
fprintf('\n');

fprintf('ITEMS: %u\n', T.count);
fprintf('\n');

fprintf('FIELDS:\n');
fields = fieldnames(T.items);
ilen = numel(num2str(numel(fields)));
nlen = max(cellfun(@numel, fields));
for i = 1 : numel(fields)
    name = fields{i};
    siz = sprintf('%ux', T.sizes.(name));
    fprintf('  %*u. %*s: %sN %s\n', ilen, i, nlen, name, siz, class(T.items.(name)));
end
fprintf('\n');

fprintf('FORMULAS:\n');
fields = fieldnames(T.forms);
ilen = numel(num2str(numel(fields)));
nlen = max(cellfun(@numel, fields));
for i = 1 : numel(fields)
    name = fields{i};
    fprintf('  %*u. %*s: %s\n', ilen, i, nlen, fields{i}, func2str(T.forms.(name)));
end
fprintf('\n');

if ~isempty(T.fopen) || ~isempty(T.fclos)

    fprintf('OPEN HANDLERS:\n');
    ilen = numel(num2str(numel(T.fopen)));
    for i = 1 : numel(T.fopen)
        fprintf('  %*u. %s\n', ilen, i, func2str(T.fopen{i}));
    end
    fprintf('\n');

    fprintf('CLOSE HANDLERS:\n');
    ilen = numel(num2str(numel(T.fclos)));
    for i = numel(T.fclos) : -1 : 1
        fprintf('  %*u. %s\n', ilen, i, func2str(T.fclos{i}));
    end
    fprintf('\n');

    fprintf('OPEN: %s\n', ArrayDesc(T.opend));
    fprintf('\n');

end

fields = fieldnames(T.cache);
fields = fields(~ismember(fields, fieldnames(T.props)));

if ~isempty(fields)

    fprintf('TRANSIENT PROPERTIES:\n');
    nlen = max(cellfun(@numel, fields));
    for i = 1 : numel(fields)
        name = fields{i};
        desc = ArrayDesc(T.cache.(name));
        fprintf('  %*s: %s\n', nlen, name, desc);
    end
    fprintf('\n');

end

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods (Hidden)
function fields = fieldnames(T)

fields = [fieldnames(T.cache); fieldnames(T.items); fieldnames(T.forms)];

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods (Hidden)
function varargout = subsref(T, ids)

switch ids(1).type
case '.'

    name = ids(1).subs;

    if isfield(T.cache, name)
        type = 0;
    elseif isfield(T.items, name)
        type = 1;
    elseif isfield(T.forms, name)
        type = 2;
    else
        [varargout{1 : nargout}] = builtin('subsref', T, ids);
        return;
    end

    if type == 0
        val = T.cache.(name);
    elseif numel(ids) < 2
        if type == 1
            val = T.items.(name);
        else
            val = T.Retrieve(1 : T.count, name);
        end
    else
        if ~ismember(ids(2).type, {'()', '{}'}), error('invalid reference'); end
        args = ids(2).subs;
        inds = T.CheckInds(args{end});
        if type == 1
            colons = T.Colons(name);
            val = T.items.(name)(colons{:}, inds);
        else
            val = T.Retrieve(inds, name);
        end
        args{end} = ':';
        if (numel(args) == 1) && (ndims(val) == 2) && (size(val, 1) == 1), args = [{1} args]; end
        if numel(args) < ndims(val), error('invalid indices'); end
        if size(val, numel(args)) ~= numel(inds), error('invalid indices'); end
        ids(2).subs = args;
    end

    if numel(ids) == 1
        varargout{1} = val;
    else
        [varargout{1 : nargout}] = subsref(val, ids(2 : end));
    end

otherwise

    error('invalid reference');

end

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods (Hidden)
function delete(T)

T.Close;

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods (Access = private)
function CheckName(T, name)

if ~isvarname(name), error('name "%s" is invalid', name); end
if ismethod(T, name), error('name "%s" is reserved', name); end
if isstrprop(name(1), 'upper'), error('names cannot start with a capital letter'); end

if isfield(T.props, name), error('property "%s" already exists', name); end
if isfield(T.cache, name), error('transient property "%s" already exists', name); end
if isfield(T.items, name), error('field "%s" already exists', name); end
if isfield(T.forms, name), error('formula "%s" already exists', name); end

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods (Access = private)
function [fields, newName] = CheckMove(T, fields, name, newName, newPos)

[ans, pos] = ismember(name, fields);

if isempty(newName)
    newName = name;
elseif ~strcmp(newName, name)
    T.CheckName(newName);
end

if isempty(newPos)
    newPos = pos;
elseif isinf(newPos)
    newPos = numel(fields);
elseif (newPos >= 1) && (newPos <= numel(fields) + 1)
    if newPos > pos, newPos = newPos - 1; end
else
    error('invalid position');
end

fields(pos) = [];
fields = [fields(1 : newPos - 1); {newName}; fields(newPos : end)];

if strcmp(newName, name), newName = ''; end

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods (Access = private)
function CheckForm(T, def, name)

if isa(def, 'function_handle')
    if ~ismember(nargin(def), [1 2]), error('%s: function must take 1 or 2 inputs', name); end
elseif ischar(def)
    if ~isfield(T.items, def) && ~isfield(T.forms, def)
        error('%s: "%s" is not an existing field or formula', name, def);
    end
else
    error('%s: must provide an existing field name, formula name, or a function handle', name);
end

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods (Access = private)
function RecomputeProps(T, full)

s = T.cache;

T.cache = struct;

fields = fieldnames(T.props);
for i = 1 : numel(fields)
    name = fields{i};
    def = T.props.(name);
    if full || isa(def, 'function_handle')
        T.cache.(name) = ComputeProp(def, T.types.(name), T.cache);
    else        
        T.cache.(name) = s.(name);
    end
end

% TODO: remove dependency on property order (like formulas).

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods (Access = private)
function inds = CheckInds(T, inds, flag)

if nargin < 3, flag = 'n'; end

if islogical(inds)
    inds = find(inds);
    if ~isempty(inds) && (inds(end) > T.count), error('invalid indices'); end
elseif ischar(inds) && strcmp(inds, ':')
    inds = 1 : T.count;
elseif isnumeric(inds)
    if (flag == 'u') && (numel(unique(inds)) < numel(inds)), error('indices must be unique'); end
    if any(inds < 1) || any(inds > T.count), error('invalid indices'); end
else
    error('invalid indices');
end

if (flag == 's') && ~isscalar(inds), error('need a single index'); end

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods (Access = private)
function cnt = CheckItems(T, s, full)

fields = fieldnames(s);

if full
    if ~isempty(setxor(fields, fieldnames(T.items))), error('missing or invalid fields'); end
else
    if ~isempty(setdiff(fields, fieldnames(T.items))), error('invalid fields'); end
end

if isempty(fields)
    cnt = 0;
    return;
end

for i = 1 : numel(fields)
    name = fields{i};
    fsiz = T.sizes.(name);
    siz = size(s.(name));
    siz(end + 1 : numel(fsiz) + 1) = 1;
    if ~isequal(siz(1 : end - 1), fsiz), error('field "%s": size mismatch', name); end
    if i == 1
        cnt = siz(end);
    elseif siz(end) ~= cnt
        error('field "%s": number of items (%u) does not match field "%s" (%u)', name, siz(end), fields{1}, cnt);
    end
end

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods (Access = private)
function [cnt, s] = CheckDataset(T, U, ignore)

fields = fieldnames(T.items);

if ignore
    if ~isempty(setdiff(fields, fieldnames(U.items))), error('missing fields'); end
else
    if ~isempty(setxor(fields, fieldnames(U.items))), error('missing or invalid fields'); end
end

cnt = U.count;

s = struct;
for i = 1 : numel(fields)
    name = fields{i};
    if ~isequal(T.sizes.(name), U.sizes.(name)), error('field "%s": size mismatch', name); end
    s.(name) = U.items.(name);
end

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods (Access = private)
function varargout = Retrieve(T, inds, varargin)

S = vex_result(T, inds, T.cache);

for i = 1 : numel(varargin)
    varargout{i} = T.Retrieve2(S, varargin{i});
end

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods (Hidden)
function [val, ok] = Retrieve2(T, S, name)

if isa(name, 'function_handle')
    func  = name;
    multi = (nargin(name) == 1);
elseif isfield(T.forms, name)
    func  = T.forms.(name);
    multi = T.multi.(name);
elseif isfield(T.items, name)
    func = [];
else
    val = [];
    ok  = false;
    return;
end

inds = Inds(S);

if isempty(func)
    colons = T.Colons(name);
    val = T.items.(name)(colons{:}, inds);
elseif multi
    val = func(S);
elseif isempty(inds)
    val = [];
elseif isscalar(inds)
    val = func(S, 1);
else
    val = func(S, 1);
    dim = ndims(val);
    if (dim == 2) && (size(val, 2) == 1), dim = 1; end
    val = repmat(val, [ones(1, dim), numel(inds)]);
    colons = repmat({':'}, 1, dim);
    for i = 2 : numel(inds)
        val(colons{:}, i) = func(S, i);
    end
end

ok = true;

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods (Access = private)
function c = Colons(T, name)

c = repmat({':'}, 1, numel(T.sizes.(name)));

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods (Access = private)
function Printf(T, mode, arg)

if T.quiet, return; end

switch mode
case 0
    fprintf('%s\n', arg);
case 1
    fprintf('%s:', arg);
case 2
    if mod(arg, T.pstep) == 0
        fprintf(' %u', arg);
    end
case 3
    if mod(T.ptotl, T.pstep) == 0
        fprintf('\n');
    else
        fprintf(' %u\n', T.ptotl);
    end
end

end
end

%-----------------------------------------------------------------------------------------------------------------------

end

%***********************************************************************************************************************

function def = StripFunc(def)

if ~isa(def, 'function_handle'), return; end

def = str2func(func2str(def));

end

%***********************************************************************************************************************

function [use, store] = ComputeProp(def, type, s)

switch type
case ''
    use   = def;
    store = def;
case 'path'
    if isa(def, 'function_handle'), error('invalid path'); end
    [use, store] = gl_abspath(def);
otherwise
    error('invalid property type "%s"', type);
end

if isa(use, 'function_handle'), use = use(s); end

end

%***********************************************************************************************************************

function def = SaveProp(use, store, type)

switch type
case ''
    def = store;
case 'path'
    def = gl_savepath(use, store);
end

end

%***********************************************************************************************************************

function s = ArrayDesc(a, type)

if nargin < 2, type = ''; end

if ischar(a)
    s = sprintf('''%s''', a);
elseif strcmp(type, 'path') && iscellstr(a) && (numel(a) == 2)
    s = sprintf('{''%s'', ''%s''}', a{1}, a{2});
elseif (isnumeric(a) || islogical(a)) && isscalar(a)
    s = num2str(a);
elseif isa(a, 'double') && isequal(size(a), [0 0])
    s = '[]';
elseif isa(a, 'function_handle')
    s = func2str(a);
else
    siz = sprintf('%ux', size(a));
    s = sprintf('%s %s', siz(1 : end - 1), class(a));
end

end

%***********************************************************************************************************************

function s = ConvertOld(s)

if s.versn < 2
    s.fopen = [];
    s.fclos = [];
end

if s.versn < 3
    if isempty(s.fopen), s.fopen = {}; else s.fopen = {s.fopen}; end
    if isempty(s.fclos), s.fclos = {}; else s.fclos = {s.fclos}; end
end

if s.versn < 4
    s = rmfield(s, 'dform');
end

if s.versn < 5
    fields = fieldnames(s.props);
    for i = 1 : numel(fields)
        s.props.(fields{i}) = StripFunc(s.props.(fields{i}));
    end
    fields = fieldnames(s.forms);
    for i = 1 : numel(fields)
        s.forms.(fields{i}) = StripFunc(s.forms.(fields{i}));
    end
    for i = 1 : numel(s.fopen)
        s.fopen{i} = StripFunc(s.fopen{i});
    end
    for i = 1 : numel(s.fclos)
        s.fclos{i} = StripFunc(s.fclos{i});
    end
end

end