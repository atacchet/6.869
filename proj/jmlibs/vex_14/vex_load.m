function varargout = vex_load(path, varargin)

% Loads one or more vex_dataset objects from a saved .mat file.
%
% D = vex_load(PATH) assumes the .mat file (identified by PATH) contains a
% single variable which is the vex_dataset object.  You do not need to give
% the variable's name.
%
% [D1, D2, ...] = vex_load(PATH, N1, N2, ...) loads one or more vex_dataset
% objects from variables N1, N2, ... in the .mat file.
%
% See also: vex_save.

%***********************************************************************************************************************

% TODO: doc
path = gl_abspath(path);
if exist(path, 'dir'), path = fullfile(path, 'vex.mat'); end

s = gl_load(path, varargin{:});

if isempty(varargin)
    names = fieldnames(s);
    if numel(names) ~= 1, error('no variable name given'); end
else
    names = varargin;
end

varargout = cell(1, numel(names));
for i = 1 : numel(names)
    d = s.(names{i});
    if ~isa(d, 'vex_dataset'), error('"%s" is not a vex_dataset object', names{i}); end
    varargout{i} = d;
end

return;