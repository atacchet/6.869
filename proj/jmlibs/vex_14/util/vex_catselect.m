function [sel, rest] = vex_catselect(inds, cats, num, allCats)

if isempty(inds)
    inds = 1 : numel(cats);
elseif islogical(inds)
    inds = find(inds);
elseif ~isnumeric(inds)
    error('indices must be numeric');
end

if ~isnumeric(cats), error('categories must be numeric'); end
cats = cats(inds);

if isequal(num, inf)
    num = [0 inf];
elseif isscalar(num)
    num = [num num];
end
if (numel(num) ~= 2) || any(num < 0) || isinf(num(1)) || (num(1) > num(2))
    error('invalid range');
end

if nargin < 4, allCats = unique(cats); end

sel = zeros(1, num(1) * numel(allCats));
pos = 0;

for i = 1 : numel(allCats)

    this = inds(cats == allCats(i));
    if numel(this) < num(1), error('category %i: need at least %u items', allCats(i), num(1)); end
    if numel(this) > num(2)
        this = this(randperm(numel(this)));
        this = this(1 : num(2));
    end

    sel(pos + 1 : pos + numel(this)) = this;
    pos = pos + numel(this);

end

sel = sort(sel);

if nargout >= 2, rest = setdiff(inds, sel); end

return;