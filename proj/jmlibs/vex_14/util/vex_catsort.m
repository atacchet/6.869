function inds = vex_catsort(inds, cats, mode, varargin)

if isempty(inds)
    inds = 1 : numel(cats);
elseif islogical(inds)
    inds = find(inds);
elseif isnumeric(inds)
    inds = sort(inds);
else
    error('indices must be numeric');
end

if ~isnumeric(cats), error('categories must be numeric'); end
cats = cats(inds);

if nargin < 3, mode = 'prop'; end

allCats = unique(cats);

catInds = cell(1, numel(allCats));
for i = 1 : numel(allCats)
    catInds{i} = inds(cats == allCats(i));
end

switch mode
case 'prop', inds = SortProp(allCats, catInds, numel(inds), varargin{:});
otherwise  , error('invalid mode');
end

return;

%***********************************************************************************************************************

function inds = SortProp(allCats, catInds, n)

for i = 1 : numel(allCats)
    catInds{i} = catInds{i}(randperm(numel(catInds{i})));
end

used = zeros(1, numel(allCats));
norm = zeros(1, numel(allCats));

inds = zeros(1, n);

for i = 1 : numel(inds)
    j = find(norm == min(norm));
    if ~isscalar(j), j = j(randi(numel(j))); end
    used(j) = used(j) + 1;
    norm(j) = used(j) / numel(catInds{j});
    inds(i) = catInds{j}(used(j));
end

return;