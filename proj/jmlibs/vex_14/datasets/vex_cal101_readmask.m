function [mask, win] = vex_cal101_readmask(maskPath, imSize)

cat = fileparts(maskPath);
[ans, cat] = fileparts(cat);
if strcmp(cat, 'BACKGROUND_Google')
    mask = true(imSize);
    win  = [1 imSize(2) 1 imSize(1)]';
    return;
end

s = load(maskPath);
ys = s.obj_contour(2, :) + s.box_coord(1);
xs = s.obj_contour(1, :) + s.box_coord(3);

mask = roipoly(imSize(1), imSize(2), xs, ys);

yc = 0.5 * (min(ys) + max(ys));
xc = 0.5 * (min(xs) + max(xs));
yw = min(abs([1 imSize(1)] - yc));
xw = min(abs([1 imSize(2)] - xc));
y1 = round(yc - yw);
y2 = round(yc + yw);
x1 = round(xc - xw);
x2 = round(xc + xw);
win = [x1 x2 y1 y2]';

return;