function vex_cal101_make(over)

if nargin < 1, over = false; end

base  = '/cbcl/scratch01/datasets/cal101';
imDir = 'images';
anDir = 'masks';

saveFile = fullfile(base, 'vex.mat');
if ~over && exist(saveFile, 'file'), error('"%s" already exists', saveFile); end

list = dir(fullfile(base, imDir));
list = {list([list.isdir] & ~ismember({list.name}, {'.', '..'})).name};
[ans, inds] = sort(lower(list));
catNames = list(inds);

catIDs = cell(1, numel(catNames));
exIDs  = cell(1, numel(catNames));

for i = 1 : numel(catNames)

    list = dir(fullfile(base, imDir, catNames{i}, '*.jpg'));
    list = {list(~[list.isdir]).name};
    [ans, inds] = sort(lower(list));
    list = list(inds);

    catIDs{i} = repmat(i, 1, numel(list));

    exIDs{i} = zeros(1, numel(list));
    for j = 1 : numel(list)
        exIDs{i}(j) = str2double(list{j}(7 : 10));
    end

end

catIDs = [catIDs{:}];
exIDs  = [exIDs{:}];

d = vex_dataset;
d.AddProp ('imBase'  , {base, imDir}, 'path');
d.AddProp ('anBase'  , {base, anDir}, 'path');
d.AddProp ('catNames', catNames);
d.AddField('imgID'   , 1 : numel(catIDs));
d.AddField('catID'   , catIDs);
d.AddField('exID'    , exIDs);
d.AddField('height'  , 0);
d.AddField('width'   , 0);
d.AddField('color'   , false);
d.AddField('win'     , zeros(4, 1));
d.AddForm ('catName' , @(x)   x.catNames(x.catID));
d.AddForm ('imPath'  , @(x)   vex_paths(x.imBase, x.catName, 'image_%04u.jpg'     , x.exID));
d.AddForm ('anPath'  , @(x)   vex_paths(x.anBase, x.catName, 'annotation_%04u.mat', x.exID));
d.AddForm ('image'   , @(x,i) {vex_imread(x.imPath{i})});
d.AddForm ('mask'    , @(x,i) {vex_cal101_readmask(x.anPath{i}, [x.height(i) x.width(i)])});
d.AddForm ('aspect'  , @(x)   x.width ./ x.height);
d.AddForm ('item'    , @(x)   x.image);
d.AddForm ('desc'    , @(x)   x.imPath);

height = zeros(1, d.NumItems);
width  = zeros(1, d.NumItems);
color  = false(1, d.NumItems);
win    = zeros(4, d.NumItems);

for i = 1 : d.NumItems
    imPath = d.imPath{i};
    anPath = d.anPath{i};
    fprintf('reading %s\n', imPath);
    im = imread(imPath);
    [ans, w] = vex_cal101_readmask(anPath, [size(im, 1), size(im, 2)]);
    height(i) = size(im, 1);
    width (i) = size(im, 2);
    color (i) = (size(im, 3) > 1);
    win(:, i) = w;
end

d.UpdateField('height', height);
d.UpdateField('width' , width );
d.UpdateField('color' , color );
d.UpdateField('win'   , win   );

vex_save(saveFile, d);

system(sprintf('chmod g=u "%s"', saveFile));

return;