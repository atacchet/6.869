function vex_pascal07_make(over)

if nargin < 1, over = false; end

base  = '/cbcl/scratch01/datasets/Pascal2007';
imDir = 'images/JPEGImages';
anDir = 'images/Annotations';
spDir = 'images/ImageSets/Main';
cdDir = 'code/VOCcode';

saveFile_im  = fullfile(base, 'vex_im.mat');
saveFile_obj = fullfile(base, 'vex_obj.mat');
if ~over && exist(saveFile_im , 'file'), error('"%s" already exists', saveFile_im ); end
if ~over && exist(saveFile_obj, 'file'), error('"%s" already exists', saveFile_obj); end

gl_addpath(fullfile(base, cdDir));

list = dir(fullfile(base, imDir, '*.jpg'));
list = {list.name};
imgIDs = zeros(1, numel(list));
for i = 1 : numel(list)
    imgIDs(i) = str2double(strtok(list{i}, '.'));
end
if ~isequal(imgIDs, 1 : numel(list)), error('missing image numbers'); end
nIm = imgIDs(end);

setNames = {'train', 'val', 'test'};
setIDs   = zeros(1, nIm);
h = fopen(fullfile(base, spDir, 'train.txt'), 'r');
inds = textscan(h, '%f');
setIDs(inds{1}) = 1;
fclose(h);
h = fopen(fullfile(base, spDir, 'val.txt'), 'r');
inds = textscan(h, '%f');
setIDs(inds{1}) = 2;
fclose(h);
h = fopen(fullfile(base, spDir, 'test.txt'), 'r');
inds = textscan(h, '%f');
setIDs(inds{1}) = 3;
fclose(h);

height = zeros(1, nIm);
width  = zeros(1, nIm);

nObj = 0;
catNames = {};

for i = 1 : nIm

    anPath = fullfile(base, anDir, sprintf('%06u.xml', i));
    fprintf('step 1: reading %s\n', anPath);
    r = VOCreadxml(anPath);
    r = r.annotation;

    height(i) = str2double(r.size.height);
    width (i) = str2double(r.size.width);

    nObj = nObj + numel(r.object);
    catNames = union(catNames, {r.object.name});

end

numObjs = zeros(numel(catNames), nIm);

imind  = zeros(1, nObj);
catIDs = zeros(1, nObj);
pose   = cell (1, nObj);
trunc  = false(1, nObj);
diff   = false(1, nObj);
win    = zeros(4, nObj);

k = 0;
for i = 1 : nIm

    anPath = fullfile(base, anDir, sprintf('%06u.xml', i));
    fprintf('step 2: reading %s\n', anPath);
    r = VOCreadxml(anPath);
    r = r.annotation;

    for j = 1 : numel(r.object)
        k = k + 1;

        w = zeros(4, 1);
        w(1) = str2double(r.object(j).bndbox.xmin);
        w(2) = str2double(r.object(j).bndbox.xmax);
        w(3) = str2double(r.object(j).bndbox.ymin);
        w(4) = str2double(r.object(j).bndbox.ymax);

        [ans, cat] = ismember(r.object(j).name, catNames);
        if cat == 0, error('category not found'); end

        numObjs(cat, i) = numObjs(cat, i) + 1;

        imind (1, k) = i;
        catIDs(1, k) = cat;
        pose  {1, k} = r.object(j).pose;
        trunc (1, k) = strcmp(r.object(j).truncated, '1');
        diff  (1, k) = strcmp(r.object(j).difficult, '1');
        win   (:, k) = w;

    end
    
end

[poseNames, ans, poseIDs] = unique(pose);

a = vex_dataset;

a.AddProp ('base'    , {base, imDir}, 'path');
a.AddProp ('setNames', setNames);
a.AddProp ('catNames', catNames);
a.AddField('imgID'   , imgIDs);
a.AddField('setID'   , setIDs);
a.AddField('imHeight', height);
a.AddField('imWidth' , width);
a.AddForm ('path'    , @(x)   vex_paths(x.base, '%06u.jpg', x.imgID));
a.AddForm ('setName' , @(x)   x.setNames(x.setID));
a.AddForm ('image'   , @(x,i) {vex_imread(x.path{i})});

b = a.Copy;
b.SelectItems(imind);

a.AddField('numObj'   , numObjs);
a.AddForm ('totalObjs', @(x) sum(x.numObj, 1));
a.AddForm ('numCats'  , @(x) sum(x.numObj > 0, 1));
a.AddForm ('item'     , @(x) x.image);
a.AddForm ('desc'     , @(x) x.path);

b.AddProp ('poseNames', poseNames);
b.AddProp ('margin'   , 0);
b.AddField('objID'    , 1 : numel(imind));
b.AddField('catID'    , catIDs);
b.AddField('poseID'   , poseIDs);
b.AddField('trunc'    , trunc);
b.AddField('diff'     , diff);
b.AddField('win'      , win);
b.AddForm ('catName'  , @(x)   x.catNames(x.catID));
b.AddForm ('poseName' , @(x)   x.poseNames(x.poseID));
b.AddForm ('object'   , @(x,i) {vex_window(x.image{i}, x.win(:, i), x.margin)});
b.AddForm ('item'     , @(x)   x.object);
b.AddForm ('desc'     , @(x,i) sprintf('%s: %s @ (%.2f, %.2f)', x.path{i}, x.catName{i}, ...
                               mean(x.win(1:2,i)), mean(x.win(3:4,i))));

vex_save(saveFile_im , a);
vex_save(saveFile_obj, b);

system(sprintf('chmod g=u "%s"', saveFile_im ));
system(sprintf('chmod g=u "%s"', saveFile_obj));

gl_rmpath(fullfile(base, cdDir));

return;