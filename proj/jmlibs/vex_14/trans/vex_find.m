function inds = vex_find(vals, list)

[ans, inds] = ismember(vals, list);

if any(inds == 0), error('invalid values'); end

return;