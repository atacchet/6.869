function paths = vex_paths(varargin)

if nargin < 1, error('need at least one argument'); end

argcnt = [];
scalar = false(1, nargin);
nitems = 1;

for i = 1 : nargin
    if iscell(varargin{i})
        argcnt(end + 1) = 0;
    elseif ischar(varargin{i})
        varargin{i} = varargin(i);
        argcnt(end + 1) = 0;
    elseif isnumeric(varargin{i})
        if isempty(argcnt), error('argument 1 must be a string or a cell array of strings'); end
        argcnt(end) = argcnt(end) + 1;
    else
        error('argument %u is invalid', i);
    end
    n = numel(varargin{i});
    if n == 1
        scalar(i) = true;
    elseif nitems == 1
        nitems = n;
    elseif n ~= nitems
        error('argument %u must have %u elements or be scalar', i, nitems);
    end
end

paths = cell(1, nitems);
for i = 1 : nitems
    args = cell(1, numel(argcnt));
    p = 1;
    for j = 1 : numel(argcnt)
        if scalar(p)
            args(j) = varargin{p}(1);
        else
            args(j) = varargin{p}(i);
        end
        p = p + 1;
        if argcnt(j) > 0
            nums = cell(1, argcnt(j));
            for k = 1 : argcnt(j)
                if scalar(p)
                    nums(k) = varargin(p);
                else
                    nums{k} = varargin{p}(i);
                end
                p = p + 1;
            end
            args{j} = sprintf(args{j}, nums{:});
        end
    end
    paths{i} = fullfile(args{:});
end

return;