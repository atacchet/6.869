function path = vex_path(varargin)

path = vex_paths(varargin{:});

if ~isscalar(path), error('result must be a single path'); end

path = path{1};

return;