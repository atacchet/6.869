function obj = vex_window(im, win, marg)

if nargin < 3, marg = 0; end

x1 = max(win(1) - marg, 1);
x2 = min(win(2) + marg, size(im, 2));
y1 = max(win(3) - marg, 1);
y2 = min(win(4) + marg, size(im, 1));

obj = im(y1 : y2, x1 : x2, :);

return;