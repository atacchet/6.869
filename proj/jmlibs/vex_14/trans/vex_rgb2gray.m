function im = vex_rgb2gray(im)

if ndims(im) == 2, return; end

im = rgb2gray(im);

return;