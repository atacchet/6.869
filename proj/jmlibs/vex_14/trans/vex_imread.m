function im = vex_imread(path)

persistent lastPath;
persistent lastIm;

if isempty(lastPath) || ~strcmp(path, lastPath)
    im = imread(path);
    lastPath = path;
    lastIm   = im;
else
    im = lastIm;
end

return;