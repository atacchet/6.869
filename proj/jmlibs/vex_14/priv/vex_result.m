classdef vex_result < handle

%-----------------------------------------------------------------------------------------------------------------------

properties (Transient, Access = private)
    T;
    inds;
    cache;
    buf;
end

%-----------------------------------------------------------------------------------------------------------------------

methods (Hidden)
function S = vex_result(T, inds, cache)

S.T     = T;
S.inds  = inds;
S.cache = cache;
S.buf   = struct;

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods (Hidden)
function varargout = subsref(S, ids)

switch ids(1).type
case '.'

    name = ids(1).subs;

    if isfield(S.cache, name)
        val   = S.cache.(name);
        check = false;
    elseif isfield(S.buf, name)
        val   = S.buf.(name);
        check = true;
    else
        [val, ok] = S.T.Retrieve2(S, name);
        if ~ok
            [varargout{1 : nargout}] = builtin('subsref', S, ids);
            return;
        end
        S.buf.(name) = val;
        check = true;
    end

    if numel(ids) == 1
        varargout{1} = val;
    else
        if check
            if ~ismember(ids(2).type, {'()', '{}'}), error('invalid reference'); end
            args = ids(2).subs;
            ind = args{end};
            if ~isnumeric(ind) || ~isscalar(ind), error('invalid indices'); end
            if (numel(args) == 1) && (ndims(val) == 2) && (size(val, 1) == 1), args = [{1} args]; end
            if numel(args) < ndims(val), error('invalid indices'); end
            if size(val, numel(args)) ~= numel(S.inds), error('invalid indices'); end
            ids(2).subs = args;
        end
        [varargout{1 : nargout}] = subsref(val, ids(2 : end));
    end

otherwise

    error('invalid reference');

end

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods (Hidden)
function inds = Inds(T)

inds = T.inds;

end
end

%-----------------------------------------------------------------------------------------------------------------------

end