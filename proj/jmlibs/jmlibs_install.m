function startup

% Automatically called on MATLAB startup.

%***********************************************************************************************************************

addpath(fullfile(jmlibs_root, 'jmlibs', 'gl_4'));
addpath(fullfile(jmlibs_root, 'utils'));
gl_addpath(jmlibs_root);

gl_initrand;


return;
