function varargout = dm_read(tpath, varargin)

tpath = gl_abspath([tpath '.mat']);

if isempty(varargin)

    if nargout > 1, error('too many outputs'); end
    varargout{1} = gl_load(tpath);

elseif isscalar(varargin) && iscellstr(varargin{1})

    if nargout > 1, error('too many outputs'); end
    if isempty(varargin{1})
        if ~exist(tpath, 'file'), error('"%s" does not exist', tpath); end
        varargout{1} = struct;
    else
        varargout{1} = gl_load(tpath, varargin{1}{:});
        if numel(fieldnames(varargout{1})) ~= numel(varargin{1}), error('field(s) missing'); end
    end

elseif iscellstr(varargin)

    if max(nargout, 1) ~= numel(varargin), error('need %u outputs', numel(varargin)); end
    s = gl_load(tpath, varargin{:});
    for i = 1 : numel(varargin)
        varargout{i} = s.(varargin{i});
    end

else

    error('invalid arguments');

end

return;