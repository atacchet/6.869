function ok = dm_deletefile(path)

ok = dm_quiet(@delete, path);

return;