function dm_writefile(path, lines)

if nargin < 2, lines = {'X'}; end

h = fopen(path, 'w');
if h < 0, error('unable to write file "%s"', path); end

for i = 1 : numel(lines)
    fprintf(h, '%s\n', lines{i});
end

fclose(h);

return;