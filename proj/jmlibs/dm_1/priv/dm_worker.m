classdef dm_worker < handle

%-----------------------------------------------------------------------------------------------------------------------

properties

    jname = '';
    jdir  = '';

    host      = '';
    procID    = '';
    logFile   = '';
    errFile   = '';
    runFile   = '';
    stepFile  = '';
    haltOnErr = false;
    haltFile  = '';

    tasks  = {};
    names  = {};
    ready  = [];
    cur    = [];
    dKeys  = {};
    dTasks = {};

    err  = [];
    halt = false;
    stop = 0;

end

%-----------------------------------------------------------------------------------------------------------------------

methods
function Run(W, jname, opt)

if nargin < 3, opt = struct; end

if ~isfield(opt, 'procID'   ), opt.procID    = ''   ; end
if ~isfield(opt, 'runFile'  ), opt.runFile   = ''   ; end
if ~isfield(opt, 'stepFile' ), opt.stepFile  = ''   ; end
if ~isfield(opt, 'haltOnErr'), opt.haltOnErr = false; end

W.jname = jname;
W.jdir  = gl_abspath(jname);

dm_prepdir(W.jdir);

W.host = dm_hostname;

W.StartLog(opt.procID, opt.runFile);

W.stepFile  = opt.stepFile;
W.haltOnErr = opt.haltOnErr;
W.haltFile  = fullfile(W.jdir, 'error.mat');

dm_halt('');

pause on;
more off;

try
    W.Run2;
    err = [];
catch err
end

W.StopLog(err, ~isempty(opt.runFile));

if ~isempty(err), rethrow(err); end

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods (Access = private)
function StartLog(W, procID, runFile)

if isempty(procID) ~= isempty(runFile)
    error('must supply both procID and runFile, or neither');
end

if isempty(procID)

    time    = datestr(now, 30);
    attempt = 1;

    while true

        procID = sprintf('%s_%s_%u', strrep(W.host, '.', '-'), time, dm_pid);
        if attempt > 1, procID = [procID '_' sprintf('%u', attempt)]; end

        logFile = fullfile(W.jdir, 'log', [procID '.log']);
        errFile = fullfile(W.jdir, 'log', [procID '.mat']);
        runFile = fullfile(W.jdir, 'log', [procID '.run']);

        if ~exist(logFile, 'file') && ~exist(errFile, 'file') && ~exist(runFile, 'file'), break; end

        attempt = attempt + 1;

    end

    dm_writefile(logFile, {});
    dm_writefile(runFile);

else

    logFile = fullfile(W.jdir, 'log', [procID '.log']);
    errFile = fullfile(W.jdir, 'log', [procID '.mat']);

    if  exist(logFile, 'file'), error('log file "%s" already exists', logFile); end
    if  exist(errFile, 'file'), error('err file "%s" already exists', errFile); end
    if ~exist(runFile, 'file'), error('run file "%s" does not exist', runFile); end

    dm_writefile(logFile, {});

end

W.procID  = procID;
W.logFile = logFile;
W.errFile = errFile;
W.runFile = runFile;

diary(W.logFile);

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods (Access = private)
function StopLog(W, err, keepRun)

diary off;

if ~isempty(err)
    h = fopen(W.logFile, 'a');
    fprintf(h, '??? %s\n', err.getReport);
    fclose(h);
end

if ~keepRun, dm_deletefile(W.runFile); end

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods (Access = private)
function Run2(W)

W.tasks  = {};
W.names  = {};
W.ready  = [];
W.cur    = [];
W.dKeys  = {};
W.dTasks = {};

W.err  = [];
W.halt = false;
W.stop = 0;

W.JobPrintf('starting job\n');

serr = [];

if W.haltOnErr
    sHaltFile = W.haltFile;
else
    sHaltFile = '';
end

while true

    try
        dm_lockrun(W.jdir, sHaltFile, @RunSerial, W);
        serr = [];
    catch serr
        break;
    end

    if ~isempty(W.err), break; end
    if W.halt, break; end
    if W.stop, break; end
    if isempty(W.tasks), break; end

    if isempty(W.cur)
        W.WriteStep;
        W.JobPrintf('waiting for other processors\n');
        pause(gl_envvar('dm_waitsleep', 60));
    else
        try
            W.WriteStep;
            W.cur.Execute;
        catch err
            W.err = err;
        end
    end

    if (W.stop == 0) && ~exist(W.runFile, 'file'), W.stop = 1; end

end

W.WriteStep(true);

if ~isempty(serr)
    W.JobPrintf('synchronization error\n\nError file: %s\n\n', W.errFile);
    SaveErr(W.errFile, serr);
    rethrow(serr);
elseif ~isempty(W.err)
    W.JobPrintf('job terminated\n\nError file: %s\n\n', W.errFile);
    SaveErr(W.errFile, W.err);
    rethrow(W.err);
elseif W.halt
    W.JobPrintf('halted\n');
elseif W.stop
    W.JobPrintf('stopped\n');
else
    W.JobPrintf('all tasks complete\n');
end

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods (Access = private)
function WriteStep(W, done)

if nargin < 2, done = false; end

if isempty(W.stepFile), return; end

if done
    desc = '';
    sort = 'zzzzzzzz';
elseif isempty(W.cur)
    desc = 'waiting';
    sort = 'zzzzzzzy';
else
    desc = W.cur.step.desc;
    sort = [W.cur.name ' ' W.cur.step.sort];
end

dm_writefile(W.stepFile, {desc, sort});

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods (Access = private)
function RunSerial(W)

if ~isempty(W.err) || (W.stop == 2)
    W.cur.Release;
elseif ~isempty(W.cur)
    W.cur.Save;
end

if ~isempty(W.err) && isempty(strfind(W.err.message, '@HALT@'))
    % This worker just generated an ordinary error.
    if W.haltOnErr && ~exist(W.haltFile, 'file')
        SaveErr(W.haltFile, W.err);
    end
elseif exist(W.haltFile, 'file')
    % Some other worker has halted the job.
    W.err  = [];
    W.halt = true;
elseif ~isempty(W.err)
    % This worker just called dm_halt.
    SaveErr(W.haltFile, dm_halt);
    W.err  = [];
    W.halt = true;
end
if ~isempty(W.err), return; end
if W.halt, return; end
if W.stop, return; end

W.GetStatus;

% Delete tasks we don't need anymore.
keep = true(1, numel(W.tasks));
for i = 1 : numel(W.tasks)
    a = W.tasks{i};
    if ~a.done, continue; end
    keep(i) = false;
    for j = 1 : numel(a.keepFor)
        [ans, k] = ismember(a.keepFor{j}, W.names);
        if (k > 0) && ~W.tasks{k}.done
            keep(i) = true;
            break;
        end
        % TODO: should we get rid of expired 'keepFors' here?
    end
    if ~keep(i)
        a.Remove;
        a.TaskPrintf('deleted result\n');
        delete(a);
    end
end
W.tasks = W.tasks(keep);
W.names = W.names(keep);

% Have we left a task?
prev = W.cur;
if ~isempty(W.tasks) && W.ready(1)
    W.cur = W.tasks{1};
else
    W.cur = [];
end
if ~isempty(prev) && isvalid(prev) && (isempty(W.cur) || (W.cur ~= prev))
    prev.Close;
end

% Are we starting something?
if ~isempty(W.cur)
    W.cur.Hold;
end

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods (Access = private)
function GetStatus(W)

keep   = false(1, numel(W.tasks));
ntasks = {};

list = dir(W.jdir);
[ans, inds] = sort({list.name});
list = list(inds);

j = 0;
while j < numel(list)
    j = j + 1;

    if ~any(list(j).name == '.'), continue; end

    i = j;
    name = [strtok(list(i).name, '.'), '.'];
    list(i).name(1 : numel(name)) = '';
    if strcmp(list(i).name, 'dm.mat')
        p = i;
    else
        p = 0;
    end
    while (j < numel(list)) && ~isempty(strmatch(name, list(j + 1).name))
        j = j + 1;
        list(j).name(1 : numel(name)) = '';
        if (p == 0) && strcmp(list(j).name, 'dm.mat'), p = j; end
    end
    if p == 0, continue; end
    name(end) = '';

    [ans, k] = ismember(name, W.names);
    if k == 0
        ntasks{end + 1} = W.GetTaskStatus(name, list(i : j), p - i + 1);
    else
        keep(k) = true;
        ntasks{end + 1} = W.GetTaskStatus(W.tasks{k}, list(i : j), p - i + 1);
    end

end

for i = find(~keep)
    delete(W.tasks{i});
end

W.tasks = ntasks;

W.names = cell (numel(W.tasks), 1);
W.ready = false(numel(W.tasks), 1);
dates   = zeros(numel(W.tasks), 1);
for i = 1 : numel(W.tasks)
    W.names{i} = W.tasks{i}.name;
    W.ready(i) = W.tasks{i}.ready;
    dates  (i) = W.tasks{i}.created;
end

for i = 1 : numel(W.tasks)
    for j = 1 : numel(W.tasks{i}.depends)
        if ~W.ready(i), break; end
        [ans, k] = ismember(W.tasks{i}.depends{j}, W.names);
        if (k > 0) && ~W.tasks{k}.done, W.ready(i) = false; end
    end
end

[ans, inds] = sortrows([~W.ready, dates]);
W.tasks = W.tasks(inds);
W.names = W.names(inds);
W.ready = W.ready(inds);

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods (Access = private)
function na = GetTaskStatus(W, a, list, p)

if ischar(a)
    name = a;
    a = [];
else
    name = a.name;
end

if any([list.isdir]), error('invalid files found'); end

if isempty(a) || (a.datenum ~= list(p).datenum)
    if ~isempty(a), delete(a); end
    s = gl_load(fullfile(W.jdir, [name '.' list(p).name]));
    na = dm_active.Construct(W, s, name, list(p).datenum);
else
    na = a;
end

na.UpdateState(list);

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function ReadDeps(W, A)

[ans, inds] = ismember(A.depkeys, W.dKeys);
for i = find(inds == 0)
    W.dKeys {end + 1} = A.depkeys{i};
    W.dTasks{end + 1} = [];
    inds(i) = numel(W.dKeys);
end
W.dKeys  = W.dKeys (inds);
W.dTasks = W.dTasks(inds);

for i = 1 : numel(W.dKeys)
    if ~isempty(W.dTasks{i}), continue; end
    form = W.dKeys{i}(1);
    path = W.dKeys{i}(3 : end);
    if ~any(path == '/'), path = fullfile(W.jdir, path); end
    switch form
    case 's', W.dTasks{i} = dm_read(path);
    case 't', W.dTasks{i} = dm_construct(path);
    end
end

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function CacheResult(W, key, t)

if ismember(key, W.dKeys)
    % Should never occur.
    error('"%s" is already in the cache!', key);
end

W.dKeys {end + 1} = key;
W.dTasks{end + 1} = t;

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function Printf(W, format, varargin) %#ok

dm_printf(format, varargin{:});

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function JobPrintf(W, format, varargin)

format = ['%s, %s: %s: ' format];
args   = {W.host, datestr(clock, 31), W.jname, varargin{:}};

W.Printf(format, args{:});

end
end

%-----------------------------------------------------------------------------------------------------------------------

end

%***********************************************************************************************************************

function SaveErr(path, err)

if isstruct(err)
    gl_save(path, err);
elseif ischar(err)
    message = err; %#ok
    gl_save(path, 'message');
else
    gl_save(path, 'err');
end

end
