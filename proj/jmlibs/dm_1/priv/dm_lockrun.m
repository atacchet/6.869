function varargout = dm_lockrun(jdir, errFile, func, varargin)

lockFile  = fullfile(jdir, 'log', 'lock');
lockSleep = gl_envvar('dm_locksleep', 1);

while true

    fd = dm_lock(lockFile);
    if ~isempty(fd), break; end

    pause(lockSleep);

end

try
    [varargout{1 : nargout}] = feval(func, varargin{:});
    err = [];
catch err
    if ~isempty(errFile) && ~exist(errFile, 'file')
        gl_save(errFile, 'err');
    end
end

dm_unlock(fd);

if ~isempty(err), rethrow(err); end

return;