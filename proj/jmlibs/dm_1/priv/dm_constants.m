function s = dm_constants(class, name, dflt)

if nargin < 2, name = ''; end
if nargin < 3, dflt = []; end

persistent cache;
if isempty(cache), cache = struct; end

if ~isfield(cache, class)
    cache.(class) = GetConstants(class);
end
s = cache.(class);

if ~isempty(name)
    if isfield(s, name)
        s = s.(name);
    else
        s = dflt;
    end
end

return;

%***********************************************************************************************************************

function s = GetConstants(class)

s.dims = struct;

s = Traverse(class, s);

return;

%***********************************************************************************************************************

function s = Traverse(class, s)

m = meta.class.fromName(class);

for i = 1 : numel(m.SuperClasses)
    super = m.SuperClasses{i}.Name;
    if any(ismember({'dm_task', 'dm_task_h'}, [{super}; superclasses(super)]))
        s = Traverse(super, s);
    end
end

for i = 1 : numel(m.Properties)
    name = m.Properties{i}.Name;
    if m.Properties{i}.Constant && m.Properties{i}.HasDefault && ~isempty(strmatch('dm_', name))
        s = AddProp(class, s, name(4 : end), m.Properties{i}.DefaultValue);
    end
end

return;

%***********************************************************************************************************************

function s = AddProp(class, s, name, value)

if ~isempty(strmatch('dim_', name))

    field = name(5 : end);
    if isempty(field), error('"%s": "dm_%s" invalid', class, name); end
    if isfield(s.dims, field), error('"%s": "dm_%s" duplicated', class, name); end
    s.dims.(field) = value;

elseif ismember(name, {'dims'})

    error('"%s": "dm_%s" invalid', class, name);

else
    
    s.(name) = value;

end
        
return;