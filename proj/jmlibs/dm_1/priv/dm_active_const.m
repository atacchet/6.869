classdef dm_active_const < dm_active

%-----------------------------------------------------------------------------------------------------------------------

methods
function Load(A, W, s, name, datenum)

A.Load@dm_active(W, s, name, datenum);

if ~isempty(A.T.dm_holdTime)
    error('cannot specify holdTime for constant tasks');
end

A.holdTime = 0;

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function UpdateState(A, list)

A.done  = true;
A.ready = false;

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function Execute(A) %#ok
    error('should not be called for a constant task');
end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function Save(A) %#ok
    error('should not be called for a constant task');
end
end

%-----------------------------------------------------------------------------------------------------------------------

end