classdef dm_active < handle

%-----------------------------------------------------------------------------------------------------------------------

methods (Static)
function A = Construct(W, s, name, datenum)

super = superclasses(s.dm_class);

if any(ismember({'dm_task_const', 'dm_task_const_h'}, super))
    A = dm_active_const;
elseif any(ismember({'dm_task_single', 'dm_task_single_h'}, super))
    A = dm_active_single;
elseif any(ismember({'dm_task_batch', 'dm_task_batch_h'}, super))
    A = dm_active_batch;
else
    error('invalid class "%s"', s.dm_class);
end

A.Load(W, s, name, datenum);

end
end

%-----------------------------------------------------------------------------------------------------------------------

properties

    W;
    T;
    temp;
    depends;
    created;
    keepFor;
    name;
    datenum;
    depkeys;
    holdTime;

    done     = false;
    ready    = false;
    holdFile = '';
    intFile  = '';
    matFile  = '';
    step     = struct;

end

%-----------------------------------------------------------------------------------------------------------------------

methods
function Load(A, W, s, name, datenum)

A.W       = W;
A.T       = dm_construct(rmfield(s, {'dm_temp', 'dm_depends', 'dm_created', 'dm_keepFor'}));
A.temp    = s.dm_temp;
A.depends = s.dm_depends;
A.created = s.dm_created;
A.keepFor = s.dm_keepFor;
A.name    = name;
A.datenum = datenum;

A.depkeys = cell(1, numel(A.depends));
args = dm_constants(s.dm_class, 'dm_args', '');
for i = 1 : numel(A.depends)
    if ischar(args)
        arg = args;
    else
        arg = args{i};
    end
    if strcmp(arg, 'struct')
        A.depkeys{i} = ['s*' A.depends{i}];
    else
        A.depkeys{i} = ['t*' A.depends{i}];
    end
end

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function Close(A) %#ok

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function delete(A)

A.Close;

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods (Abstract)
    UpdateState(A, list);
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function Hold(A)

path = fullfile(A.W.jdir, A.holdFile);

if ~dm_link(A.W.logFile, path), error('unable to create "%s"', path); end

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods (Abstract)
    Execute(A);
    Save(A);
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function Release(A)

dm_deletefile(fullfile(A.W.jdir, A.holdFile));
dm_deletefile(fullfile(A.W.jdir, A.intFile )); % Might be needed if task has errored out.

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function Remove(A)

dm_deletefile(fullfile(A.W.jdir, [A.name '.*']));

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function TaskPrintf(A, format, varargin)

format = ['%s, %s: %s, %s: ' format];
args   = {A.W.host, datestr(clock, 31), A.W.jname, A.name, varargin{:}};

A.W.Printf(format, args{:});

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function s = Struct(A)

s = A.T.DMStruct;

s.dm_temp    = A.temp;
s.dm_depends = A.depends;
s.dm_created = A.created;
s.dm_keepFor = A.keepFor;

end
end

%-----------------------------------------------------------------------------------------------------------------------

end