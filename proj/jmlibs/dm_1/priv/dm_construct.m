function T = dm_construct(s)

if ischar(s), s = gl_load([s '.mat']); end

T = feval(s.dm_class);

T = dm_modmethod(T, 'DMLoad', rmfield(s, 'dm_class'));

return;