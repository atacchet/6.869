function match = dm_fnmatch(name, expr)

match = ~isempty(regexp(name, ['^' expr '$'], 'once'));

return;