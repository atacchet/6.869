function valid = dm_holdvalid(datenum, holdTime)

valid = (now <= datenum + holdTime / (24 * 60 * 60));

return;