function [U, varargout] = dm_copymethod(T, method, varargin)

if nargin < 2, method = ''; end

if isa(T, 'handle')
    U = dm_construct(T.DMStruct);
    if ~isempty(method)
        [varargout{1 : nargout - 1}] = U.(method)(varargin{:});
    end
else
    if isempty(method)
        U = T;
    else
        [U, varargout{1 : nargout - 1}] = T.(method)(varargin{:});
    end
end

return;