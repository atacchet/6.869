function [T, varargout] = dm_modmethod(T, method, varargin)

if isa(T, 'handle')
    [varargout{1 : nargout - 1}] = T.(method)(varargin{:});
else
    [T, varargout{1 : nargout - 1}] = T.(method)(varargin{:});
end

return;