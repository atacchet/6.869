classdef dm_active_single < dm_active

%-----------------------------------------------------------------------------------------------------------------------

methods
function Load(A, W, s, name, datenum)

A.Load@dm_active(W, s, name, datenum);

if isempty(A.T.dm_holdTime)
    A.holdTime = gl_envvar('dm_holdtime', 60 * 60);
else
    A.holdTime = A.T.dm_holdTime;
end

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function UpdateState(A, list)

keys = zeros(1, numel(list));

for i = 1 : numel(list)

    name = list(i).name;

    if strcmp(name, 'dm.mat')
        keys(i) = 0;
    elseif dm_fnmatch(name, 'hold\.[^.]+') % single hold (procID)
        keys(i) = 1;
    elseif dm_fnmatch(name, 'save\.[^.]+\.mat') % single intermediate (procID)
        keys(i) = 2;
    elseif strcmp(name, 'mat') % result
        keys(i) = 3;
    else
        error('invalid task file "%s.%s"', A.name, name);
    end

end

[keys, inds] = sort(keys);
list = list(inds);

A.done = false;

err  = 0;
hold = 0;

for i = 2 : numel(list)

    name = list(i).name;

    switch keys(i)
    case 1 % single hold (procID)

        if strcmp(dm_fnpart(name, 2), A.W.procID), err = 1; break; end
        hold = max(hold, 1 + dm_holdvalid(list(i).datenum, A.holdTime));

    case 2 % single intermediate (procID)

        if strcmp(dm_fnpart(name, 2), A.W.procID), err = 1; break; end

    case 3 % result

        if ~A.temp, err = 1; break; end
        A.done = true;

    end

end

if err > 0, error('invalid task file "%s.%s" (error %u-%u)', A.name, name, keys(i), err); end

A.ready = ~A.done && (hold <= 1);

if A.ready
    A.holdFile  = sprintf('%s.hold.%s'    , A.name, A.W.procID);
    A.intFile   = sprintf('%s.save.%s.mat', A.name, A.W.procID);
    A.matFile   = sprintf('%s.mat'        , A.name);
    A.step.desc = A.name;
    A.step.sort = '0';
end

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function Execute(A)

A.TaskPrintf('starting\n');

A.W.ReadDeps(A);

u = dm_copymethod(A.T, 'DMBuild', A.W.dTasks{:});

u = dm_modmethod(u, 'DMCleanup');
A.W.CacheResult(['t*' A.name], u);

s = u.DMResult;

A.TaskPrintf('completed\n');

gl_save(fullfile(A.W.jdir, A.intFile), s);

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function Save(A)

path1 = fullfile(A.W.jdir, A.intFile);
path2 = fullfile(A.W.jdir, A.matFile);
if exist(path2, 'file')
    % Should only happen if somebody's hold expired.
    dm_deletefile(path1);
    A.TaskPrintf('discarded\n');
else
    movefile(path1, path2);
end

A.Release;

if ~A.temp
    dm_deletefile(fullfile(A.W.jdir, [A.name '.dm.mat']));
end

end
end

%-----------------------------------------------------------------------------------------------------------------------

end