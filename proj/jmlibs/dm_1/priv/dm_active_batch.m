classdef dm_active_batch < dm_active

%-----------------------------------------------------------------------------------------------------------------------

methods
function Load(A, W, s, name, datenum)

A.Load@dm_active(W, s, name, datenum);

if isempty(A.T.dm_holdTime)
    A.holdTime = gl_envvar('dm_holdtime_batch', gl_envvar('dm_holdtime', 60 * 60));
else
    A.holdTime = A.T.dm_holdTime;
end

if isscalar(A.holdTime), A.holdTime = repmat(A.holdTime, 1, 3); end

A.dims = dm_constants(class(A.T), 'dims');

end
end

%-----------------------------------------------------------------------------------------------------------------------

properties (Access = private)

    dims;

    stage   = -1;
    total   = -1;
    batchNo = 0;

    U    = [];
    open = false;

end

%-----------------------------------------------------------------------------------------------------------------------

methods
function Close(A)

if A.open && ismethod(A.T, 'DMClose')
    A.U.DMClose;
end

A.U    = [];
A.open = false;

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function UpdateState(A, list)

keys = zeros(1, numel(list));

for i = 1 : numel(list)

    name = list(i).name;

    if strcmp(name, 'dm.mat')
        keys(i) = 0;
    elseif dm_fnmatch(name, '0\.hold\.[^.]+') % start hold (procID)
        keys(i) = 1;
    elseif dm_fnmatch(name, '0\.save\.[^.]+\.mat') % start intermediate (procID)
        keys(i) = 2;
    elseif dm_fnmatch(name, '0+\.\d+\.mat') % start result (total)
        keys(i) = 3;
    elseif dm_fnmatch(name, '\d+\.hold\.[^.]+') % batch hold (batch, procID)
        keys(i) = 4;
    elseif dm_fnmatch(name, '\d+\.save\.[^.]+.mat') % batch intermediate (batch, procID)
        keys(i) = 5;
    elseif dm_fnmatch(name, '\d+\.mat') % batch result (batch)
        keys(i) = 6;
    elseif dm_fnmatch(name, 'hold\.[^.]+') % final hold (procID)
        keys(i) = 7;
    elseif dm_fnmatch(name, 'save\.[^.]+\.mat') % final intermediate (procID)
        keys(i) = 8;
    elseif strcmp(name, 'mat') % result
        keys(i) = 9;
    else
        error('invalid task file "%s.%s"', A.name, name);
    end

end

[keys, inds] = sort(keys);
list = list(inds);

A.done = false;

err   = 0;
stage = 0;
shold = 0;
total = 0;
bstat = [];
chold = 0;

for i = 2 : numel(list)

    name = list(i).name;

    switch keys(i)
    case 1 % start hold (procID)

        if strcmp(dm_fnpart(name, 3), A.W.procID), err = 1; break; end
        shold = max(shold, 1 + dm_holdvalid(list(i).datenum, A.holdTime(1)));

    case 2 % start intermediate (procID)

        if strcmp(dm_fnpart(name, 3), A.W.procID), err = 1; break; end

    case 3 % start result (total)

        total = str2double(dm_fnpart(name, 2));
        bstat = zeros(1, total);
        if total == 0, stage = 2; else stage = 1; end

    case 4 % batch hold (batch, procID)

        if stage < 1, continue; end
        b = str2double(dm_fnpart(name, 1));
        if (b < 1) || (b > total), err = 1; break; end
        if strcmp(dm_fnpart(name, 3), A.W.procID), err = 2; break; end
        bstat(b) = max(bstat(b), 1 + dm_holdvalid(list(i).datenum, A.holdTime(2)));

    case 5 % batch intermediate (batch, procID)

        if stage < 1, continue; end
        b = str2double(dm_fnpart(name, 1));
        if (b < 1) || (b > total), err = 1; break; end
        if strcmp(dm_fnpart(name, 3), A.W.procID), err = 2; break; end

    case 6 % batch result (batch)

        if stage < 1, err = 1; break; end
        b = str2double(dm_fnpart(name, 1));
        if (b < 1) || (b > total), err = 2; break; end
        bstat(b) = 3;
        if (b == total) && all(bstat == 3), stage = 2; end

    case 7 % final hold (procID)

        if stage < 2, continue; end
        if strcmp(dm_fnpart(name, 2), A.W.procID), err = 1; break; end
        chold = max(chold, 1 + dm_holdvalid(list(i).datenum, A.holdTime(3)));

    case 8 % final intermediate (procID)

        if stage < 2, continue; end
        if strcmp(dm_fnpart(name, 2), A.W.procID), err = 1; break; end

    case 9 % result

        if ~A.temp, err = 1; break; end
        if stage > 0, err = 2; break; end
        stage = 3;
        A.done = true;

    end

end

if err > 0, error('invalid task file "%s.%s" (error %u-%u)', A.name, name, keys(i), err); end

A.stage = stage;

switch stage
case 0

    A.ready = (shold <= 1);

    if A.ready
        A.holdFile  = sprintf('%s.0.hold.%s'        , A.name, A.W.procID);
        A.intFile   = sprintf('%s.0.save.%s.mat'    , A.name, A.W.procID);
        A.matFile   = sprintf('%s.0%%0*u.0%%0*u.mat', A.name);
        A.step.desc = sprintf('%s (starting)', A.name);
        A.step.sort = '0';
        A.total     = -1;
        A.batchNo   = 0;
    end

case 1

    [stat, b] = min([bstat 3]);
    A.ready = (stat <= 1);

    if A.ready
        width = numel(num2str(total));
        A.holdFile  = sprintf('%s.0%0*u.hold.%s'    , A.name, width, b, A.W.procID);
        A.intFile   = sprintf('%s.0%0*u.save.%s.mat', A.name, width, b, A.W.procID);
        A.matFile   = sprintf('%s.0%0*u.mat'        , A.name, width, b);
        A.step.desc = sprintf('%s (%u/%u)', A.name, b, total);
        A.step.sort = sprintf('%0*u', width, b);
        A.total     = total;
        A.batchNo   = b;
    end

case 2

    A.ready = (chold <= 1);

    if A.ready
        A.holdFile  = sprintf('%s.hold.%s'    , A.name, A.W.procID);
        A.intFile   = sprintf('%s.save.%s.mat', A.name, A.W.procID);
        A.matFile   = sprintf('%s.mat'        , A.name);
        A.step.desc = sprintf('%s (combining)', A.name);
        A.step.sort = 'X';
        A.total     = total;
        A.batchNo   = 0;
    end

case 3

    A.ready = false;

end

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function Execute(A)

A.W.ReadDeps(A);

switch A.stage
case 0   , s = A.ExecuteStart;
case 1   , s = A.ExecuteBatch;
case 2   , s = A.ExecuteFinal;
otherwise, error('invalid stage'); % should never occur
end

gl_save(fullfile(A.W.jdir, A.intFile), s);

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods (Access = private)
function s = ExecuteStart(A)

A.TaskPrintf('initializing batch task\n');

A.U = dm_copymethod(A.T, 'DMStart', A.W.dTasks{:});

numItems  = A.U.dm_numItems;
batchSize = A.U.dm_batchSize;
if isempty(numItems), error('dm_numItems not set'); end
if isempty(batchSize) || (batchSize < 1), error('dm_batchSize not set'); end

b = A.U.dm_array;
if isempty(b)
    b = [];
elseif isnumeric(b) && isscalar(b)
    if ~ismethod(A.T, 'DMCombine'), error('no DMCombine method'); end
elseif isa(b, 'bigarray')
    if b.State > 1, error('dm_array is not empty'); end
else
    error('invalid dm_array');
end
A.U.dm_array = b;

total = ceil(numItems / batchSize);
width = numel(num2str(total));
A.matFile = sprintf(A.matFile, width, 0, width, total);

s = A.U.DMStruct;

A.TaskPrintf('initialization complete\n');

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods (Access = private)
function s = ExecuteBatch(A)

if isempty(A.U)
    width = numel(num2str(A.total));
    s = gl_load(fullfile(A.W.jdir, sprintf('%s.0%0*u.0%0*u.mat', A.name, width, 0, width, A.total)));
    A.U = dm_construct(s);
end
if ~A.open
    if ismethod(A.T, 'DMOpen')
        A.U = dm_modmethod(A.U, 'DMOpen', A.W.dTasks{:});
    end
    A.open = true;
end

startNo  = (A.batchNo - 1) * A.U.dm_batchSize + 1;
numItems = min(A.U.dm_batchSize, A.U.dm_numItems - startNo + 1);

A.TaskPrintf('starting batch %u/%u (%u-%u)\n', A.batchNo, A.total, startNo, startNo + numItems - 1);

r          = struct;
numColumns = 0;
numVars    = 0;
auto       = false;

stopCheck = gl_envvar('dm_stopcheck', 30);
stopTic = tic;

for i = 1 : numItems

    if toc(stopTic) >= stopCheck
        if ~exist(A.W.runFile, 'file')
            A.W.stop = 2;
            break;
        end
        stopTic = tic;
    end

    itemNo = startNo + i - 1;

    time = tic;
    item = A.U.DMItem(itemNo, A.W.dTasks{:});
    time = toc(time);

    if ~isstruct(item)
        item2 = struct;
        item2.v = item;
        item = item2;
        auto = true;
    end

    if isfield(item, 'desc')
        desc = item.desc;
        item = rmfield(item, 'desc');
        if isempty(desc), desc = 'done'; end
    else
        desc = 'done';
    end

    A.W.Printf('%u/%u: %s (%.3fs)\n', itemNo, A.U.dm_numItems, desc, time);

    if i == 1

        varNames = fieldnames(item)';

        numVars = numel(varNames);
        dims    = zeros(1, numVars);
        colons  = cell(1, numVars);
        for j = 1 : numVars
            [dims(j), colons{j}, initialSize] = ...
                GetVarInfo(A.dims, varNames{j}, size(item.(varNames{j})), numItems);
            r.(varNames{j}) = PreAllocate(class(item.(varNames{j})), initialSize);
        end

    end

    itemColumns = size(item.(varNames{1}), dims(1) + 1);
    if (itemColumns ~= 1) && ~isempty(A.U.dm_array)
        error('item %u: must return a single result', itemNo);
    end

    for j = 1 : numVars

        if (j ~= 1) && (size(item.(varNames{j}), dims(j) + 1) ~= itemColumns)
            error('item %u, variable "%s": size of last dimension does not match', itemNo, varNames{j});
        end

        if itemColumns ~= 0
            r.(varNames{j})(colons{j}{:}, numColumns + 1 : numColumns + itemColumns) = item.(varNames{j});
        end

    end

    numColumns = numColumns + itemColumns;

end

if numColumns < numItems
    for j = 1 : numVars
        r.(varNames{j}) = r.(varNames{j})(colons{j}{:}, 1 : numColumns);
    end
end

s = struct;

s.num = numColumns;
s.var = r;

if A.batchNo == 1
    s.auto = auto;
end

if A.W.stop
    A.TaskPrintf('stopped in batch %u\n', A.batchNo);
else
    A.TaskPrintf('completed batch %u\n', A.batchNo);
end

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods (Access = private)
function s = ExecuteFinal(A)

if isempty(A.U)
    width = numel(num2str(A.total));
    s = gl_load(fullfile(A.W.jdir, sprintf('%s.0%0*u.0%0*u.mat', A.name, width, 0, width, A.total)));
    A.U = dm_construct(s);
elseif A.open
    if ismethod(A.T, 'DMClose')
        A.U = dm_modmethod(A.U, 'DMClose');
    end
    A.open = false;
end

A.TaskPrintf('finalizing batch task\n');

if isempty(A.U.dm_array)
    A.Combine;
else
    A.CombineBig;
end

A.U = dm_modmethod(A.U, 'DMCleanup');
A.W.CacheResult(['t*' A.name], A.U);

s = A.U.DMResult;

A.U = [];

A.TaskPrintf('completed\n');

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods (Access = private)
function Combine(A)

width = numel(num2str(A.total));

r          = struct;
numColumns = 0;
auto       = false;

for i = 1 : A.total

    s = gl_load(fullfile(A.W.jdir, sprintf('%s.0%0*u.mat', A.name, width, i)));
    itemColumns = s.num;
    item        = s.var;

    if i == 1

        auto = s.auto;

        varNames = fieldnames(item)';

        numVars = numel(varNames);
        dims    = zeros(1, numVars);
        colons  = cell(1, numVars);
        for j = 1 : numVars
            [dims(j), colons{j}, initialSize] = ...
                GetVarInfo(A.dims, varNames{j}, size(item.(varNames{j})), A.U.dm_numItems);
            r.(varNames{j}) = PreAllocate(class(item.(varNames{j})), initialSize);
        end

    end

    for j = 1 : numVars

        if size(item.(varNames{j}), dims(j) + 1) ~= itemColumns
            error('batch %u, variable "%s": size of last dimension does not match', i, varNames{j});
        end

        if itemColumns ~= 0
            r.(varNames{j})(colons{j}{:}, numColumns + 1 : numColumns + itemColumns) = item.(varNames{j});
        end

    end

    numColumns = numColumns + itemColumns;

end

if numColumns < A.U.dm_numItems
    for j = 1 : numVars
        r.(varNames{j}) = r.(varNames{j})(colons{j}{:}, 1 : numColumns);
    end
end

if ismethod(A.T, 'DMCombine')
    if auto, r = r.v; end
    A.U = dm_modmethod(A.U, 'DMCombine', r, A.W.dTasks{:});
else
    A.U = dm_modmethod(A.U, 'DMLoad', r);
end

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods (Access = private)
function CombineBig(A)

width = numel(num2str(A.total));

b = A.U.dm_array;
if isnumeric(b)
    blockSize = b;
    if blockSize == 0, blockSize = A.U.dm_batchSize; end
    b = bigarray_mat({A.W.jdir, fullfile('big', A.name)});
    pass = true;
else
    blockSize = b.BlockSize;
    if isempty(blockSize), blockSize = A.U.dm_batchSize; end
    if b.State > 1, error('dm_array is not empty'); end
    pass = false;
end
b.Init(blockSize, width + 1);

if isa(b, 'bigarray_mat') && (b.BlockSize == A.U.dm_batchSize)
    A.CopyFast(b);
else
    A.CopySlow(b);
end

if ~pass, b = []; end
if ismethod(A.T, 'DMCombine')
    A.U = dm_modmethod(A.U, 'DMCombine', b, A.W.dTasks{:});
end

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods (Access = private)
function CopyFast(A, b)

width = numel(num2str(A.total));

if A.total > 0
    src = fullfile(A.W.jdir, sprintf('%s.0%0*u.mat', A.name, width, 1));
    s = gl_load(src);
    if s.auto
        b.Append(s.var.v);
    else
        b.Append(s.var);
    end
    b.Flush;
    delete(src);
end

for i = 2 : A.total
    src = fullfile(A.W.jdir, sprintf('%s.0%0*u.mat', A.name, width, i));
    dst = sprintf('%s.%0*u.mat', b.Base, b.Digits, i);
    movefile(src, dst);
end

b.Refresh;

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods (Access = private)
function CopySlow(A, b)

width = numel(num2str(A.total));
auto  = false;

for i = 1 : A.total

    s = gl_load(fullfile(A.W.jdir, sprintf('%s.0%0*u.mat', A.name, width, i)));
    if i == 1, auto = s.auto; end

    if auto
        b.Append(s.var.v);
    else
        b.Append(s.var);
    end

end

b.Flush;

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function Save(A)

path1 = fullfile(A.W.jdir, A.intFile);
path2 = fullfile(A.W.jdir, A.matFile);
if exist(path2, 'file')
    % Should only happen if somebody's hold expired.
    dm_deletefile(path1);
    A.TaskPrintf('discarded\n');
else
    movefile(path1, path2);
end

A.Release;

if A.stage == 2
    dm_deletefile(fullfile(A.W.jdir, [A.name '.0*.mat']));
    if ~A.temp
        dm_deletefile(fullfile(A.W.jdir, [A.name '.dm.mat']));
    end
end

end
end

%-----------------------------------------------------------------------------------------------------------------------

end

%***********************************************************************************************************************

function [dim, colons, initialSize] = GetVarInfo(dimensions, varName, itemSize, initialCount)

if isfield(dimensions, varName)
    dim = dimensions.(varName);
    if dim == 0, error('invalid dimensionality'); end
else
    dim = 1;
end

colons = repmat({':'}, 1, dim);

itemSize(end + 1 : dim) = 1;
initialSize = [itemSize(1 : dim) initialCount];

end

%***********************************************************************************************************************

function array = PreAllocate(class, size)

if strcmp(class, 'cell')
    array = cell(size);
else
    array = repmat(feval(class, 0), size);
end

end