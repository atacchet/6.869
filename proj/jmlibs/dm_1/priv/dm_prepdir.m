function dm_prepdir(jdir, create)

if nargin < 2, create = false; end

[found, attr] = fileattrib(jdir);
if found
    if ~attr.directory, error('"%s" is not a directory', jdir); end
else
    if ~create, error('directory "%s" does not exist', jdir); end
    if ~mkdir(jdir), error('unable to create directory "%s"', jdir); end
end

logDir = fullfile(jdir, 'log');
[found, attr] = fileattrib(logDir);
if found
    if ~attr.directory, error('"%s" is not a directory', logDir); end
else
    if ~mkdir(logDir), error('unable to create directory "%s"', logDir); end
end

lockFile = fullfile(jdir, 'log', 'lock');
[found, attr] = fileattrib(lockFile);
if found
    if attr.directory, error('"%s" is a directory', lockFile); end
else
    dm_writefile(lockFile);
end

return;