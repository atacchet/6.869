function [ok, varargout] = dm_quiet(func, varargin)

warning off all;
lastwarn('');

[varargout{1 : nargout - 1}] = func(varargin{:});

ok = isempty(lastwarn);
warning on all;

return;