function p = dm_halt(q)

persistent halt;
if isempty(halt), halt = ''; end

p = halt;

if nargin >= 1
    halt = q;
    if ~ischar(halt) || ~isempty(halt), error('@HALT@'); end
end

return;