classdef dm_task

%-----------------------------------------------------------------------------------------------------------------------

properties (Transient, Hidden)
    dm_holdTime = [];
end

%-----------------------------------------------------------------------------------------------------------------------

methods (Hidden)
function T = DMLoad(T, s)

if strcmp(class(T), 'dm_task'), error('invalid class'); end

fields = fieldnames(s);
for i = 1 : numel(fields)
    name = fields{i};
    T.(name) = s.(name);
end

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods (Hidden)
function T = DMCleanup(T)

names = T.DMFields(true);
for i = 1 : numel(names)
    T.(names{i}) = [];
end

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods (Sealed)
function s = DMResult(T)

s = struct;

s.dm_class = class(T);

names = T.DMFields(false);
for i = 1 : numel(names)
    s.(names{i}) = T.(names{i});
end

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods (Sealed)
function s = DMStruct(T)

s = struct;

s.dm_class = class(T);

names = T.DMFields([]);
for i = 1 : numel(names)
    s.(names{i}) = T.(names{i});
end
    
end
end

%-----------------------------------------------------------------------------------------------------------------------

methods (Sealed, Access = private)
function names = DMFields(T, trans)

names = {};

m = metaclass(T);
for i = 1 : numel(m.Properties)
    if m.Properties{i}.Constant , continue; end
    if m.Properties{i}.Dependent, continue; end
    if ~isempty(trans) && (m.Properties{i}.Transient ~= trans), continue; end
    names{end + 1} = m.Properties{i}.Name;
end

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods (Sealed)
function r = isfield(T, fields)

if ischar(fields), fields = {fields}; end

r = false(size(fields));

for i = 1 : numel(fields)
    try
        ans = isempty(T.(fields{i}));
        r(i) = true;
    catch
        r(i) = false;
    end
end

end
end

%-----------------------------------------------------------------------------------------------------------------------

end