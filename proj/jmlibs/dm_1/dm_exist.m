function r = dm_exist(tpath)

r = exist(gl_abspath([tpath '.mat']), 'file');

return;