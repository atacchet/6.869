function dm_create(jdir, list)

jdir = gl_abspath(jdir);

[list, tasks, old] = Parse(jdir, list);

dm_prepdir(jdir, true);

dm_lockrun(jdir, '', @Create, jdir, list, tasks, old);

return;

%***********************************************************************************************************************

function [list, tasks, old] = Parse(jdir, list)

if isstruct(list), list = {list}; end

names = {};

for i = 1 : numel(list)

    s = list{i};

    if ~isfield(s, 'dm_name') || isempty(s.dm_name), error('task %u: missing name', i); end
    if ismember(s.dm_name, names), error('task %u: duplicate name "%s"', i, s.dm_name); end
    if ~all(isstrprop(s.dm_name, 'alphanum') | (s.dm_name == '_'))
        error('task %u: invalid name "%s"', i, s.dm_name);
    end
    if ismember(s.dm_name, {'big', 'error', 'log'}), error('task %u: invalid name "%s"', i, s.dm_name); end
    names{i} = s.dm_name;

end

temp = false(1, numel(list));
used = false(1, numel(list));
old  = struct;

common = {};

for i = 1 : numel(list)

    s = list{i};

    if ~isfield(s, 'dm_class') || isempty(s.dm_class)
        error('task "%s": missing class', s.dm_name);
    end
    if strcmp(s.dm_class, 'dm_struct')
        error('task "%s": invalid class "%s"', s.dm_name, s.dm_class);
    elseif strcmp(s.dm_class, 'struct')
        s.dm_class = 'dm_struct';
    elseif ~any(ismember({'dm_task', 'dm_task_h'}, superclasses(s.dm_class)))
        error('task "%s": invalid class "%s"', s.dm_name, s.dm_class);
    end
    deps = dm_constants(s.dm_class, 'dm_args', '');

    if ~isfield(s, 'dm_temp'   ), s.dm_temp    = false; end
    if ~isfield(s, 'dm_depends'), s.dm_depends = {}   ; end

    s.dm_depends = [common, s.dm_depends(:)'];
    if isfield(s, 'dm_common')
        if s.dm_common, common{end + 1} = s.dm_name; end
        s = rmfield(s, 'dm_common');
    end

    if any(ismember({'dm_task_const', 'dm_task_const_h'}, superclasses(s.dm_class)))
        % TODO: maybe just require that any dependencies already exist.
        if ~isempty(s.dm_depends), error('task "%s" cannot have any dependencies', s.dm_name); end
    end

    temp(i) = s.dm_temp;

    CheckNumDeps(s.dm_name, deps, numel(s.dm_depends));
    for j = 1 : numel(s.dm_depends)
        dname = s.dm_depends{j};
        if any(dname == '/')
            dname = gl_abspath(dname, jdir);
            s.dm_depends{j} = dname;
            info = GetDepInfo(jdir, dname);
            if ~info.exist, error('task "%s": dependency "%s" does not exist', s.dm_name, dname); end
            CheckDepClass(s.dm_name, deps, j, dname, info.class);
        else
            [ans, k] = ismember(dname, names);
            if k == 0
                if ~isfield(old, dname), old.(dname).keepFor2 = {}; end
                old.(dname).keepFor2{end + 1} = s.dm_name;
            elseif k < i
                CheckDepClass(s.dm_name, deps, j, dname, list{k}.dm_class);
                used(k) = true;
                if temp(k), list{k}.dm_keepFor{end + 1} = s.dm_name; end
            else
                error('task "%s": cannot depend on task "%s"', s.dm_name, dname);
            end
        end
    end

	s.dm_created = now;
    pause(0.01);

    s.dm_keepFor = {};

    list{i} = s;

end

i = find(temp & ~used, 1, 'first');
if ~isempty(i), error('task "%s": temporary task not used', list{i}.dm_name); end

tasks = cell(1, numel(list));

for i = 1 : numel(list)

    s = list{i};
    if strcmp(s.dm_class, 'dm_struct'), s = Separate(s); end

    a = dm_active.Construct([], rmfield(s, 'dm_name'), s.dm_name, 0);

    if isa(a.T, 'dm_task_const') || isa(a.T, 'dm_task_const_h')
        if ismethod(a.T, 'DMBuild')
            a.T = dm_modmethod(a.T, 'DMBuild');
        end
    end

    tasks{i} = a;

end

return;

%***********************************************************************************************************************

function Create(jdir, list, tasks, old)

dnames = fieldnames(old);

% Make sure new tasks don't already exist.

for i = 1 : numel(tasks)
    a = tasks{i};
    if ~isempty(dir(fullfile(jdir, [a.name '.*'])))
        error('files for task "%s" already exist', a.name);
    end
end

% Make sure any existing dependencies (in same dir) exist.

for i = 1 : numel(dnames)
    info = GetDepInfo(jdir, dnames{i}, old.(dnames{i}));
    if ~info.exist, error('dependency "%s" does not exist', dnames{i}); end
    old.(dnames{i}) = info;
end

% Check types of any existing dependencies (in same dir).

for i = 1 : numel(tasks)
    a = tasks{i};
    deps = dm_constants(class(a.T), 'dm_args', '');
    for j = 1 : numel(a.depends)
        dname = a.depends{j};
        if any(dname == '/') || ~isfield(old, dname), continue; end
        info = old.(dname);
        CheckDepClass(a.name, deps, j, dname, info.class);
    end
end

% Create new tasks.

for i = 1 : numel(tasks)
    a = tasks{i};
    const = isa(a.T, 'dm_task_const') || isa(a.T, 'dm_task_const_h');
    if ~const || a.temp
        s = a.Struct;
        gl_save(fullfile(jdir, [a.name '.dm.mat']), s);
    end
    if const
        if isa(a.T, 'dm_struct')
            [ans, s] = Separate(list{i});
        else
            s = a.T.DMResult;
        end
        gl_save(fullfile(jdir, [a.name '.mat']), s);
    end
end

% Update any existing temporary tasks (in same dir).

for i = 1 : numel(dnames)
    info = old.(dnames{i});
    if ~info.temp, continue; end
    for j = 1 : numel(info.keepFor2)
        if ismember(info.keepFor2{j}, info.keepFor), continue; end
        info.keepFor{end + 1} = info.keepFor2{j};
    end
    dm_keepFor = info.keepFor;
    save(fullfile(jdir, [dnames{i} '.dm.mat']), '-append', 'dm_keepFor');
end

return;

%***********************************************************************************************************************

function CheckNumDeps(name, deps, n)

if ischar(deps), return; end

if n ~= numel(deps)
    error('task "%s": %u dependencies required', name, numel(deps));
end

return;

%***********************************************************************************************************************

function CheckDepClass(name, deps, j, dname, dclass)

if ischar(deps)
    needed = deps;
else
    needed = deps{j};
end

if strcmp(needed, 'struct'), return; end

if isempty(needed), needed = {'dm_task', 'dm_task_h'}; end

if ~any(ismember(needed, [{dclass}; superclasses(dclass)]))
    error('task "%s": dependency "%s" is the wrong class', name, dname);
end

return;

%***********************************************************************************************************************

function info = GetDepInfo(jdir, name, info)

if nargin < 3, info = struct; end

if any(name == '/')
    fn = name;
else
    fn = fullfile(jdir, name);
end

if any(name == '/')
    a = [];
else
    try
        a = gl_load([fn '.dm.mat'], 'dm_class', 'dm_temp', 'dm_keepFor');
    catch
        a = [];
    end
end

if exist([fn '.mat'], 'file')
    try
        r = gl_load([fn '.mat'], 'dm_class');
    catch
        r.dm_class = 'struct';
    end
else
    r = [];
end

info.exist = ~isempty(a) || ~isempty(r);
if ~info.exist, return; end

if any(name == '/') || isempty(a)
    info.temp    = false;
    info.keepFor = {};
else
    info.temp    = a.dm_temp;
    info.keepFor = a.dm_keepFor;
end

if ~isempty(r)
    info.class = r.dm_class;
else
    info.class = a.dm_class;
end

return;

%***********************************************************************************************************************

function [d, r] = Separate(s)

d = struct;
r = struct;

fields = fieldnames(s);
for i = 1 : numel(fields)
    if strmatch('dm_', fields{i})
        d.(fields{i}) = s.(fields{i});
    else
        r.(fields{i}) = s.(fields{i});
    end
end

return;