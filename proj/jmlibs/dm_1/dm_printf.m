function dm_printf(format, varargin)

fprintf(format, varargin{:});

if strcmp(get(0, 'Diary'), 'on')
    diary off;
    diary on;
end

return;