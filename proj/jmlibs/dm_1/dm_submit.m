function dm_submit(jdir, varargin)

jdir = gl_abspath(jdir);

if ~isempty(varargin) && isstruct(varargin{1})
    opt = varargin{1};
    varargin(1) = [];
else
    opt = struct;
end

if ~isfield(opt, 'haltOnErr'), opt.haltOnErr = false; end

lines = {};
lines{end + 1} = 'opt = struct;';
lines{end + 1} = 'opt.procID = ''%baseFileName%'';';
lines{end + 1} = 'opt.runFile = ''%runFilePath%'';';
lines{end + 1} = 'opt.stepFile = ''%stepFilePath%'';';
lines{end + 1} = sprintf('opt.haltOnErr = %u;', opt.haltOnErr);
lines{end + 1} = sprintf('dm_build(''%s'', opt);', jdir);

cm_submit(jdir, lines, varargin{:});

return;