#include "mex.h"
#include <sys/unistd.h>

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {

    if (nrhs != 0) mexErrMsgTxt("incorrect number of arguments");
    if (nlhs >  1) mexErrMsgTxt("incorrect number of outputs");

    plhs[0] = mxCreateDoubleScalar((double)getpid());

}
