#include "mex.h"
#include <sys/unistd.h>

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {

    if (nrhs != 0) mexErrMsgTxt("incorrect number of arguments");
    if (nlhs >  1) mexErrMsgTxt("incorrect number of outputs");

    char hostName[512];

    if (gethostname(hostName, 512) != 0) hostName[0] = 0;

    plhs[0] = mxCreateString(hostName);

}
