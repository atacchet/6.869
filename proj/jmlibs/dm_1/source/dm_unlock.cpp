#include "mex.h"
#include <sys/unistd.h>

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {

    if (nrhs != 1) mexErrMsgTxt("incorrect number of arguments");
    if (nlhs != 0) mexErrMsgTxt("incorrect number of outputs");

    int fd = *(int64_T *)mxGetData(prhs[0]);

    close(fd);

}
