#include "mex.h"
#include <sys/unistd.h>
#include <fcntl.h>

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {

    if (nrhs != 1) mexErrMsgTxt("incorrect number of arguments");
    if (nlhs != 1) mexErrMsgTxt("incorrect number of outputs");

    char filePath[512];

    mxGetString(prhs[0], filePath, 512);

    int fd = open(filePath, O_WRONLY);
    if (fd == -1) mexErrMsgTxt("unable to open lock file");

    struct flock lock;
    lock.l_type   = F_WRLCK;
    lock.l_whence = SEEK_SET;
    lock.l_start  = 0;
    lock.l_len    = 0;

    if (fcntl(fd, F_SETLK, &lock) == -1) {
        close(fd);
        fd = -1;
    }

    if (fd == -1) {
        plhs[0] = mxCreateNumericMatrix(0, 0, mxINT64_CLASS, mxREAL);
    } else {
        plhs[0] = mxCreateNumericMatrix(1, 1, mxINT64_CLASS, mxREAL);
        *(int64_T *)mxGetData(plhs[0]) = fd;
    }

}
