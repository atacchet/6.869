#include "mex.h"
#include <sys/unistd.h>

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {

    if (nrhs != 2) mexErrMsgTxt("incorrect number of arguments");
    if (nlhs >  1) mexErrMsgTxt("incorrect number of outputs");

    char filePath[512];
    char linkPath[512];

    mxGetString(prhs[0], filePath, 512);
    mxGetString(prhs[1], linkPath, 512);

    bool ok = (link(filePath, linkPath) == 0);

    plhs[0] = mxCreateLogicalScalar(ok);

}
