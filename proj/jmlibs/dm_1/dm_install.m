function dm_install

clear functions;

Compile('priv', 'dm_hostname');
Compile('priv', 'dm_link');
Compile('priv', 'dm_lock');
Compile('priv', 'dm_pid');
Compile('priv', 'dm_unlock');

rehash path;

return;

%***********************************************************************************************************************

function Compile(outdn, func)

base = fileparts(mfilename('fullpath'));

outdp = fullfile(base, outdn);
outfp = fullfile(outdp, [func '.' mexext]);
srcfp = fullfile(base, 'source', [func '.cpp']);

if exist(outfp, 'file'), delete(outfp); end

mex('-outdir', outdp, srcfp);

return;