classdef dm_task_batch < dm_task

%-----------------------------------------------------------------------------------------------------------------------

properties (Transient, Hidden)
    dm_numItems  = [];
    dm_batchSize = [];
    dm_array     = [];
end

%-----------------------------------------------------------------------------------------------------------------------

methods (Abstract)
    T = DMStart(T, varargin)
end

%-----------------------------------------------------------------------------------------------------------------------

methods (Abstract)
    r = DMItem(T, itemNo, varargin)
end

%-----------------------------------------------------------------------------------------------------------------------

end