classdef hmax_input < hmax_base

% Defines the "input" cell type.  An "input" layer just holds an input image
% without computing anything.
%
% Note: use the hmax_input.Load method, not cns('set'), to load an image into
% an input layer.

methods (Static)

%-----------------------------------------------------------------------------------------------------------------------

function [m, p] = Construct(m, g, p)

% Called automatically by hmax.Model to add an "input" layer to the CNS model
% structure M.  User-specified parameters specific to "input" layers are as
% follows (where C = P.groups{G}):
%
%    C.size - a two element vector specifying the (y, x) size of the input
%    layer.  Note that an input layer is just a memory buffer.  It is not the
%    base of an image pyramid from which the simplest HMAX features are
%    computed.  An image pyramid is produced from an input layer via a
%    subsequent "scale" stage.  Thus, the size of an input layer does not
%    affect the rest of the HMAX computation.  Make the input layer big enough
%    to hold all, or most, of your images without resizing.  If the occasional
%    image is larger, that's ok: hmax_input.Load will downsize such images for
%    you on the CPU side before copying them into the input layer on the GPU.

z = numel(m.layers) + 1;

[m, p] = cns_super(m, g, p, z);
c = p.groups{g};

m.layers{z}.pzs = [];

if numel(c.size) == 2
    c.size = [1 c.size];
end

m.layers{z}.size{1} = c.size(1);
m = cns_mapdim(m, z, 2, 'pixels', c.size(2), [-0.5 0.5] * c.size(2)); % Common coords may change when images are loaded.
m = cns_mapdim(m, z, 3, 'pixels', c.size(3), [-0.5 0.5] * c.size(3)); % Common coords may change when images are loaded.

end

%-----------------------------------------------------------------------------------------------------------------------

function Load(m, g, im, varargin)

% hmax_input.Load(M, G, IM) loads an image into an input layer of an
% instantiated CNS model.
%
%    M - The CNS model which is currently instantiated on the GPU.
%
%    G - The number of the stage (group) that holds the input image.
%
%    IM - Either the path of an image or an array containing an image.

iz = m.groups{g}.zs;
bz = m.base_z;

bufSize = [m.layers{iz}.size{2 : 3}, prod(m.layers{iz}.size{1})];

n.size  = [m.layers{bz}.size{2 : 3}];
n.start = [m.layers{bz}.y_start, m.layers{bz}.x_start];
n.space = [1 1];

[p, val] = cns_prepimage(bufSize, n, im, varargin{:});

cns('set', {iz, 'val', shiftdim(val, -1)}, ...
    {iz, 'y_start', p.start(1)}, {iz, 'y_space', p.space(1)}, {iz, 'y_count', p.size(1)}, ...
    {iz, 'x_start', p.start(2)}, {iz, 'x_space', p.space(2)}, {iz, 'x_count', p.size(2)});

end

%-----------------------------------------------------------------------------------------------------------------------

function f = CNSFields

% Defines CNS fields specific to this cell type.

% Note: hmax_input.Load automatically sets both these fields for you when you load a new input image.

f.y_count = {'gp', 'int', 'dflt', cns_intmax}; % Actual height of the input image in pixels.
f.x_count = {'gp', 'int', 'dflt', cns_intmax}; % Actual width of the input image in pixels.

end

%-----------------------------------------------------------------------------------------------------------------------

end
end