function s = vars2struct(varargin)

args = varargin;

if (numel(args) >= 1) && isstruct(args{1})
    s = args{1};
    args = args(2 : end);
else
    s = struct;
end

if numel(args) < 1
    patterns = {'-upper'};
elseif (numel(args) == 1) && iscellstr(args{1})
    patterns = args{1};
elseif iscellstr(args)
    patterns = args;
else
    error('invalid arguments');
end

patterns(strcmp(patterns, '-upper')) = {'[A-Z]*'};
patterns = strcat('^', strrep(patterns, '*', '.*'), '$');

vars = {};
for i = 1 : numel(patterns)
    list = evalin('caller', ['whos(''-regexp'',''' patterns{i} ''')']);
    for j = 1 : numel(list)
        if ~ismember(list(j).name, vars) && ~list(j).global
            vars{end + 1} = list(j).name;
        end
    end
end
vars = sort(vars);

for i = 1 : numel(vars)
    s.(vars{i}) = evalin('caller', vars{i});
end

return;