function vload(varargin)

t = load(fullfile(varargin{:}));

names = fieldnames(t);
for i = 1 : numel(names)
    assignin('caller', names{i}, t.(names{i}));
end

return;