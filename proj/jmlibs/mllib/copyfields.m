function target = copyfields(target, source, names)

if ~isscalar(target) || ~isscalar(source), error 'only single structures allowed'; end

if nargin < 3
    names = fieldnames(source);
elseif ischar(names)
    fields = fieldnames(source);
    names = fields(~cellfun(@isempty, regexp(fields, names, 'once')));
end

for i = 1 : numel(names)
    if isfield(source, names{i})
        target.(names{i}) = source.(names{i});
    end
end

return;
