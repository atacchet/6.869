function [x, y, b, n] = gpoint(handles)

if nargin < 1, handles = gca; end

ca = gca;

while true

    [x, y, b] = ginput(1);
    h = gca;
    axes(ca);

    if numel(b) > 1, continue; end

    if b == 27
        x = [];
        y = [];
        b = [];
        n = [];
        break;
    end

    if (b < 1), continue; end
    if (b > 3), continue; end

    n = find(handles == h, 1);
    if isempty(n), continue; end

    xl = xlim(h);
    yl = ylim(h);
    if x < xl(1), continue; end
    if x > xl(2), continue; end
    if y < yl(1), continue; end
    if y > yl(2), continue; end

    break;

end

return;