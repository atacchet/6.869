function vdiff(a, b, varargin)

[ds, afs, as, bfs, bs] = parse({}, [], {}, [], {}, 'v', true, a, true, b);

display(ds, afs, as, bfs, bs, varargin{:});

return;

%***********************************************************************************************************************

function [ds, afs, as, bfs, bs] = parse(ds, afs, as, bfs, bs, d, af, a, bf, b)

if (af && bf && ~strcmp(class(a), class(b))) || ...
    (af && ~isstruct(a) && ~iscell(a)) || ...
    (bf && ~isstruct(b) && ~iscell(b)) || ...
    (iscell(a) && ~all(cellfun(@isstruct, a))) || ...
    (iscell(b) && ~all(cellfun(@isstruct, b))) || ...
    (ndims(a) > 2) || (min(size(a)) > 1) || ...
    (ndims(b) > 2) || (min(size(b)) > 1) || ...
    (isempty(a) && isempty(b))
    ds {end + 1} = d;
    afs(end + 1) = af;
    as {end + 1} = a;
    bfs(end + 1) = bf;
    bs {end + 1} = b;
    return;
end

if iscell(a)
    for i = 1 : max(numel(a), numel(b))
        d2 = sprintf('%s{%u}', d, i);
        af2 = af && (i <= numel(a));
        bf2 = bf && (i <= numel(b));
        if af2, a2 = a{i}; else a2 = []; end
        if bf2, b2 = b{i}; else b2 = []; end
    	[ds, afs, as, bfs, bs] = parse(ds, afs, as, bfs, bs, d2, af2, a2, bf2, b2);
    end
    return;
end

if max(numel(a), numel(b)) > 1
    for i = 1 : max(numel(a), numel(b))
        d2 = sprintf('%s(%u)', d, i);
        af2 = af && (i <= numel(a));
        bf2 = bf && (i <= numel(b));
        if af2, a2 = a(i); else a2 = []; end
        if bf2, b2 = b(i); else b2 = []; end
    	[ds, afs, as, bfs, bs] = parse(ds, afs, as, bfs, bs, d2, af2, a2, bf2, b2);
    end
    return;
end

if af, anames = fieldnames(a)'; else anames = {}; end
if bf, bnames = fieldnames(b)'; else bnames = {}; end
names = [anames, bnames(~ismember(bnames, anames))];

for i = 1 : numel(names)
    d2 = sprintf('%s.%s', d, names{i});
    af2 = af && isfield(a, names{i});
    bf2 = bf && isfield(b, names{i});
    if af2, a2 = a.(names{i}); else a2 = []; end
    if bf2, b2 = b.(names{i}); else b2 = []; end
    [ds, afs, as, bfs, bs] = parse(ds, afs, as, bfs, bs, d2, af2, a2, bf2, b2);
end

return;

%***********************************************************************************************************************

function display(ds, afs, as, bfs, bs, same)

if nargin < 6, same = false; end

dlen = min(max(max(cellfun(@numel, ds)), 1), inf);
maxcol = get(0, 'CommandWindowSize');
maxcol = floor((maxcol(1) - 12 - dlen) / 2);

ads = cellfun(@desc, num2cell(afs), as, 'UniformOutput', false);
bds = cellfun(@desc, num2cell(bfs), bs, 'UniformOutput', false);

alen = min(max(max(cellfun(@numel, ads)), 1), maxcol);
blen = min(max(max(cellfun(@numel, bds)), 1), maxcol);

for i = 1 : numel(ds)

    d  = ds {i};
    af = afs(i);
    a  = as {i};
    ad = ads{i};
    bf = bfs(i);
    b  = bs {i};
    bd = bds{i};

    if af && bf
        diff = ~isequalwithequalnans(a, b);
    else
        diff = true;
    end
    if diff
        diffdesc = '**';
    else
        diffdesc = '  ';
    end

    if diff || same
        fprintf('%s  %*s  %*s  %s  %*s\n', diffdesc, -dlen, d, -alen, ad, diffdesc, -blen, bd);
    end

end

return;

%***********************************************************************************************************************

function s = desc(af, a)

if ~af
    s = '';
    return;
end

disp  = true;
trans = false;

if ~disp
elseif (ndims(a) > 2) || (min(size(a)) > 1) || (numel(a) > 20)
    disp = false;
elseif isempty(a) && any(size(a) > 0)
    disp = false;
elseif size(a, 1) > 1
    a = a';
    trans = true;
end

if ~disp
elseif isnumeric(a) || islogical(a)
    s = mat2str(a);
elseif ischar(a)
    s = ['''', a, ''''];
elseif iscell(a)
    s = '{';
    for i = 1 : numel(a)
        if i > 1, s = [s ', ']; end
        s = [s, desc(true, a{i})];
    end
    s = [s '}'];
else
    disp = false;
end

if ~disp
    siz = size(a);
    s = sprintf('[%u%s %s]', siz(1), sprintf('x%u', siz(2 : end)), class(a));
end

if trans
    s = sprintf('%s''', s);
end

return;
