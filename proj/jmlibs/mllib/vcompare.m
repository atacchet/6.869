function vcompare(a, b, desc)

if nargin < 3, desc = 'a'; end

if isequalwithequalnans(a, b), return; end

if ~strcmp(class(a), class(b))
    fprintf('%s: different classes (%s vs %s)\n', desc, class(a), class(b));
    return;
end

if ~isequal(size(a), size(b))
    fprintf('%s: different sizes\n', desc);
    return;
end

if isstruct(a)

    names = fieldnames(a);

    diff = setxor(names, fieldnames(b));
    if ~isempty(diff)
        fprintf('%s: field name(s) (%s%s) not in both\n', desc, diff{1}, sprintf(', %s', diff{2 : end}));
        names = setdiff(names, diff);
    end

    for i = 1 : numel(a)
        for j = 1 : numel(names)
            vcompare(a(i).(names{j}), b(i).(names{j}), sprintf('%s(%u).%s', desc, i, names{j}));
        end
    end

elseif iscell(a)

    for i = 1 : numel(a)
        vcompare(a{i}, b{i}, sprintf('%s{%u}', desc, i));
    end

elseif isnumeric(a) || ischar(a)

    diff = (a(:) ~= b(:));
    if any(diff)
        fprintf('%s: %u position(s) different (%f%%), first=%u\n', desc, ...
            sum(diff), sum(diff) / numel(diff) * 100, find(diff, 1));
    end
    
else

    fprintf('%s: different\n', desc);

end

return;