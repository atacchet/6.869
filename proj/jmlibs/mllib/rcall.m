function varargout = rcall(func, varargin)

list = evalin('caller', 'whos(''-regexp'', ''[A-Z]*'')');
list = list(~[list.global]);
s = struct;
for i = 1 : numel(list)
    s.(list(i).name) = evalin('caller', list(i).name);
end

[varargout{1 : nargout}] = feval(func, s, varargin{:});

return;