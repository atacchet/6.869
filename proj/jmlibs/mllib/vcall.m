function varargout = vcall(func, varargin)

list = evalin('caller', 'whos(''-regexp'', ''[A-Z]*'')');
list = list(~[list.global]);
s = struct;
for i = 1 : numel(list)
    s.(list(i).name) = evalin('caller', list(i).name);
end

[t, varargout{1 : nargout}] = feval(func, s, varargin{:});

names = fieldnames(t);
for i = 1 : numel(names)
    assignin('caller', names{i}, t.(names{i}));
end

return;