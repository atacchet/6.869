function varargout = ndsumm(a, varargin)

if isnumeric(a)
    d = ParseDef(varargin{1});
    p = ParseQuery(d, varargin{2 : end});
    [varargout{1 : nargout}] = Query(a, d, p);
elseif ischar(a) && strcmp(a, 'qdesc')
    d = ParseDef(varargin{1});
    p = ParseQuery(d, varargin{2 : end});
    [varargout{1 : nargout}] = QDesc(d, p, false);
elseif ischar(a) && strcmp(a, 'qdescfull')
    d = ParseDef(varargin{1});
    p = ParseQuery(d, varargin{2 : end});
    [varargout{1 : nargout}] = QDesc(d, p, true);
elseif ischar(a) && strcmp(a, 'empty')
    [varargout{1 : nargout}] = Empty(varargin{:});
elseif ischar(a) && strcmp(a, 'consts')
    d = ParseDef(varargin{1});
    [varargout{1 : nargout}] = Consts(d, varargin{2 : end});
elseif ischar(a) && strcmp(a, 'names')
    d = ParseDef(varargin{1});
    [varargout{1 : nargout}] = Names(d, varargin{2 : end});
else
    error('invalid command');
end

return;

%***********************************************************************************************************************

function def = ParseDef(arg)

if isstruct(arg)
    def = arg;
elseif iscellstr(arg)
    def = Empty(numel(arg));
    [def.name] = deal(arg{:});
elseif isnumeric(arg) && isscalar(arg)
    def = Empty(arg);
else
    error('invalid definition');
end

for i = 1 : numel(def)
    if ischar(def(i).name  ), def(i).name   = {def(i).name  }; end
    if ischar(def(i).format), def(i).format = {def(i).format}; end
end

return;

%***********************************************************************************************************************

function p = ParseQuery(def, varargin)

p = struct;

if mod(numel(varargin), 2) == 0
    args = varargin;
    p.perm = [0];
else
    args = varargin(1 : end - 1);
    p.perm = varargin{end};
end

if iscellstr(p.perm)
    if ~isempty(p.perm) && strcmp(p.perm{end}, '*')
        p.perm = [FindDim(def, p.perm(1 : end - 1)), 0];
    else
        p.perm = FindDim(def, p.perm);
    end
end

p.left = true(1, numel(def));
p.inds = repmat({':'}, 1, numel(def));
p.ops  = {};
p.dims = [];

for i = 1 : 2 : numel(args)

    if iscell(args{i}) || iscell(args{i + 1}) || (isnumeric(args{i}) && ~isscalar(args{i}))
        if isnumeric(args{i    }), dd = num2cell(args{i    }); else dd = args{i    }; end
        if isnumeric(args{i + 1}), nn = num2cell(args{i + 1}); else nn = args{i + 1}; end
    else
        dd = args(i);
        nn = args(i + 1);
    end

    for j = 1 : numel(dd)

        d = dd{j};
        if isnumeric(d) && isscalar(d)
        elseif ischar(d)
            d = FindDim(def, d);
        else
            error('invalid dimension');
        end
        if ~p.left(d), error('repeated dimension'); end

        if def(d).single
            n = 1;
        else
            n = nn{j};
        end
        if isnumeric(n) && isscalar(n)
            p.left(d) = false;
            p.inds{d} = n;
        elseif isa(n, 'function_handle')
            p.left(d) = false;
            p.ops {end + 1} = n;
            p.dims(end + 1) = d;
        else
            error('invalid index or operation');
        end

    end

end

if numel(unique(p.perm)) ~= numel(p.perm), error('repeated dimension'); end
if ~isempty(p.perm) && (p.perm(end) == 0)
    p.perm = p.perm(1 : end - 1);
    if ~all(p.left(p.perm)), error('invalid result dimensions'); end
else
    if ~isempty(setxor(p.perm, find(p.left))), error('invalid result dimensions'); end
end

return;

%***********************************************************************************************************************

function d = FindDim(def, names)

if ischar(names), names = {names}; end

d = zeros(1, numel(names));

for i = 1 : numel(names)

    for j = 1 : numel(def)
        if ismember(upper(names{i}), upper(def(j).name))
            d(i) = j;
            break;
        end
    end

    if d(i) == 0, error('invalid dimension name'); end

end

return;

%***********************************************************************************************************************

function [s, sdef] = Query(a, def, p)

s = a(p.inds{:});

for i = 1 : numel(p.ops)
    s = EvalOp(p.ops{i}, s, p.dims(i));
end

% if isempty(p.perm)
% 
%     siz = size(s);
%     siz(end + 1 : numel(def)) = 1;
%     siz = siz(p.left);
%     siz(end + 1 : 2) = 1;
% 
%     s = reshape(s, siz);
% 
%     sdef = def(p.left);
% 
% else

    rest = p.left;
    rest(p.perm) = false;
    rest = find(rest);

    s = permute(s, [p.perm, rest, find(~p.left)]);

    sdef = def([p.perm, rest]);

% end

return;

%***********************************************************************************************************************

function s = EvalOp(op, s, d)

switch func2str(op)
case 'min', s = op(s, [], d);
case 'max', s = op(s, [], d); 
case 'std', s = op(s, 0, d); 
case 'var', s = op(s, 0, d); 
otherwise , s = op(s, d); % Works for median, mode, prod, sum, any, all.
end

return;

%***********************************************************************************************************************

function desc = QDesc(def, p, full)

desc = '';

for i = 1 : numel(def)
    if ~def(i).single && isnumeric(p.inds{i})
        desc = AddToDesc(desc, def, i, p.inds{i});
    end
end

if full
    for i = 1 : numel(def)
        if def(i).single && isnumeric(p.inds{i})
            desc = AddToDesc(desc, def, i, p.inds{i});
        end
    end
end

return;

%***********************************************************************************************************************

function desc = AddToDesc(desc, def, d, v)

if ~isempty(desc), desc = [desc ', ']; end

for i = 1 : numel(def(d).name)
    if i > 1, desc = [desc '/']; end
    desc = [desc, def(d).name{i}];
end
desc = [desc '='];

if ~isempty(def(d).vals), v = def(d).vals(:, v)'; end
v = v .* def(d).scale;

for i = 1 : numel(def(d).name)
    if i > 1, desc = [desc '/']; end
    if isempty(def(d).format{i})
        desc = [desc, num2str(v(i))];
    else
        desc = [desc, sprintf(def(d).format{i}, v(i))];
    end
end

return;

%***********************************************************************************************************************

function def = Empty(num)

if nargin < 1, num = 0; end

def = struct('name', {{''}}, 'vals', {[]}, 'scale', {1}, 'format', {{''}}, 'single', {false});

if num == 0
    def = repmat(def, 0, 0);
else
    def = repmat(def, 1, num);
end

return;

%***********************************************************************************************************************

function c = Consts(def, prefix)

if nargin < 2, prefix = 'D'; end

c = struct;

for i = 1 : numel(def)
    for j = 1 : numel(def(i).name)
        c.(upper([prefix, def(i).name{j}])) = i;
    end
end

return;

%***********************************************************************************************************************

function names = Names(def, d)

if nargin < 2, d = 1 : numel(def); end

names = cell(1, numel(d));

for i = 1 : numel(d)
    names{i} = def(d(i)).name{1};
end

return;