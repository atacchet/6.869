function subplotvar(numPlots, aspectRatio, plotNo)

if plotNo > numPlots, error('invalid plot number'); end

if aspectRatio < 1
    spi = ceil(sqrt(numPlots / aspectRatio));
    spj = ceil(numPlots / spi);
    while (spi - 1) * spj >= numPlots, spi = spi - 1; end
else
    spj = ceil(sqrt(numPlots / aspectRatio));
    spi = ceil(numPlots / spj);
    while (spj - 1) * spi >= numPlots, spj = spj - 1; end
end

subplot(spi, spj, plotNo);

return;