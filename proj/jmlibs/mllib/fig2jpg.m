function path = fig2jpg(varargin)

if nargin == 1
    h    = gcf;
    path = varargin{1};
elseif nargin == 2
    h    = varargin{1};
    path = varargin{2};
else
    error('incorrect number of arguments');
end

path = [path '.jpg'];

children = findobj(h, 'Type', 'axes');
for i = 1 : numel(children)
    hold(children(i), 'off');
end

print(h, '-djpeg90', '-r300', path);

return;