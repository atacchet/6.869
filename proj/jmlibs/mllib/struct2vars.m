function struct2vars(s, varargin)

if nargin < 2
    patterns = {'*'};
elseif (nargin == 2) && iscellstr(varargin{1})
    patterns = varargin{1};
elseif iscellstr(varargin)
    patterns = varargin;
else
    error('invalid arguments');
end

patterns(strcmp(patterns, '-upper')) = {'[A-Z]*'};
patterns = strcat('^', strrep(patterns, '*', '.*'), '$');

names = fieldnames(s);

for i = 1 : numel(names)
    match = regexp(names{i}, patterns, 'once');
    if ~isempty([match{:}])
        assignin('caller', names{i}, s.(names{i}));
    end
end

return;