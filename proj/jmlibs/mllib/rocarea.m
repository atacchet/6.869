function [area, fp, tp] = rocarea(cats, vals)

thrs = [inf, sort(unique(vals(:)'), 'descend'), -inf];
vneg = vals(cats ~= 1);
vpos = vals(cats == 1);

fp = zeros(1, numel(thrs));
tp = zeros(1, numel(thrs));

for i = 1 : numel(thrs)
    fp(i) = mean(vneg >= thrs(i));
    tp(i) = mean(vpos >= thrs(i));
end

dxs = diff(fp);
ys  = (tp(1 : end - 1) + tp(2 : end)) / 2;

area = sum(dxs .* ys);

return;