function paths = findfun(base)

if isempty(base), error('invalid arguments'); end
if base(end) ~= filesep
    base(end + 1) = filesep;
end

list = inmem('-completenames');

paths = list(strmatch(base, list));

return;