classdef hmaxjob_libchoose < hmaxjob_lib & dm_task_single

%-----------------------------------------------------------------------------------------------------------------------

properties (Constant, GetAccess = private)
    dm_args = {'hmaxjob_params', 'hmaxjob_lib'};
end

%-----------------------------------------------------------------------------------------------------------------------

properties (Transient, Hidden)
    gs;
    ns;
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function T = DMBuild(T, P, L)

T.lib = L.lib;

m = cns_call(P.p, 0, 'Model', T.lib);

for g = 1 : numel(P.p.groups)

    i = find(T.gs == g, 1);
    if isempty(i)
        rCount = 0;
    else
        rCount = T.ns(i);
    end

    if (rCount == 0) || isinf(rCount)

        ind = rCount;

    else

        numUnits = 0;
        for z = m.groups{g}.zs
            numUnits = numUnits + prod([m.layers{z}.size{:}]);
        end

        ind = randperm(numUnits);
        ind = sort(ind(1 : rCount));

    end

    T.lib.groups{g}.rIndexes = ind;

end

end
end

%-----------------------------------------------------------------------------------------------------------------------

end