classdef hmaxjob_v

%-----------------------------------------------------------------------------------------------------------------------

properties (Constant, GetAccess = private)
    dm_args = {'hmaxjob_params', 'hmaxjob_lib'};
end

%-----------------------------------------------------------------------------------------------------------------------

properties
    cats;
    paths;
    wins = [];
end

properties (Transient, Hidden)
    read = '';
    g;
    m;
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function T = DMStart(T, varargin)

T.dm_numItems = numel(T.cats);

if isempty(T.dm_batchSize), T.dm_batchSize = 200; end

if isempty(T.read), T.read = 'hmaxjob_readimage'; end

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function T = DMOpen(T, P, L)

T.m = cns_call(P.p, 0, 'Model', L.lib);

cns('init', T.m, 'gpu');

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function r = DMItem(T, itemNo, P, L)

if isempty(T.wins)
    im = feval(T.read, T.paths{itemNo});
else
    im = feval(T.read, T.paths{itemNo}, T.wins(:, itemNo));
end

cns_call(T.m, 0, 'LoadImage', im);
cns('run');

r = hmaxjob_resp(T.m, L.lib, T.g);

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function T = DMClose(T)

cns('done');

T.m = [];

end
end

%-----------------------------------------------------------------------------------------------------------------------

end