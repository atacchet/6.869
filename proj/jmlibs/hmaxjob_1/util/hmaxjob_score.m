function s = hmaxjob_score(jname)

if dm_exist(fullfile(jname, 'error'))
    s = 0;
    return;
end

p = dm_read(fullfile(jname, 'pred'));

s = zeros(1, max(p.cats));
for i = 1 : max(p.cats)
    s(i) = mean(p.pred(p.cats == i) == i);
end

s = mean(s);

return;