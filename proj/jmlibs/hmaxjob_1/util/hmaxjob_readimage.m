function [im, mask] = hmaxjob_readimage(im, win)

if nargin < 2, win = []; end

if ischar(im)
    im = imread(gl_abspath(im));
end

if ~isempty(win)
    x1 = max(round(win(1)), 1);
    x2 = min(round(win(2)), size(im, 2));
    y1 = max(round(win(3)), 1);
    y2 = min(round(win(4)), size(im, 1));
    im = im(y1 : y2, x1 : x2, :);
end

mask = [];

return;