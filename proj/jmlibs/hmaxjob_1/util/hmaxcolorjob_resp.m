function r = hmaxcolorjob_resp(m, lib, randIndSub, gs)

if (nargin < 4) || isempty(gs), gs  = 1 : numel(lib.groups); end

r = single([]);

for g = gs

    ind = lib.groups{g}.rIndexes(:);
    if isequal(ind, 0), continue; end

    rzs = cns('get', -g, 'val');
    for i = 1 : numel(rzs)
        rzs{i} = rzs{i}(:);
    end
      
    rg = cat(1, rzs{:});
    
    if g==7
        rg = reshape(rg, 1024, 2)';
        rg  = rg(:);
    end
    
    if ~isequal(ind, inf), rg = rg(ind); end

    r = [r; rg];

end

if ~isempty(randIndSub) || numel(randIndSub) ~=0
    r = r(randIndSub);
end

return;