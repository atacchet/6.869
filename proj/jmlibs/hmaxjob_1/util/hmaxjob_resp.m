function r = hmaxjob_resp(m, lib, gs)

if (nargin < 3) || isempty(gs), gs  = 1 : numel(lib.groups); end

r = single([]);

for g = gs

    ind = lib.groups{g}.rIndexes(:);
    if isequal(ind, 0), continue; end

    rzs = cns('get', -g, 'val');
    for i = 1 : numel(rzs)
        rzs{i} = rzs{i}(:);
    end
    rg = cat(1, rzs{:});

    if ~isequal(ind, inf), rg = rg(ind); end

    r = [r; rg];

end

return;