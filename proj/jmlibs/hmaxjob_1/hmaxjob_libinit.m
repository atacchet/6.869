classdef hmaxjob_libinit < hmaxjob_lib & dm_task_single

%-----------------------------------------------------------------------------------------------------------------------

properties (Constant, GetAccess = private)
    dm_args = {'hmaxjob_params'};
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function T = DMBuild(T, P)

T.lib = struct;
T.lib.groups = repmat({struct}, 1, numel(P.p.groups));

end
end

%-----------------------------------------------------------------------------------------------------------------------

end