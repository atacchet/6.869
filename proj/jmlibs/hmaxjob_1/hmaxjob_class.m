classdef hmaxjob_class < dm_task_batch

%-----------------------------------------------------------------------------------------------------------------------

properties (Constant, GetAccess = private)
    dm_args = {'hmaxjob_collect'};
end

%-----------------------------------------------------------------------------------------------------------------------

properties
    class;
end

properties (Transient, Hidden)
    args;
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function T = DMStart(T, X)

T.args.x       = X.v;
T.args.y       = X.cats;
T.args.numCats = max(X.cats);
T.args.lib     = 'rls';
T.args.printf  = @dm_printf;

T.args = linclass('start', T.args);

T.dm_numItems  = T.args.count;
T.dm_batchSize = T.args.batchSize;

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function r = DMItem(T, itemNo, varargin)

r = linclass('item', T.args, itemNo);

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function T = DMCombine(T, r, varargin)

T.class = linclass('combine', T.args, r);

end
end

%-----------------------------------------------------------------------------------------------------------------------

end