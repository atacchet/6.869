classdef hmaxjob_libsample < hmaxjob_lib & dm_task_batch

%-----------------------------------------------------------------------------------------------------------------------

properties (Constant, GetAccess = private)
    dm_args      = {'hmaxjob_params', 'hmaxjob_lib'};
    dm_dim_fVals = 3;
end

%-----------------------------------------------------------------------------------------------------------------------

properties (Transient, Hidden)
    cats;
    paths;
    wins = [];
    read = '';
    g;
    numSamples;
    k = [];
    catCounts;
    m;
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function T = DMStart(T, P, varargin)

nc = max(T.cats);

perCat = floor(T.numSamples / nc);
extra = randperm(nc);
extra = extra(1 : T.numSamples - perCat * nc);
T.catCounts = repmat(perCat, 1, nc);
T.catCounts(extra) = T.catCounts(extra) + 1;

T.dm_numItems  = nc;
T.dm_batchSize = max(floor(nc / 50), 1); % Want around 50 batches.  No more than 100.

if isempty(T.read), T.read = 'hmaxjob_readimage'; end

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function T = DMOpen(T, P, L)

T.m = cns_call(P.p, 0, 'Model', L.lib);

cns('init', T.m, 'gpu');

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function r = DMItem(T, itemNo, P, varargin)

needed = T.catCounts(itemNo);
inds = find(T.cats == itemNo);
if isempty(inds), error('no images for category %u', itemNo); end
inds = inds(randperm(numel(inds)));
total = numel(inds);

r = cns_call(T.m, -T.g, 'EmptyDict');

while needed > 0

    if isempty(T.wins)
        [im, mask] = feval(T.read, T.paths{inds(1)});
    else
        [im, mask] = feval(T.read, T.paths{inds(1)}, T.wins(:, inds(1)));
    end
    if ~isempty(mask)
        mask = (cns_resizeimage(double(mask), P.p.groups{P.g.si}.baseSize) >= 0.1);
    end
    extra.mask = mask;
    extra.rad  = 0.05; % TODO: make a parameter

    cns_call(T.m, 0, 'LoadImage', im);
    cns('run');

    try
        r = cns_call(T.m, -T.g, 'SampleFeatures', r, 1, extra);
        needed = needed - 1;
        inds = [inds(2 : end), inds(1)];
    catch
        dm_printf('unable to sample from "%s"\n', T.paths{inds(1)});
        inds = inds(2 : end);
        if numel(inds) <= 0.75 * total
            error('unable to sample features for category %u', itemNo);
        end
    end

end

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function T = DMClose(T)

cns('done');

T.m = [];

end
end

%-----------------------------------------------------------------------------------------------------------------------

methods
function T = DMCombine(T, r, P, L)

if ~isempty(T.k)

    if any(diff(r.fSizes)), error('kmeans requires a single feature size'); end

    siz = size(r.fVals);
    siz(end + 1 : 4) = 1;
    v = double(reshape(r.fVals, [prod(siz(1 : 3)), siz(4)])');
    [ans, v] = kmeans(v, T.k, 'Display', 'iter');
    r.fVals = reshape(single(v)', [siz(1 : 3), T.k]);

    r.fSizes = r.fSizes(1 : T.k);
    r.fSPos  = repmat(1  , 1, T.k);
    r.fYPos  = repmat(0.5, 1, T.k);
    r.fXPos  = repmat(0.5, 1, T.k);

end

r = cns_call(P.p, -T.g, 'SortFeatures', r);

if cns_istype(P.p, -T.g, 'ss')
    r = cns_call(P.p, -T.g, 'SparsifyDict', r);
end

T.lib = L.lib;
T.lib.groups{T.g} = r;

end
end

%-----------------------------------------------------------------------------------------------------------------------

end