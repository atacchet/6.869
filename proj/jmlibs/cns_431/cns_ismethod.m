function r = cns_ismethod(m, n, method)

% CNS_ISMETHOD
%    Click <a href="matlab: cns_help('cns_ismethod')">here</a> for help.

%***********************************************************************************************************************

% Copyright (C) 2011 by Jim Mutch (www.jimmutch.com).
%
% This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
% License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later
% version.
%
% This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
% warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License along with this program.  If not, see
% <http://www.gnu.org/licenses/>.

%***********************************************************************************************************************

if cns_isstruct(m)

    package = m.package;

    if n == 0
        type = '';
    elseif n > 0
        type = m.layers{n}.type;
    else
        type = cns_grouptype(m, -n);
    end

elseif ischar(m)

    package = m;
    type    = n;

else

    error('invalid model or package name');

end

if isempty(type)
    cname = package;
else
    cname = [package '_' type];
end

if isempty(which(cname)), error('"%s.m" not found', cname); end

r = ismethod(cname, method);

return;