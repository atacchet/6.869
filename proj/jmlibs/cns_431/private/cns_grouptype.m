function type = cns_grouptype(m, g)

if isfield(m.groups{g}, 'type')
    type = m.groups{g}.type;
    return;
end

if isfield(m, 'layers')
    numLayers = numel(m.layers);
else
    numLayers = 0;
end
found = false;
for z = 1 : numLayers
    if isfield(m.layers{z}, 'groupNo') && (m.layers{z}.groupNo == g)
        type  = m.layers{z}.type;
        found = true;
        break;
    end
end
if ~found, error('invalid group number'); end

return;