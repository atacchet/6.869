
addpath(genpath([pwd '/src']))
params = sr_initialize;

% Load test images
test_images = sr_get_test_images(params);
ids_to_process = [1:3 5 801:806 864:869 886:890 1291:1296 1700:1705 1769:1776 1827:1828];

% fprintf('\n\n-- Computing up-res\n')
% Loop through test images and do super-res
counter = 0;
it = 50; %number of iterations for bp
for im_ind = ids_to_process;
    counter = counter + 1;
    [path,filename, ~] = fileparts(test_images{im_ind});
    [~,classname,~] = fileparts(path);

    fprintf('-- Loading image struct %s\n',[classname '_' filename])
    structPath = fullfile(params.path.imStruct,[classname '_' filename '_struct.mat']);
    im_struct = load(structPath);
    
    filename = fullfile(params.path.output,im_struct.im_name,im_struct.im_name);

    
    % load images
    clear prob
    gt      = imread([filename '_groundtruth_HR.jpg']);
    lr      = imread([filename '_lowres.jpg']);
    prob{1} = imread(sprintf('%s_prob_%03d_bp_%03d_highres.jpg',filename,0,it));
    prob{2} = imread(sprintf('%s_prob_%03d_bp_%03d_highres.jpg',filename,10,it));
    
    % % to gray channel
    gt = im2double(gt);
    lr = im2double(lr);
    prob{1} = im2double(prob{1});
    prob{2} = im2double(prob{2});
    [gt,~] = decomposeImage(gt);
    [lr,~] = decomposeImage(lr);
    [prob{1},~] = decomposeImage(prob{1});
    [prob{2},~] = decomposeImage(prob{2});
    
    % Alternative to gray
    % gt = gt(:,:,1);
    % lr = lr(:,:,1);
    % prob{1} = prob{1}(:,:,1);
    % prob{2} = prob{2}(:,:,1);

    % PSNR
    % PSNR(gt,lr);
    % PSNR(gt,prob{1});
    % PSNR(gt,prob{2});


    % SSIM
    % gt = uint8(gt*255);
    % prob{1} = uint8(prob{1}*255);
    % prob{2} = uint8(prob{2}*255);
    % lr = uint8(lr*255);
    % ssim(gt,lr)
    % ssim(gt,prob{1})
    % ssim(gt,prob{2})
    
    scores(counter,:) = [PSNR(gt,lr) PSNR(gt,prob{1}) PSNR(gt,prob{2}) ...
        ssim(gt,lr) ssim(gt,prob{1}) ssim(gt,prob{2})];
end
