function [] = admMRFRetrieveRun(jobFileName)
    fprintf('-- Loading job file %s',jobFileName)
    jf = load(jobFileName);
    fprintf(' - done\n')

    fprintf('-- Loading dictionary %s', jf.jobStruct.DictPath)
    tic;
    dictionary = load(jf.jobStruct.DictPath);
    t = toc;
    fprintf(' - done in %fs\n\n',t)

    fprintf('Starting job hunt :\n')

    imInfo = load(jf.jobStruct.imInfoPath);
    fLock = admSetup(jf.jobStruct.stateFileName);

    blockID = NO_WORK;

    while (blockID ~= JOB_DONE)
        blockID = getWork(jf.jobStruct.stateFileName, fLock);
        if ~exist(jf.jobStruct.stateFileName,'file')
            fprintf('Unable to read "%s"\n', jf.jobStruct.stateFileName);
            blockID = JOB_DONE;
        end	
        switch blockID
            case JOB_DONE
                fprintf('-- Job Completed!\r');
            case NO_WORK
                fprintf('-- Node out of work - waiting 10s  \r');
                pause(10);
            case FINALIZE_JOB
                fprintf('-- Finalizing Job\r');
                doFinal(jf, jobFileName,imInfo);
                reportWork(jf.jobStruct.stateFileName, fLock, blockID);
            otherwise
                fprintf('-- Taking care of job: %d\r', blockID);
                tic;
                out = doWork(dictionary,jf,double(blockID));
                save([jf.jobStruct.stateFileName '_' num2str(blockID)], '-struct','out','-v7.3');
                reportWork(jf.jobStruct.stateFileName, fLock, blockID);
                t=toc;
        end
    end

    admDismiss(fLock);
    if exist(jf.jobStruct.stateFileName,'file')
        delete(jf.jobStruct.stateFileName);
    end	
end		

function out = doWork(dictionary, jf, blockID)
    im_struct = load(jf.jobStruct.imStructPath{blockID});

    out.imgID = im_struct.imgID;
    rowID = jf.jobStruct.rowID(blockID);
    out.rowID = rowID;
    for j=1:im_struct.patch_w
        % use the patch and mask as the key to search the training data
        fprintf('Taking care of job: %d', blockID);
        fprintf(' - %06.2f%%',100*j/im_struct.patch_w)
        if im_struct.doPCA
            patchPCA = (im_struct.patches(:,rowID,j)'-dictionary.lowres_mean)*dictionary.V;
            [idx,distance] = ...
                vl_kdtreequery(dictionary.kd_tree,dictionary.lowresPCA',patchPCA','NumNeighbors',im_struct.NN*4);
            [~, index] = sort(distance,'ascend');
            idx = idx(index(1:im_struct.NN));
        else
            [idx,distance] = ...
                vl_kdtreequery(dictionary.kd_tree,dictionary.lowres',im_struct.patches(:,rowID,j),'NumNeighbors',im_struct.NN);
        end
        idx = idx(end:-1:1); % sort wrt distance
        out.candidates(j).idx               = idx;
        out.candidates(j).patches           = dictionary.highres(idx,:);
        if im_struct.doContrastNormalize
            out.candidates(j).patches       = out.candidates(j).patches*im_struct.scale(rowID,j);
        end
        fprintf('\r')
    end
    fprintf('                                 \r')
end

function doFinal(jf, jobFileName, imInfo)
    nextBlock = 0;
    for i = 1:imInfo.NumImages
        im_struct = load(jf.jobStruct.imStructPath{i});
        for j = 1:imInfo.image(i).patches_h
            nextBlock = nextBlock + 1;
            bName = sprintf('%s_%d.mat', jf.jobStruct.stateFileName, nextBlock);
            t = load(bName);
            delete(bName);
            if (t.imgID ~= i) fprintf('<!> WRONG IMAGE ID <!>\n'); end
            if(t.rowID ~= j) fprintf('<!> WRONG ROW ID<!>\n'); end
            im_struct.candidates(j,:) = t.candidates;
        end
        save(imInfo.reportWorkFileName{i}, '-struct','im_struct','-v7.3');
    end
end
