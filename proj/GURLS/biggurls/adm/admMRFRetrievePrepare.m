function [] = admMRFRetrievePrepare(opt, jobFileName)
    fprintf('-- Preparing job submission for %02d images',opt.NumImages)

    % Wrap job structure
    jobStruct                   = struct;
    jobStruct.DictPath          = opt.DictPath;
    jobStruct.imInfoPath        = opt.imInfoPath;
    jobStruct.imStructPath      = opt.imStructPath;
    jobStruct.rowID             = opt.rowID;
    jobStruct.stateFileName     = [pwd() '/output/' datestr(now,30)];

    f = fopen(jobStruct.stateFileName, 'w');

    totRows = 0;
    for i = 1:opt.NumImages
        totRows = totRows + opt.imRows(i);
    end
    for s = 1:totRows
        fprintf(f,'%d\t%d\n',s,0);
    end
    fprintf(f,'%d\t%d\n',-1,0);
    fclose(f);
    save(jobFileName,'jobStruct');
    pause(5);

    fprintf(1,' - done\n')
