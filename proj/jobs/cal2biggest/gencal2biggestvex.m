function gencal10bestvex(trloc, teloc, scaleFactor)

	d = vex_load('/cbcl/scratch01/datasets/cal101/vex.mat');
	t = load('/cbcl/cbcl01/atacchet/gnips/ctech/ctechlinbatch.mat');
	nclass = 3;
	[~,bc] = sort(t.opt.perf.acc(:));
	bc = bc(end-nclass+1:end)

	ids = find(ismember(d.catID, bc));

	o = d.Copy(ids);
	cn = d.catNames;
	cn = {cn{bc}};
	o.UpdateProp('catNames',cn);
	cid = o.catID;
	for k = 1:nclass
		cid(cid == bc(k)) = k;
	end
	o.UpdateField('catID',cid);

	mc = o.NumItems;
	for k = 1:nclass
		if sum(o.catID == k) < mc
			mc = sum(o.catID == k);
		end
	end
	tr = [];
	te = [];
	for k = 1:nclass
		ids = find(o.catID == k);
		idtr = randsample(ids,mc-1);
		tr = [tr idtr];
		te = [te setdiff(ids, idtr)];
	end
	o.AddProp('codes',2*eye(nclass) -1);
	o.AddForm('dsLabels',@(x) {x.codes(x.catID,:)});
	o.AddProp('scaleFactor', scaleFactor);
	o.AddForm('lowres', @(x,i) {imresize(x.image{i}, x.scaleFactor)});
	o.AddForm('imlow', @(x,i) {vex_imband(x.image{i}, 1)});
	o.AddForm('imband', @(x,i) {vex_imband(x.image{i}, 2)});
	o.AddForm('imhigh', @(x,i) {vex_imband(x.image{i}, 3)});
	o.UpdateForm('image', @(x,i){im2double(vex_imread(x.imPath{i}))})
	o.UpdateForm('item',@(x)x.lowres);
	trvex = o.Copy(tr);
	tevex = o.Copy(te);

	vex_save(trloc, trvex);
	vex_save(teloc, tevex);


