function d = updatevex(d)
		d.AddForm('imlow',@(x,i) {vex_imband(x.image{i}, 1)}); 
		d.AddForm('imband',@(x,i) {vex_imband(x.image{i}, 2)}); 
		d.AddForm('imhigh',@(x,i) {vex_imband(x.image{i}, 3)}); 
		d.UpdateForm('image', @(x,i){im2double(vex_imread(x.imPath{i}))})

