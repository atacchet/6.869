function cal10best_train(jobname)
		xxtr = bigarray_mat(fullfile(workdir, jobname, 'big', 'trainX'));
		yytr = bigarray_mat(fullfile(workdir, jobname, 'big', 'trainY'));
		xxte = bigarray_mat(fullfile(workdir, jobname, 'big', 'testX'));
		yyte = bigarray_mat(fullfile(workdir, jobname, 'big', 'testY'));

		xxtr.Transpose(true);
		yytr.Transpose(true);
		xxte.Transpose(true);
		yyte.Transpose(true);

		Xtr = xxtr(1:xxtr.NumItems,:);
		ytr = yytr(1:yytr.NumItems,:)';

		Xte = xxte(1:xxte.NumItems,:);
		yte = yyte(1:yyte.NumItems,:)';


		opt = defopt(jobname);
		opt.seq = {'paramsel:siglam','kernel:rbf','rls:dual','predkernel:traintest','pred:dual','perf:macroavg'};
		opt.process{1} = [2,2,2,0,0,0];
		opt.process{2} = [3,3,3,2,2,2];

		opt.hoperf = @perf_macroavg;
		opt.nlambda = 100;

		gurls(Xtr, ytr, opt, 1);
		gurls(Xte, yte, opt, 2);

