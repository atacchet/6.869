function tasks = neo2_dmparams_v3(jobname, trvexloc, tevexloc)

[s.p, s.g] = hmax_ctech;
fCount = 4096;
s.drls_workdir = fullfile(workdir, jobname);

p = s.p;
layers = s.g;
g = cns_groupno(p);

if ~isfield(s,'fraction'),  s.fraction  = 1.0;  end
if ~isfield(s,'batchSize'), s.batchSize = 1000; end

dSample = vex_load(fullfile(workdir, jobname, trvexloc));
dTrain	= vex_load(fullfile(workdir, jobname, trvexloc));
dTest 	= vex_load(fullfile(workdir, jobname, tevexloc));

%-----------------------------------------------------------------------------------------------------------------------

tasks = {};

t = struct;
t.dm_name  = 'start';
t.dm_class = 'hmaxdm_start';
t.dm_temp  = false;
t.p        = p;
t.g        = g;
t.catNames = dTrain.catNames;
tasks{end + 1} = t;

t = struct;
t.dm_name    = 'sample';
t.dm_class   = 'hmaxdm_sample';
t.dm_depends = {'start'};
t.dm_temp    = false;
t.d          = dSample;
t.sg         = g.s2;
t.numSamples = fCount;
tasks{end + 1} = t;

t = struct;
t.dm_name    = 'choose';
t.dm_class   = 'hmaxdm_choose';
t.dm_depends = {'sample'};
t.dm_temp    = false;
t.gs         = layers;
t.ns         = inf(1,numel(layers));
tasks{end + 1} = t;

t = struct;
t.dm_name    = 'trainX';
t.dm_class   = 'hmaxdm_collect_big';
t.dm_depends = {'choose'};
t.dm_temp    = false;
t.d          = dTrain;
t.gs         = layers;
t.dm_batchSize = s.batchSize;
tasks{end + 1} = t;

t = struct;
t.dm_name    = 'trainY';
t.dm_class   = 'neo2dm_labels_big';
t.dm_depends = {'trainX'};
t.dm_temp    = false;
t.filename   = fullfile(s.drls_workdir,'/big/trainY');
tasks{end + 1} = t;


t = struct;
t.dm_name    = 'model';
t.dm_class   = 'neo2dm_model_nodep';
t.dm_depends = {'choose'}
t.choosefilename = fullfile(workdir, jobname, 'choose.mat');
t.jobname     = jobname;
tasks{end+1} = t;


t = struct;
t.dm_name    = 'testX';
t.dm_class   = 'hmaxdm_collect_big';
t.dm_depends = {'model'};
t.dm_temp    = false;
t.d          = dTest;
t.gs         = layers;
t.dm_batchSize = s.batchSize;
tasks{end + 1} = t;

t = struct;
t.dm_name    = 'testY';
t.dm_class   = 'neo2dm_labels_big';
t.dm_depends = {'testX'};
t.dm_temp    = false;
t.filename   = fullfile(s.drls_workdir,'/big/testY');
tasks{end + 1} = t;
    
return;
