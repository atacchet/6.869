function gencal10bestvex(trloc, teloc)

	d = vex_load('/cbcl/scratch01/datasets/cal101/vex.mat');
	for i = 1:5
		o = d.Copy;
		nclass = numel(d.catNames);
		mc = o.NumItems;
		for k = 1:nclass
			if sum(o.catID == k) < mc
				mc = sum(o.catID == k);
			end
		end
		tr = [];
		te = [];
		for k = 1:nclass
			ids = find(o.catID == k);
			idtr = randsample(ids,mc-1);
			tr = [tr idtr];
			te = [te setdiff(ids, idtr)];
		end
		o.AddProp('codes',2*eye(nclass) -1);
		o.AddForm('dsLabels',@(x) {x.codes(x.catID,:)});
		trvex = o.Copy(tr);
		tevex = o.Copy(te);
	
		vex_save(trloc{i}, trvex);
		vex_save(teloc{i}, tevex);
	end
	
	
