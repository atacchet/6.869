In this paper we explore the consequences of endowing a low-level computer
vision algorithm with some knowledge about the semantic properties of the world,
in particular, we try to improve example-based super-resolution by incorporating
image category information. In other words, we try to answer the following
question: is it possible to improve upon the current state of the art of image
super-resolution by somehow telling the machine that it has to up-sample an
image of a bear as opposed to just a generic image?

We build upon, and try to improve, example-based super-resolution: a technique to
solve the ill-posed problem of up-sampling a low-resolution image. The algorithm
seeks a candidate high resolution image that explains the low resolution input,
reconstructs the details that were lost in the low resolution image and is a
plausible real-world image. It does so by constructing a collage of high
resolution patches chosen from a dictionary that was previously compiled storing
all patches from a collection of lowres-highres image pairs.

In order to answer our initial question we modified the original algorithm in
such a way that it is able to recognize what is depicted in the low resolution
input (e.g. a bear) and then prefers image patches in the dictionary that come
from image of the same semantic category (i.e. pictures of bears). Overall our
method performs slightly better than the original algorithm from Freeman et
al.~\cite{Freeman01} according to a number of performance metrics and to a small
user study.

The paper is organized as follows: in Section~(\ref{sec:review})  we describe
the problem of super-resolution and give brief review of previous work. In
Section~(\ref{sec:pipeline}) we describe our system and then cover in more
details the object recognition and the super-resolution modules in
Section~(\ref{sec:class}) and~(\ref{sec:mrf}) respectively. We report and
discuss our results in Section~(\ref{sec:res}) and we conclude in
Section~(\ref{sec:conclusion}). 

\section{Previous Work}\label{sec:review}

The problem of image super-resolution is that of building a high resolution
image from a low resolution one. This problem arises in a number of applications
such as consumer image enlargements for print or high-definition digital display
and compression. Image super-resolution is ill-posed: many high resolution
images can produce the same low-resolution image (see Fig.~\ref{fig:confusion}
for an example). 

\begin{figure}[!h] \center \includegraphics[]{confusion.jpg} \caption{Two image
    patches that collapse to the same image (a uniform mid-gray patch) when
down-sampled by a factor of 2 using bilinear interpolation.}
\label{fig:confusion} \end{figure}

Several approaches have been proposed to solve the super-resolution problem. The
most straightforward method is to up-sample the input image by using regular
interpolation techniques such as nearest-neighbor, bilinear or bicubic. These
methods make a local smoothness assumption and therefore usually output blurred
images that contain very visible artifacts. Even though the actual pixel number
increases, the output image still exhibits a low level of detail.

More advanced methods such as \cite{VideoSR} exploit redundancy between video
frames to infer a high-resolution version of a given movie clip. Methods that
are of closer interest to us \cite{Freeman01,SRmain} employ a dictionary-based
reconstruction scheme. They learn a dictionary of image patch pairs that
associate a given low-resolution patch and a possible high resolution
explaination. The dictionary is built by extracting patches from a training set
of natural images. Then, the method infers a high-resolution version of an input
image by extracting patches from it and looking into the dictionary for related
low resolution patches. The high-resolution estimate is then synthesized from
the corresponding high-resolution patches \cite{Synthesis}. The method takes
into account spatial agreement between neighboring patches by means of a Markov
Random Field. More recently, other methods have been proposed that use the test
image itself to build the dictionary
\cite{SingleImage01,SingleImage02,SingleImage03}. They exploit self-similarity
of image regions at different scales to construct the high resolution output.

In this paper, we take the opposite approach: we take advantage of using more
training data rather than using just the test image. The goal of our project is
to build upon and improve the method proposed by Freeman et al.
\cite{SRmain,Freeman01,Freeman02}. The reconstruction performance of this method
depends on the dataset used to build the dictionary of patches. This simple
remark led us to re-visit example-based super resolution by adding a prior on
the semantic class to which the image being reconstructed belongs. We then use
this prior to reconstruct the high resolution image preferring patches that come
from the dominant same semantic class.
