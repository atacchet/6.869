To evaluate the performance of our method in reconstructing high resolution
images, we used a few different metrics and designed a user study. For sanity
check, we verified that our algorithm indeed enforced the selection of
patches coming from the same class as the image being reconstructed. On average,
81\% of patches come from the most probable class with our method, as a benchmark, only 32\%
of patches come from the same class if one does not take the class membership probability into account.

We started by evaluating our reconstruction against ground truth using the
standard MSE and PSNR measures that capture the absolute difference between two
images at pixel level. We also varied the weight of the probability prior
$\gamma$ in Eq.~\eqref{eq:energy} and found the best results when we set it
between 1 and 10. We also compared to standard bicubic interpolated upsampling.
PSNR and MSE metrics are known to be less than ideal in correlating with human
perception \cite{MSE}, so we also used the structural similarity metric defined
by :

\begin{equation}
    SSIM(x,y) =
    \frac{(2\mu_x\mu_y+c_1)(2cov_{xy}+c_2)}{(\mu_x^2+\mu_y^2+c_1)(\sigma_x^2+\sigma_y^2+c_2)}
    \label{eq:SSIM}
\end{equation}

SSIM has been designed with the assumption that human visual perception
extracts structural information from a scene \cite{SSIM}. Its aim is to be a
reliable objective metric closer to human perception. It should supposedly save
the time, cost and inconvenience of subjective studies.

\begin{figure}[!h]
    \center
    \subfigure[low-res input]{
        \includegraphics[width=.2\textwidth]{lowres.jpg}
    }
    \subfigure[output after $1$ iteration]{
        \includegraphics[width=.2\textwidth]{bp_001_highres.jpg}
    }
    \subfigure[output after $50$ iterations]{
        \includegraphics[width=.2\textwidth]{bp_050_highres.jpg}
    }
    \subfigure[output after $100$ iterations]{
        \includegraphics[width=.2\textwidth]{bp_100_highres.jpg}
    }
    \caption{An example output of our algorithm for different values of the
    number of iterations.}
    \label{fig:reconstructions}
\end{figure}

Both these quantitative metrics show a clear improvement over naive upsampling
($+0.3dB$ for PSNR on average), but the difference is not so clear compared to
the method of Freeman et al. where our improvement is only slightly above 1\%
(for both metrics). Average results are summarized on Table~(\ref{tab:scores}).

\begin{table}[!h] 
    \begin{centering} 
        \begin{tabular}{llr} 
            \toprule 
            & PSNR (dB) & SSIM \\
            \midrule 
            bicubic     & 21.3724 & 0.6646 \\
            $\gamma=0$  & 21.6470 & 0.6505 \\
            $\gamma=1$  & 21.6530 & 0.6509 \\
            $\gamma=10$ & \textbf{21.8700} &\textbf{ 0.6589} \\
            \bottomrule 
        \end{tabular} 
        \caption{PSNR and SSIM averaged on 50 images from Caltech-101, higher is
        better. We show results for different values of $\gamma$ (0 is equivalent
    to Freeman et al.)}
        \label{tab:scores} 
    \end{centering} 
\end{table}

We believed that these results did not tell the full story as far as perceptual
quality is involved. We therefore conducted a user study that we built using
the Psychophysics Toolbox \cite{toolbox} for Matlab. We presented 40 images from 4 different object
classes (Faces, Piano, Airplanes, Cars) to 10 human subjects. The subjects were
shown a low-res image for 2s then two high-res reconstruction side by side for
4s (with and without the class probability prior). They were asked to choose
which image they prefered. In 60\% of cases, our method was prefered showing a
slight improvement over the previous approach.


\section{Discussion}

Using the same high-level idea, other methods have been successfully proposed for
context-constrained image reconstruction \cite{ContextConstrained}. The somewhat
limited improvement of our approach can be explained by the size of the patches
we use. The statistics of such small patches is rather uniform among
different object classes, and a patch 7 pixel large is essentially unaware of
higher level semantics of the whole image. Increasing patch size has a direct
computational overhead. However, our approach lays ground for an extension that
could use visual classes on patch structure rather than on the semantic class of
the object being reconstructed. Separating textured regions, strong gradients,
uniform surfaces should exhibit a more dramatic impact on the reconstruction
quality. This is the approach of \cite{patchClass}.

In addition, our implementation, though faster than \cite{Freeman01}, still
suffers from high memory requirement to store the dictionary of image patches
(around 10Gb with our training data). It appears that the dictionary of patches
presents is very redundant. Pruning out the similar patches
should greatly improve the memory trace.

