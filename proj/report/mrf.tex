Once we obtained our first ingredient, the posterior class probabilities of our
test image, we build a graphical model to reconstruct the image. We
first pre-process images in a training dataset to build our dictionary of
patches. The test image is then modeled by a Markov Random Field whose observed
nodes are the low-res patches of the input. The latent variables are the
high-res patches we seek to infer. We learn the parameters of the model on the
test image and, for computational reasons, restric the latent variables to have
30 allowed states: one among the 30 high resolution patches that correspond to the 30 nearest neighbors of the low-res
patches in the dictionary. We then run loopy Belief Propagation to infer the full
high-res composite.

\subsection{Image pre-processing}

We prepared the low-res images by low-pass filtering and down-sampling
high-resolution images from \emph{Caltech 101}, in order to have high-low
correspondences for the dictionary and ground truth for the evaluation. We used
a scale factor of $.25$ for the re-sampling and a Gaussian blur with unit variance
as low-pass filter. Both images are then divided on a grid of patches. We
used the same sampling rate for both resolutions so that each low-res patch
corresponds to a high-res one. We then separated color and luminance information
and contrast-normalized the luminance patches (we simply divided each pixel
value by the standard deviation within the patch). Then we performed a frequency
decomposition of the luminance image in 3 frequency bands : low, mid and high frequency.
We assumed that the high frequencies of the low-resolution image (i.e. the
mid-band) are what essentially matters to estimate the high-resolution
reconstruction, and therefore used this as our representation of the low-res
patches stored in the dictionary. The high frequencies are used as the
representation for the high-res patches. This representation is chosen to be
more compact and easier for generalization \cite{Freeman01}.
To get back a high-resolution image, we simply add
the low-res image to the high-frequency image.

This pre-processing step has two goals. First, we want the model to generalize
well on new data. Second, we want our representation to be as compact as
possible. Removeing contrast and color from the equation serves both purposes.
Filtering also helps the cause under the assumption that only the high frequencies in the low-res
image are what is important to estimate the high-res version \cite{SRmain}.


\subsection{Building a dictionary of patches}

Once the images are pre-processed, we build a dictionary of low-res/high-res
patch correspondences by extracting patches from the training dataset. We used
the same sampling rate for both scales. Following \cite{SRmain}, we used patches
of size $5*5$ and $7*7$ for the high and low resolution images respectively. To
reduce the nearest neighbors retrieval time, we performed PCA on the low-res
image patches, ending up with a feature size of $20$ as suggested in
\cite{Freeman02}. In addition to compression benefits, this also improves the
generalization to new examples. We wanted to be able to distinguish between
object classes, we used many images to build our dictionary of patches, ending
up with around 7 millions patches extracted from 10 different object classes.

\subsection{Construction of the MRF}

The dictionary of patches and the class membership probabilities of the input
image are all we need to build the Markov Random Field \cite{JordanGM} that represents
the input image. The observed variable $y_\alpha$ represents the patch of
coordinate $\alpha$ extracted form the low-resolution input. The latent
variables $x_\alpha$ is the corresponding high-resolution patch we seek to infer from the
model. We build two kinds of links to define the network (see Fig.~\ref{fig:mrf}).
As in \cite{Freeman01}, the $\psi$ factors take into account
the agreement between neighboring high-res patches that overlap so that they enforce
spatial consistency in the output reconstruction:

\begin{equation}
    \psi_{\alpha\beta}(x_\alpha,x_\beta) =
    \exp\left(-\frac{d_{\alpha\beta}(x_\alpha,x_\beta)}{s}\right)
    \label{eq:compatibility}
\end{equation}

Where $d_{\alpha\beta}$ is the Euclidean distance between the regions of
$x_\alpha$ and $x_\beta$ that overlap and $s$ is a scale factor.
In turn, the $\phi$ compatibility functions measure how well a high-resolution
patch matches the low-res observed variable. 
This is where we introduce our class membership probability prior. Let
$\hat{x}_\alpha$ be the down-sampled version of $x_\alpha$, and
$\sigma_\alpha^2$ be the variance of the low resolution patch, then: 

\begin{equation}
    \phi_\alpha(x_\alpha,y_\alpha) =
    p(\mathcal{C}(y_\alpha)=c)\times\exp\left(-\frac{||y_\alpha-\hat{x}_\alpha||^2}{\sigma_\alpha^2}\right)
    \label{eq:latent_compatibility}
\end{equation}

In practice, we add a weight term to control how much we want to emphasize the
class membership prior. The energy (negative of the exponent) of the compatibility function is then :
\begin{equation}
    E_\alpha =
    \frac{||y_\alpha-\hat{x}_\alpha||^2}{\sigma_\alpha^2}-\gamma\ln(p(\mathcal{C}(y_\alpha)=c))
    \label{eq:energy}
\end{equation}

We learn these link functions on the test image. For computational tractability,
we restricted the latent high-res variables to be discrete
variables with $30$ allowed states corresponding to the $30$ nearest-neighbors
of each low-res patch within the dictionary built previously.

\begin{figure}[!h]
    \includegraphics[width=.5\textwidth]{mrf.pdf}
    \caption{The Markov Random Field that represent our image model. The latent
    high-res image $(x_i)_i$ is constrained by compatibility functions $\phi$
    with the observed low-res image $(y_i)_i$
and spatial consistency functions $\psi$.}
    \label{fig:mrf}
\end{figure}

\subsection{Belief-Propagation}

The grid structure of our latent Markov model contains loops. Exact inference
using standard graphical models techniques is therefore not tractable. This
is why we used loopy belief propagation to do approximate inference of
the high-resolution image given the MRF \cite{BP}. We searched for the Maximum A
Posteriori state of the variables $x_\alpha$. Given the graph structure,
the joint probability of the variables can be written as:

\begin{equation}
    p(x_\alpha,\ldots,x_n)=\frac{1}{Z}\prod_i\phi_i(x_i,y_i)\prod_{i,j}\psi_{ij}(x_i,x_j)
    \label{eq:maxsum}
\end{equation}

Where $Z$ is a normalization constant and the nodes are indexed by $i
\in\{1\ldots n\}$.
We used the
max-sum algorithm~\cite{Bishop} to infer the state of the latent variables, working directly
with the energy functions. Fig.~\ref{fig:bp} shows an example of the energy
profile against the number of iteration of belief propagation.
Fig.~\ref{fig:reconstructions} illustrates the associated reconstructions we
obtained. The converges quickly to a low energy estimate of the
high-res reconstructions, and a number of iterations around $30$ gives
good results in practice. Iterating further increases the total energy of the graph
(sum of the link energies) and introduces artifacts as can be seen on
Fig.~\ref{fig:reconstructions}d.

\begin{figure}[!h]
    \includegraphics[width=.5\textwidth]{energy.eps}
    \caption{Evolution of the total energy of the Markov Network as we run
    several iterations of Belief Propagation.}
    \label{fig:bp}
\end{figure}

\subsection{Speed improvements}

We wanted to explore the effect of object classes priors on high resolution
reconstruction so we needed to have a large and diverse dictionary of patches to
try and capture the between-classes variability.

Our final dictionary contains around $7$ million patches (vs. $100,000$ for the
original version), and occupies 10GB in memory. It typically takes 150 seconds to load
a dictionary this big
from disk. In order to avoid combinatorial explosion of the nearest-neighbors
retrieval cost, we used a $kd-tree$ structure as in \cite{SRmain}. We used the
existing implementation from the VLfeat package \cite{VLfeat}. This enabled us
to drastically speed-up the NN query-time, from 2 hours down to around 40 minutes per
image (around $60*80$ patches).

This was not enough to allow for a quick iterative process while we were
experimenting with our algorithm. So we implemented a
distributed job manager system to run the NN queries in parallel across a
cluster of 5 worker nodes. We defined a job as the retrieval of the 30
nearest neighbors for one row of low-res patches. This mechanism allowed us to
compute the high resolution estimate of several input images reasonably fast
(less than 10 minutes per image).
