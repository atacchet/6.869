function [p gs ns] = hmax_2010_params
% Returns p : params, gs: the cells to extract final features from, and ns: the indices of features to keep
% 'p' must have the 'package' field

p.package = 'hmax';
p.groups = {};

%-----------------------------------------------------------------------------------------------------------------------

c = struct;
c.name = 'input';
c.type = 'input';
c.size = [400 600];

p.groups{end + 1} = c;
p.input = numel(p.groups);

%-----------------------------------------------------------------------------------------------------------------------

c = struct;
c.name        = 'scale';
c.type        = 'scale';
c.pg          = p.input;
c.baseSize    = [256 256];
c.scaleFactor = 2 ^ 0.2;
c.numScales   = 8;

p.groups{end + 1} = c;
p.scale = numel(p.groups);

%-----------------------------------------------------------------------------------------------------------------------

c = struct;
c.name    = 's1';
c.type    = 'sdNDP';
c.pg      = p.scale;
c.rfCount = 11;
c.rfStep  = 1;
c.zero    = 1;
c.thres   = 0.15;
c.abs     = 1;
c.fCount  = 8;
c.fParams = {'gabor', 0.3, 5.6410, 4.5128};

p.groups{end + 1} = c;
p.s1 = numel(p.groups);

%-----------------------------------------------------------------------------------------------------------------------

c = struct;
c.name    = 'c1';
c.type    = 'cMax';
c.pg      = p.s1;
c.sCount  = 2;
c.sStep   = 1;
c.rfCount = 10;
c.rfStep  = 5;

p.groups{end + 1} = c;
p.c1 = numel(p.groups);

%-----------------------------------------------------------------------------------------------------------------------

c = struct;
c.name    = 's2';
c.type    = 'sdNDP';
c.pg      = p.c1;
c.rfCount = [4 8];
c.rfStep  = 1;
c.zero    = 1;
c.thres   = 0.00001;
c.abs     = 1;

p.groups{end + 1} = c;
p.s2 = numel(p.groups);

%-----------------------------------------------------------------------------------------------------------------------

c = struct;
c.name    = 'c2';
c.type    = 'cMax';
c.pg      = p.s2;
%c.sCount  = 4;
%c.sStep   = 3;
c.sNos = {[1 2 3 4] [4 5 6 7]};
c.rfCount = inf;

p.groups{end + 1} = c;
p.c2 = numel(p.groups);

%-----------------------------------------------------------------------------------------------------------------------

gs = [p.c2];
ns = [inf];
return;
