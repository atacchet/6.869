function path = pset4_install()

		basePath = pset4_root();
		addpath(basePath);
		addpath(fullfile(basePath, 'source'));
		run(fullfile(basePath, '..', '..','..','vlfeat','toolbox','vl_setup.m'));
