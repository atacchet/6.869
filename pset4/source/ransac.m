function [xia xib] = ransac(xa, xb, opt)
		maxinliers  = 0;
		bc			= zeros(size(xa,1),1);
		for i = 1:opt.maxIter
			candidates = randi([1 size(xa, 1)], 8, 1);
			%t = cp2tform(xa(candidates,:), xb(candidates,:), 'projective');
			t = points2tform(xa(candidates,:), xb(candidates,:));
			t = maketform('projective',t);
			[xat(:,1) xat(:,2)] = tformfwd(t,xa(:,1),xa(:,2));
			e = sum(abs(xb - xat).^2,2);
			inliers = e < opt.theta;
			fprintf('inliers: %d\n', sum(inliers));
			if sum(inliers) > maxinliers
				maxinliers = sum(inliers);
				bi = inliers;
			end	
		end	
		fprintf('max inliers %d\n', maxinliers);
		xia = xa(bi,:);
		xib = xb(bi,:);
