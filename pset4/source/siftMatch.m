function [x1 x2] = siftMatch(ia, ib)
	[fa,da] = vl_sift(im2single(rgb2gray(ia))) ;
	[fb,db] = vl_sift(im2single(rgb2gray(ib))) ;
	
	[matches, scores] = vl_ubcmatch(da,db) ;
	
	[drop, perm] = sort(scores, 'descend') ;
	matches = matches(:, perm) ;
	scores  = scores(perm) ;
	xa = fa(1,matches(1,:));
	xb = fb(1,matches(2,:));
	ya = fa(2,matches(1,:));
	yb = fb(2,matches(2,:));
	x1 = [xa' ya'];
	x2 = [xb' yb'];



