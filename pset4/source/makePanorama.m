function im = makePanorama(ia, ib, opt)
		t = transformRANSAC(ia,ib,opt);
		im = imtransform(ia, t);

		[x y] = tformfwd(t,0,0);
		x = abs(round(x));
		y = abs(round(y));

		im(y:y+size(ib,1)-1,x:x+size(ib,2)-1,:) = ib(:,:,:);
